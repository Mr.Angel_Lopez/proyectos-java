CREATE DATABASE  IF NOT EXISTS `inventario` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `inventario`;
-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: inventario
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoriaahorro`
--

DROP TABLE IF EXISTS `categoriaahorro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categoriaahorro` (
  `id_categoriaahorro` int(11) NOT NULL AUTO_INCREMENT,
  `plan_categoriaahorro` varchar(45) NOT NULL,
  `valor_beneficio_categoriaahorro` double NOT NULL,
  PRIMARY KEY (`id_categoriaahorro`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoriaahorro`
--

LOCK TABLES `categoriaahorro` WRITE;
/*!40000 ALTER TABLE `categoriaahorro` DISABLE KEYS */;
INSERT INTO `categoriaahorro` VALUES (1,'Ahorro A',0.01),(2,'Ahorro B',0.02);
/*!40000 ALTER TABLE `categoriaahorro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombres_cliente` varchar(50) NOT NULL,
  `apellidos_cliente` varchar(50) NOT NULL,
  `DUI_cliente` varchar(10) NOT NULL,
  `edad_cliente` int(11) NOT NULL,
  `telefono_cliente` varchar(15) NOT NULL,
  `direccion_cliente` varchar(50) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'nombre','apellido','12312312-3',19,'+(503)1234-5678','alli'),(2,'Douglas Alexander','Aquino Rivera','98565445-5',29,'+(503)7878-7878','san salvador');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controlahorro`
--

DROP TABLE IF EXISTS `controlahorro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `controlahorro` (
  `id_controlahorro` int(11) NOT NULL AUTO_INCREMENT,
  `id_dui_cliente_controlahorro` int(11) NOT NULL,
  `id_categoria_controlahorro` int(11) NOT NULL,
  `valor_ahorrado_controlahorro` double NOT NULL,
  `valor_deposito_controlahorro` double NOT NULL,
  `valor_extra_controlahorro` double NOT NULL,
  PRIMARY KEY (`id_controlahorro`),
  KEY `fk_ControlAhorro_Cliente_idx` (`id_dui_cliente_controlahorro`),
  KEY `fk_ControlAhorro_CategoriaAhorro1_idx` (`id_categoria_controlahorro`),
  CONSTRAINT `fk_ControlAhorro_CategoriaAhorro1` FOREIGN KEY (`id_categoria_controlahorro`) REFERENCES `categoriaahorro` (`id_categoriaahorro`),
  CONSTRAINT `fk_ControlAhorro_Cliente` FOREIGN KEY (`id_dui_cliente_controlahorro`) REFERENCES `cliente` (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controlahorro`
--

LOCK TABLES `controlahorro` WRITE;
/*!40000 ALTER TABLE `controlahorro` DISABLE KEYS */;
INSERT INTO `controlahorro` VALUES (1,1,2,204,186,18),(2,2,1,50.5,50,0.5);
/*!40000 ALTER TABLE `controlahorro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(45) NOT NULL,
  `apellido_usuario` varchar(45) NOT NULL,
  `username_Usuario` varchar(45) NOT NULL,
  `password_usuario` varchar(32) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (4,'angel','lopez','angel.lopez','f4f068e71e0d87bf0ad51e6214ab84e9'),(5,'user123','user123','user123','fd3cce4ac0f76c96d1b4bd985e3693c8');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-25 16:23:35
