package ahorrito.beans;

import java.security.MessageDigest;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import ahorrito.mantenimiento.mantenimiento_usuario;
import org.primefaces.component.message.Message;
import ahorrito.persistencia.Usuario;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;

/**
 *
 * @author angel.lopezusam
 */
@ManagedBean
@RequestScoped
public class bean_usuario {

//  <editor-fold defaultstate="collapsed" desc="variables"> 
    private int id_usuario;
    private String nomnbres_usuario;
    private String apellidos_usuario;
    private String username_usuario;
    private String password_usuario;
    public List<Usuario> mostrarusuario;
//  </editor-fold>

    /**
     * Creates a new instance of bean_usuario
     */
    public bean_usuario() {
    }

//  <editor-fold defaultstate="collapsed" desc="getter & setters"> 
    /**
     * @return the id_usuario
     */
    public int getId_usuario() {
        return id_usuario;
    }

    /**
     * @param id_usuario the id_usuario to set
     */
    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    /**
     * @return the nomnbres_usuario
     */
    public String getNomnbres_usuario() {
        return nomnbres_usuario;
    }

    /**
     * @param nomnbres_usuario the nomnbres_usuario to set
     */
    public void setNomnbres_usuario(String nomnbres_usuario) {
        this.nomnbres_usuario = nomnbres_usuario;
    }

    /**
     * @return the apellidos_usuario
     */
    public String getApellidos_usuario() {
        return apellidos_usuario;
    }

    /**
     * @param apellidos_usuario the apellidos_usuario to set
     */
    public void setApellidos_usuario(String apellidos_usuario) {
        this.apellidos_usuario = apellidos_usuario;
    }

    /**
     * @return the username_usuario
     */
    public String getUsername_usuario() {
        return username_usuario;
    }

    /**
     * @param username_usuario the username_usuario to set
     */
    public void setUsername_usuario(String username_usuario) {
        this.username_usuario = username_usuario;
    }

    /**
     * @return the password_usuario
     */
    public String getPassword_usuario() {
        return password_usuario;
    }

    /**
     * @param password_usuario the password_usuario to set
     */
    public void setPassword_usuario(String password_usuario) {
        this.password_usuario = password_usuario;
    }

    public List<Usuario> getMostrarusuario() {
        mantenimiento_usuario mantemimiento = new mantenimiento_usuario();
        return mantemimiento.mostrar_usuario();
    }

//  </editor-fold>
//  <editor-fold defaultstate="collapsed" desc="funciones"> 
    private static final char[] CONSTS_HEX = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public String funcion_incriptar(String stringAEncriptar) {
        try {
            MessageDigest msgd = MessageDigest.getInstance("MD5");
            byte[] bytes = msgd.digest(stringAEncriptar.getBytes());
            StringBuilder strbCadenaMD5 = new StringBuilder(2 * bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                int bajo = (int) (bytes[i] & 0x0f);
                int alto = (int) ((bytes[i] & 0xf0) >> 4);
                strbCadenaMD5.append(CONSTS_HEX[alto]);
                strbCadenaMD5.append(CONSTS_HEX[bajo]);
            }
            return strbCadenaMD5.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public void insertar_usuario() {
        String advertencia = "";
        Usuario usuario = new Usuario();
        mantenimiento_usuario mantenimiento = new mantenimiento_usuario();
        bean_usuario bean = new bean_usuario();

        usuario.setIdUsuario(id_usuario);
        usuario.setNombreUsuario(nomnbres_usuario);
        usuario.setApellidoUsuario(apellidos_usuario);
        usuario.setUsernameUsuario(username_usuario);
        usuario.setPasswordUsuario(bean.funcion_incriptar(password_usuario));

        if (mantenimiento.insertar_usuario(usuario) == 1) {
            advertencia = "exito al guardar nuevo usuario";
            this.setId_usuario(0);
            this.setNomnbres_usuario("");
            this.setApellidos_usuario("");
            this.setUsername_usuario("");
            this.setPassword_usuario("");
        } else {
            advertencia = "error al guardar nuevo usuario";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void actualizar_usuario() {
        String advertencia = "";
        Usuario usuario = new Usuario();
        mantenimiento_usuario mantenimiento = new mantenimiento_usuario();
        bean_usuario bean = new bean_usuario();
        usuario.setIdUsuario(id_usuario);
        usuario.setNombreUsuario(nomnbres_usuario);
        usuario.setApellidoUsuario(apellidos_usuario);
        usuario.setUsernameUsuario(username_usuario);
        usuario.setPasswordUsuario(bean.funcion_incriptar(password_usuario));

        if (mantenimiento.actualizar_usuario(usuario) == 1) {
            advertencia = "exito al guardar nuevo usuario";
            this.setId_usuario(0);
            this.setNomnbres_usuario("");
            this.setApellidos_usuario("");
            this.setUsername_usuario("");
            this.setPassword_usuario("");
        } else {
            advertencia = "error al guardar nuevo usuario";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void eliminar_usuario(int id) {
        String advertencia = "";
        mantenimiento_usuario mantenimiento = new mantenimiento_usuario();

        if (mantenimiento.eliminar_usuario(id) == 1) {
            advertencia = "exito al eliminar usuario";
        } else {
            advertencia = "error al eliminar usuario";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void mostrar_usuario_id(int id) {
        String advertencia = "";
        Usuario usuario = new Usuario();
        mantenimiento_usuario mantenimiento = new mantenimiento_usuario();

        usuario = mantenimiento.mostrar_usuario_id(id);

        if (usuario != null) {
            setId_usuario(usuario.getIdUsuario());
            setNomnbres_usuario(usuario.getNombreUsuario());
            setApellidos_usuario(usuario.getApellidoUsuario());
            setUsername_usuario(usuario.getUsernameUsuario());
            setPassword_usuario(usuario.getPasswordUsuario());
            advertencia = "Datos encontrados exitosamente";
        } else {
            advertencia = "Error al momento de encontrar los datos";
        }

        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);

    }

    public void login() {
        String advertencia = "";
        Usuario usuario = new Usuario();
        mantenimiento_usuario mantenimiento = new mantenimiento_usuario();
        bean_usuario bean = new bean_usuario();
        String hash = bean.funcion_incriptar(this.password_usuario);

        Usuario usu = mantenimiento.login(username_usuario, hash);

        if (usu.getNombreUsuario() != null) {

            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "home.xhtml");

        } else {
            advertencia = "error en contraseña o usuario";
            FacesMessage mensaje = new FacesMessage(advertencia);
            FacesContext.getCurrentInstance().addMessage(null, mensaje);
            System.out.println("mas suerte");
        }

    }

//  </editor-fold>
//    public static void main(String[] args) {
//         bean_usuario bean = new bean_usuario();
//         System.out.println(bean.funcion_incriptar("miguel"));
//                
//    }
}
