package ahorrito.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import ahorrito.mantenimiento.mantenimiento_cliente;
import ahorrito.persistencia.Cliente;

@ManagedBean
@RequestScoped
public class bean_cliente {

    public bean_cliente() {
    }
//  <editor-fold defaultstate="collapsed" desc="variables"> 
    private int id_cliente;
    private String nomnbres_cliente;
    private String apellidos_cliente;
    private String DUI_cliente;
    private int edad_cliente;
    private String telefono_cliente;
    private String direccion_cliente;
    private List<Cliente> listado;
//  </editor-fold>
//  <editor-fold defaultstate="collapsed" desc="getters & setters"> 

    /**
     * @return the id_cliente
     */
    public int getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the nomnbres_cliente
     */
    public String getNomnbres_cliente() {
        return nomnbres_cliente;
    }

    /**
     * @param nomnbres_cliente the nomnbres_cliente to set
     */
    public void setNomnbres_cliente(String nomnbres_cliente) {
        this.nomnbres_cliente = nomnbres_cliente;
    }

    /**
     * @return the apellidos_cliente
     */
    public String getApellidos_cliente() {
        return apellidos_cliente;
    }

    /**
     * @param apellidos_cliente the apellidos_cliente to set
     */
    public void setApellidos_cliente(String apellidos_cliente) {
        this.apellidos_cliente = apellidos_cliente;
    }

    /**
     * @return the DUI_cliente
     */
    public String getDUI_cliente() {
        return DUI_cliente;
    }

    /**
     * @param DUI_cliente the DUI_cliente to set
     */
    public void setDUI_cliente(String DUI_cliente) {
        this.DUI_cliente = DUI_cliente;
    }

    /**
     * @return the edad
     */
    public int getEdad_cliente() {
        return edad_cliente;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad_cliente(int edad_cliente) {
        this.edad_cliente = edad_cliente;
    }

    /**
     * @return the telefono_cliente
     */
    public String getTelefono_cliente() {
        return telefono_cliente;
    }

    /**
     * @param telefono_cliente the telefono_cliente to set
     */
    public void setTelefono_cliente(String telefono_cliente) {
        this.telefono_cliente = telefono_cliente;
    }

    /**
     * @return the direccion_cliente
     */
    public String getDireccion_cliente() {
        return direccion_cliente;
    }

    /**
     * @param direccion_cliente the direccion_cliente to set
     */
    public void setDireccion_cliente(String direccion_cliente) {
        this.direccion_cliente = direccion_cliente;
    }

    public List<Cliente> getListado() {
        mantenimiento_cliente mantenimiento = new mantenimiento_cliente();
        return mantenimiento.mostrar_cliente();
    }

//    </editor-fold>
//  <editor-fold defaultstate="collapsed" desc="funciones"> 
    public void insertar_cliente() {
        String advertencia = "";
        Cliente cliente = new Cliente();
        mantenimiento_cliente mantenimiento = new mantenimiento_cliente();
        cliente.setIdCliente(id_cliente);
        cliente.setNombresCliente(nomnbres_cliente);
        cliente.setApellidosCliente(apellidos_cliente);
        cliente.setDUIcliente(DUI_cliente);
        cliente.setEdadCliente(edad_cliente);
        cliente.setTelefonoCliente(telefono_cliente);
        cliente.setDireccionCliente(direccion_cliente);

        if (mantenimiento.insertar_cliente(cliente) == 1) {
            advertencia = "exito al guardar nuevo cliente";
            this.setId_cliente(0);
            this.setNomnbres_cliente("");
            this.setApellidos_cliente("");
            this.setDUI_cliente("");
            this.setEdad_cliente(0);
            this.setTelefono_cliente("");
            this.setDireccion_cliente("");
        } else {
            advertencia = "error al guardar nuevo cliente";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void actualizar_cliente() {
        String advertencia = "";
        Cliente cliente = new Cliente();
        mantenimiento_cliente mantenimiento = new mantenimiento_cliente();

        cliente.setIdCliente(id_cliente);
        cliente.setNombresCliente(nomnbres_cliente);
        cliente.setApellidosCliente(apellidos_cliente);
        cliente.setDUIcliente(DUI_cliente);
        cliente.setEdadCliente(edad_cliente);
        cliente.setTelefonoCliente(telefono_cliente);
        cliente.setDireccionCliente(direccion_cliente);

        if (mantenimiento.actualizar_cliente(cliente) == 1) {
            advertencia = "exito al actualizar cliente";
            this.setId_cliente(0);
            this.setNomnbres_cliente("");
            this.setApellidos_cliente("");
            this.setDUI_cliente("");
            this.setEdad_cliente(0);
            this.setTelefono_cliente("");
            this.setDireccion_cliente("");
        } else {
            advertencia = "error al actualizar cliente";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void eliminar_cliente(int id) {
        String advertencia = "";
        mantenimiento_cliente mantenimiento = new mantenimiento_cliente();

        if (mantenimiento.eliminar_cliente(id) == 1) {
            advertencia = "exito al eliminar cliente";
        } else {
            advertencia = "error al eliminar cliente";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void mostrar_cliente_id(int id) {
        String advertencia = "";
        Cliente cliente = new Cliente();
        mantenimiento_cliente mantenimiento = new mantenimiento_cliente();
        cliente = mantenimiento.mostrar_cliente_id(id);

        if (cliente != null) {
            this.setId_cliente(cliente.getIdCliente());
            this.setNomnbres_cliente(cliente.getNombresCliente());
            this.setApellidos_cliente(cliente.getApellidosCliente());
            this.setDUI_cliente(cliente.getDUIcliente());
            this.setEdad_cliente(cliente.getEdadCliente());
            this.setTelefono_cliente(cliente.getTelefonoCliente());
            this.setDireccion_cliente(cliente.getDireccionCliente());
            advertencia = "Datos encontrados exitosamente";
        } else {
            advertencia = "Error al momento de encontrar los datos";
        }

        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);

    }

//    </editor-fold>
}
