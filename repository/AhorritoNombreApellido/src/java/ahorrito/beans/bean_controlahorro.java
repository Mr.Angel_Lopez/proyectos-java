/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ahorrito.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import ahorrito.mantenimiento.mantenimiento_cliente;
import ahorrito.mantenimiento.mantenimiento_controlahorro;
import ahorrito.mantenimiento.mantemiento_categoriaahorro;
import ahorrito.persistencia.Cliente;
import ahorrito.persistencia.Categoriaahorro;
import ahorrito.persistencia.Controlahorro;

/**
 *
 * @author angel.lopezusam
 */
@ManagedBean
@RequestScoped
public class bean_controlahorro {

    //  <editor-fold defaultstate="collapsed" desc="variables">
    private int id_controlahorro;
    private int id_duicliente_controlahorro;
    private int id_categoria_controlahorro;
    private double valor_ahorrado_controlahorro;
    private double valor_deposito_controlahorro;
    private double valor_extra_controlahorro;
    private double nuevo_valor_deposito;
    private String Nombre;
    private double nuero_valor_retiro;

    public List<Controlahorro> mostrarcontrolahorro;
    public List<Cliente> mostrarcliente;
    public List<Categoriaahorro> mostrarcategorias;

    /**
     * Creates a new instance of bean_controlahorro
     */
    public bean_controlahorro() {
    }
    //  </editor-fold>

    //  <editor-fold defaultstate="collapsed" desc="listas">
    public List<Controlahorro> getMostrarcontrolahorro() {
        mantenimiento_controlahorro mantenimineto = new mantenimiento_controlahorro();
        return mantenimineto.mostrar_control();
    }

    public List<Cliente> getMostrarcliente() {
        mantenimiento_cliente mantenimineto = new mantenimiento_cliente();
        return mantenimineto.mostrar_cliente();
    }

    public List<Categoriaahorro> getMostrarcategorias() {
        mantemiento_categoriaahorro manteminiento = new mantemiento_categoriaahorro();
        return manteminiento.mostrar_Categoria();
    }
    //  </editor-fold>

    //  <editor-fold defaultstate="collapsed" desc="getters & setters">
    public int getId_controlahorro() {
        return id_controlahorro;
    }

    public void setId_controlahorro(int id_controlahorro) {
        this.id_controlahorro = id_controlahorro;
    }

    public int getId_duicliente_controlahorro() {
        return id_duicliente_controlahorro;
    }

    public void setId_duicliente_controlahorro(int id_duicliente_controlahorro) {
        this.id_duicliente_controlahorro = id_duicliente_controlahorro;
    }

    public int getId_categoria_controlahorro() {
        return id_categoria_controlahorro;
    }

    public void setId_categoria_controlahorro(int id_categoria_controlahorro) {
        this.id_categoria_controlahorro = id_categoria_controlahorro;
    }

    public double getValor_ahorrado_controlahorro() {
        return valor_ahorrado_controlahorro;
    }

    public void setValor_ahorrado_controlahorro(double valor_ahorrado_controlahorro) {
        this.valor_ahorrado_controlahorro = valor_ahorrado_controlahorro;
    }

    public double getValor_deposito_controlahorro() {
        return valor_deposito_controlahorro;
    }

    public void setValor_deposito_controlahorro(double valor_deposito_controlahorro) {
        this.valor_deposito_controlahorro = valor_deposito_controlahorro;
    }

    public double getValor_extra_controlahorro() {
        return valor_extra_controlahorro;
    }

    public void setValor_extra_controlahorro(double valor_extra_controlahorro) {
        this.valor_extra_controlahorro = valor_extra_controlahorro;
    }

    public double getNuevo_valor_deposito() {
        return nuevo_valor_deposito;
    }

    public void setNuevo_valor_deposito(double nuevo_valor_deposito) {
        this.nuevo_valor_deposito = nuevo_valor_deposito;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public double getNuero_valor_retiro() {
        return nuero_valor_retiro;
    }

    public void setNuero_valor_retiro(double nuero_valor_retiro) {
        this.nuero_valor_retiro = nuero_valor_retiro;
    }
    //  </editor-fold>

    //  <editor-fold defaultstate="collapsed" desc="funciones">
    public Categoriaahorro mostrar_categoria_id() {
        String advertencia = "";
        Categoriaahorro categoria = new Categoriaahorro();
        mantemiento_categoriaahorro mantenimiento = new mantemiento_categoriaahorro();
        categoria = mantenimiento.mostrar_Categoria_id(id_categoria_controlahorro);
        return categoria;
    }

    public Cliente mostrar_cliente_id() {
        String advertencia = "";
        Cliente cliente = new Cliente();
        mantenimiento_cliente mantenimiento = new mantenimiento_cliente();
        cliente = mantenimiento.mostrar_cliente_id(id_duicliente_controlahorro);
        return cliente;
    }

    public void insertar_control() {
        String advertencia = "";
        Controlahorro control = new Controlahorro();
        bean_controlahorro c = new bean_controlahorro();

        int idcliente = getId_duicliente_controlahorro();
        int idcategoria = getId_categoria_controlahorro();

        mantemiento_categoriaahorro category = new mantemiento_categoriaahorro();
        Categoriaahorro categoria = category.mostrar_Categoria_id(idcategoria);

        mantenimiento_cliente client = new mantenimiento_cliente();
        Cliente cliente = client.mostrar_cliente_id(idcliente);

        double valordebeneficio = categoria.getValorBeneficioCategoriaahorro();

        double valorextra = valor_deposito_controlahorro * valordebeneficio;
        double valorahorrado = valor_deposito_controlahorro + valorextra;

        mantenimiento_controlahorro mantenimiento = new mantenimiento_controlahorro();
        control.setIdControlahorro(id_controlahorro);
        control.setIdDuiClienteControlahorro(cliente);
        control.setIdCategoriaControlahorro(categoria);
        control.setValorDepositoControlahorro(valor_deposito_controlahorro);
        control.setValorExtraControlahorro(valorextra);
        control.setValorAhorradoControlahorro(valorahorrado);

        if (mantenimiento.insertar_control(control) == 1) {
            this.setId_categoria_controlahorro(0);
            this.setId_controlahorro(0);
            this.setId_duicliente_controlahorro(0);
            this.setValor_ahorrado_controlahorro(0);
            this.setValor_deposito_controlahorro(0);
            this.setValor_extra_controlahorro(0);
            advertencia = "exito al guardar nuevo control";
        } else {
            advertencia = "error al guardar nuevo control";
        }
    }

    public void actualizar_control() {
        String advertencia = "";
        Controlahorro control = new Controlahorro();
        bean_controlahorro c = new bean_controlahorro();
        mantenimiento_controlahorro mantenimiento = new mantenimiento_controlahorro();
        control.setIdControlahorro(id_controlahorro);
        control.setIdDuiClienteControlahorro(c.mostrar_cliente_id());
        control.setIdCategoriaControlahorro(c.mostrar_categoria_id());
        control.setValorDepositoControlahorro(valor_deposito_controlahorro);

        double valorextra = (valor_deposito_controlahorro * c.mostrar_categoria_id().getValorBeneficioCategoriaahorro());
        double valorahorrado = valor_deposito_controlahorro + valorextra;

        control.setValorExtraControlahorro(valorextra);
        control.setValorAhorradoControlahorro(valorahorrado);

        if (mantenimiento.actualizar_control(control) == 1) {
            this.setId_categoria_controlahorro(0);
            this.setId_controlahorro(0);
            this.setId_duicliente_controlahorro(0);
            this.setValor_ahorrado_controlahorro(0);
            this.setValor_deposito_controlahorro(0);
            this.setValor_extra_controlahorro(0);
            advertencia = "exito al guardar nuevo control";
        } else {
            advertencia = "error al guardar nuevo control";
        }
    }

    public void eliminar_control(int id) {
        String advertencia = "";
        mantenimiento_controlahorro mantenimiento = new mantenimiento_controlahorro();

        if (mantenimiento.eliminar_control(id) == 1) {
            advertencia = "exito al eliminar control";
        } else {
            advertencia = "error al eliminar control";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void mostrar_contro_id(int id) {
        String advertencia = "";
        Controlahorro control = new Controlahorro();
        mantenimiento_controlahorro mantenimiento = new mantenimiento_controlahorro();
        control = mantenimiento.mostrar_control_id(id);

        if (control != null) {
            this.setId_controlahorro(control.getIdControlahorro());
            this.setId_duicliente_controlahorro(control.getIdDuiClienteControlahorro().getIdCliente());
            this.setId_categoria_controlahorro(control.getIdCategoriaControlahorro().getIdCategoriaahorro());
            this.setValor_deposito_controlahorro(control.getValorDepositoControlahorro());
            this.setValor_extra_controlahorro(control.getValorExtraControlahorro());
            this.setValor_ahorrado_controlahorro(control.getValorAhorradoControlahorro());
            this.setNombre(control.getIdDuiClienteControlahorro().getNombresCliente() + " " + control.getIdDuiClienteControlahorro().getApellidosCliente());
            advertencia = "Datos encontrados exitosamente";
        } else {
            advertencia = "Error al momento de encontrar los datos";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);

    }

    public void nuevo_deposito() {

        String advertencia = "";
        Controlahorro control = new Controlahorro();
        Controlahorro nc = new Controlahorro();
        bean_controlahorro c = new bean_controlahorro();
        mantenimiento_controlahorro mantenimiento = new mantenimiento_controlahorro();
        control = mantenimiento.mostrar_control_id(id_controlahorro);

        int idcategoria = getId_categoria_controlahorro();
        mantemiento_categoriaahorro category = new mantemiento_categoriaahorro();
        Categoriaahorro categoria = category.mostrar_Categoria_id(idcategoria);
        double valordebeneficio = categoria.getValorBeneficioCategoriaahorro();
        double newdeposito = this.nuevo_valor_deposito;
        double newvextra = this.nuevo_valor_deposito * valordebeneficio;
        double newahorro = newdeposito + newvextra;

        nc.setIdControlahorro(control.getIdControlahorro());
        nc.setIdDuiClienteControlahorro(control.getIdDuiClienteControlahorro());
        nc.setIdCategoriaControlahorro(control.getIdCategoriaControlahorro());
        nc.setValorDepositoControlahorro((control.getValorDepositoControlahorro()) + newdeposito);
        nc.setValorExtraControlahorro((control.getValorExtraControlahorro()) + newvextra);
        nc.setValorAhorradoControlahorro((control.getValorAhorradoControlahorro()) + newahorro);

        if (mantenimiento.actualizar_control(nc) == 1) {
            this.setId_categoria_controlahorro(0);
            this.setId_controlahorro(0);
            this.setId_duicliente_controlahorro(0);
            this.setValor_ahorrado_controlahorro(0);
            this.setValor_deposito_controlahorro(0);
            this.setValor_extra_controlahorro(0);
            advertencia = "exito al realizar el deposito";
        } else {
            advertencia = "Error al momento realizar el deposito";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void nuevo_retiro() {
        String advertencia = "";
        Controlahorro control = new Controlahorro();
        Controlahorro cn = new Controlahorro();
        bean_controlahorro c = new bean_controlahorro();
        mantenimiento_controlahorro mantenimiento = new mantenimiento_controlahorro();
        control = mantenimiento.mostrar_control_id(id_controlahorro);

        if (nuero_valor_retiro > control.getValorAhorradoControlahorro()) {
            advertencia = "ERROR No Cuenta Con La Cantidad Seleccionada";
        } else {
            cn.setIdControlahorro(control.getIdControlahorro());
            cn.setIdCategoriaControlahorro(control.getIdCategoriaControlahorro());
            cn.setIdDuiClienteControlahorro(control.getIdDuiClienteControlahorro());
            cn.setValorAhorradoControlahorro(control.getValorAhorradoControlahorro() - nuero_valor_retiro);
            cn.setValorExtraControlahorro(control.getValorExtraControlahorro());
            cn.setValorDepositoControlahorro(control.getValorDepositoControlahorro() - nuero_valor_retiro);

            if (mantenimiento.actualizar_control(cn) == 1) {
                this.setId_categoria_controlahorro(0);
                this.setId_controlahorro(0);
                this.setId_duicliente_controlahorro(0);
                this.setValor_ahorrado_controlahorro(0);
                this.setValor_deposito_controlahorro(0);
                this.setValor_extra_controlahorro(0);
                this.setNuero_valor_retiro(0);
                advertencia = "Exito al realizar retiro";
            } else {
                advertencia = "Error al momento realizar el retiro";
            }
        }

        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }
//  </editor-fold>
}
