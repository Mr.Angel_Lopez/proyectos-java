/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ahorrito.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import ahorrito.mantenimiento.mantemiento_categoriaahorro;
import ahorrito.persistencia.Categoriaahorro;

/**
 *
 * @author angel.lopezusam
 */
@ManagedBean
@RequestScoped
public class bean_categoriaahorro {
//  <editor-fold defaultstate="collapsed" desc="variables">

    private int id_categoriaahorro;
    private String plan_categoria;
    private double valor_beneficio;
    public List<Categoriaahorro> mostrar_categorias;
//  </editor-fold> 

//  <editor-fold defaultstate="collapsed" desc="getters & setters">
    public int getId_categoriaahorro() {
        return id_categoriaahorro;
    }

    public void setId_categoriaahorro(int id_categoriaahorro) {
        this.id_categoriaahorro = id_categoriaahorro;
    }

    public String getPlan_categoria() {
        return plan_categoria;
    }

    public void setPlan_categoria(String plan_categoria) {
        this.plan_categoria = plan_categoria;
    }

    public double getValor_beneficio() {
        return valor_beneficio;
    }

    public void setValor_beneficio(double valor_beneficio) {
        this.valor_beneficio = valor_beneficio;
    }

    public List<Categoriaahorro> getMostrar_categorias() {
        mantemiento_categoriaahorro manteminiento = new mantemiento_categoriaahorro();
        return manteminiento.mostrar_Categoria();
    }

    /**
     * Creates a new instance of bean_categoriaahorro
     */
    public bean_categoriaahorro() {
    }
    //  </editor-fold> 

//  <editor-fold defaultstate="collapsed" desc="funciones">
    public void insertar_categoria() {
        String advertencia = "";
        Categoriaahorro categoria = new Categoriaahorro();
        mantemiento_categoriaahorro mantenimiento = new mantemiento_categoriaahorro();
        categoria.setIdCategoriaahorro(id_categoriaahorro);
        categoria.setPlanCategoriaahorro(plan_categoria);
        categoria.setValorBeneficioCategoriaahorro(valor_beneficio);

        if (mantenimiento.insertar_Categoria(categoria) == 1) {
            advertencia = "exito al guardar nuevo categoria";
            this.setId_categoriaahorro(0);
            this.setPlan_categoria("");
            this.setValor_beneficio(0);
        } else {
            advertencia = "error al guardar nuevo categoria";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void actualizar_categoria() {
        String advertencia = "";
        Categoriaahorro categoria = new Categoriaahorro();
        mantemiento_categoriaahorro mantenimiento = new mantemiento_categoriaahorro();

        categoria.setIdCategoriaahorro(id_categoriaahorro);
        categoria.setPlanCategoriaahorro(plan_categoria);
        categoria.setValorBeneficioCategoriaahorro(valor_beneficio);

        if (mantenimiento.actualizar_categoria(categoria) == 1) {
            advertencia = "exito al actualizar categoria";
            this.setId_categoriaahorro(0);
            this.setPlan_categoria("");
            this.setValor_beneficio(0);
        } else {
            advertencia = "error al actualizar categoria";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void eliminar_categoria(int id) {
        String advertencia = "";
        mantemiento_categoriaahorro mantenimiento = new mantemiento_categoriaahorro();

        if (mantenimiento.eliminar_Categoria(id) == 1) {
            advertencia = "exito al eliminar categoria";
        } else {
            advertencia = "error al eliminar categoria";
        }
        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void mostrar_categoria_id(int id) {
        String advertencia = "";
        Categoriaahorro categoria = new Categoriaahorro();
        mantemiento_categoriaahorro mantenimiento = new mantemiento_categoriaahorro();
        categoria = mantenimiento.mostrar_Categoria_id(id);

        if (categoria != null) {
            this.setId_categoriaahorro(categoria.getIdCategoriaahorro());
            this.setPlan_categoria(categoria.getPlanCategoriaahorro());
            this.setValor_beneficio(categoria.getValorBeneficioCategoriaahorro());

            advertencia = "Datos encontrados exitosamente";
        } else {
            advertencia = "Error al momento de encontrar los datos";
        }

        FacesMessage mensaje = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);

    }
//  </editor-fold>

}
