/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ahorrito.mantenimiento;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import ahorrito.persistencia.Usuario;

public class mantenimiento_usuario {

//  <editor-fold defaultstate="collapsed" desc="insertar un nuevo usuario"> 
//    esta funcion será utilizada para que podamos hacer inserciones de nuestros usuarios
    public int insertar_usuario(Usuario usuario) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
//      aqui emprieza nuestra trasaccion para poder insertar
        em.getTransaction().begin();
        System.out.println("trasaccion insertar iniciada");
        try {
            em.persist(usuario);
            em.getTransaction().commit();
//            hacemos que nuestro vector persista osea, que se guarde en nuestra persistencia y luego en nuestra base
            flag = 1;
            System.out.println("trasaccion insertar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion insertar cerrada");
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="actualizar un usuario"> 
//    esta funcion será utilizada para que podamos hacer modificaciones de nuestros usuarios    
    public int actualizar_usuario(Usuario usuario) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui emprieza nuestra trasaccion para poder modificar
        System.out.println("trasaccion actualizar iniciada");
        try {
            em.merge(usuario);
            em.getTransaction().commit();
//          pasamos el informacion modificada de nuestro vector            
            flag = 1;
            System.out.println("trasaccion actualizar ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion actualizar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="eliminar un usuario"> 
    public int eliminar_usuario(int id) {
//    esta funcion será utilizada para que podamos hacer eliminaciones de nuestros usuarios        
        int flag = 0;
        Usuario usuario = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder eliminar        
        System.out.println("trasaccion eliminar iniciada");
        try {
            usuario = em.find(Usuario.class, id);
            em.remove(usuario);
//      haciendo uso del "int id" que solicitamos para esta funcion, buscamos a nuestro vector para luego eliminarlo            
            em.getTransaction().commit();
            flag = 1;
            System.out.println("trasaccion eliminar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion eliminar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar un usuario">     
//    esta funcion será utilizada para que podamos mostrar nuestros usuarios mediante su id
    public Usuario mostrar_usuario_id(int id) {
        Usuario usuario = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
//      aqui empieza nuestra trasaccion para poder mostrar        
        em.getTransaction().begin();
        System.out.println("trasaccion mostrar por id iniciada");
        try {
            usuario = em.find(Usuario.class, id);
            em.getTransaction().commit();
//      aqui buscamos nuestro vector por su id y luego igualamos un objeto al resultado de muestra busqueda            
            System.out.println("trasaccion mostrar por id ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion mostrar por id cerrada");
        }
        return usuario;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar listado de usuario"> 
//    esta funcion será utilizada para que podamos ver todo un listado de nuestros usuarios    
    public List<Usuario> mostrar_usuario() {
        List<Usuario> usuario = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para mostrar        
        System.out.println("trasaccion mostrar iniciada");
        try {
            Query query = em.createQuery("SELECT u FROM Usuario u");
//      aqui utilizamos una query para poder traer todos los datos de nuestra tabla            
            usuario = query.getResultList();
            em.getTransaction().commit();
            System.out.println("trasaccion mostrar ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion mostrar cerrada");
        }
        return usuario;
    }
//  </editor-fold>

    public Usuario login(String user, String pass) {
        Usuario usuario = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para mostrar        
        System.out.println("trasaccion mostrar iniciada");
        try {
            Query query = em.createQuery("SELECT u FROM Usuario u WHERE u.usernameUsuario = :usernameUsuario AND u.passwordUsuario = :passwordUsuario", Usuario.class);
            query.setParameter("usernameUsuario", user);
            query.setParameter("passwordUsuario", pass);
            usuario = (Usuario) query.getSingleResult();

            em.getTransaction().commit();

            System.out.println("trasaccion mostrar ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion mostrar cerrada");
        }
        return usuario;
    }

}
