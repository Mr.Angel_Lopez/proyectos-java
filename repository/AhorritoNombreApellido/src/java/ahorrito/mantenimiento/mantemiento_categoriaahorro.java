package ahorrito.mantenimiento;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import ahorrito.persistencia.Categoriaahorro;

public class mantemiento_categoriaahorro {

//  <editor-fold defaultstate="collapsed" desc="insertar un nuevo Categoria"> 
//    esta funcion será utilizada para que podamos hacer inserciones de nuestras categorias
    public int insertar_Categoria(Categoriaahorro Categoria) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
//      aqui empieza nuestra trasaccion para poder insertar
        em.getTransaction().begin();
        System.out.println("trasaccion insertar iniciada");
        try {
            em.persist(Categoria);
            em.getTransaction().commit();
//            hacemos que nuestro vector persista osea, que se guarde en nuestra persistencia y luego en nuestra base
            flag = 1;
            System.out.println("trasaccion insertar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion
            System.out.println("trasaccion insertar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="actualizar un Categoria"> 
    public int actualizar_categoria(Categoriaahorro Categoria) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("trasaccion actualizar iniciada");
        try {
            em.merge(Categoria);
            em.getTransaction().commit();
            flag = 1;
            System.out.println("trasaccion actualizar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion actualizar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="eliminar un Categoria"> 
    public int eliminar_Categoria(int id) {
        int flag = 0;
        Categoriaahorro Categoria = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("trasaccion eliminar iniciada");
        try {
            Categoria = em.find(Categoriaahorro.class, id);
            em.remove(Categoria);
            em.getTransaction().commit();
            flag = 1;
            System.out.println("trasaccion eliminar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion eliminar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar un Categoria">     
    public Categoriaahorro mostrar_Categoria_id(int id) {
        Categoriaahorro Categoria = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("trasaccion mostrar por id iniciada");
        try {
            Categoria = em.find(Categoriaahorro.class, id);
            em.getTransaction().commit();
            System.out.println("trasaccion mostrar por id ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion mostrar por id cerrada");
        }
        return Categoria;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar listado de Categoria">     
    public List<Categoriaahorro> mostrar_Categoria() {
        List<Categoriaahorro> Categoria = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("trasaccion mostrar iniciada");
        try {
            Query query = em.createQuery("SELECT c FROM Categoriaahorro c");
            Categoria = query.getResultList();
            em.getTransaction().commit();
            System.out.println("trasaccion mostrar ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion mostrar cerrada");
        }
        return Categoria;
    }
//  </editor-fold>
}
