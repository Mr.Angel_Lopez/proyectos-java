package ahorrito.mantenimiento;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import ahorrito.persistencia.Cliente;

public class mantenimiento_cliente {

//  <editor-fold defaultstate="collapsed" desc="insertar un nuevo cliente"> 
//    esta funcion será utilizada para que podamos hacer inserciones de nuestros cliente
    public int insertar_cliente(Cliente cliente) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
//      aqui empieza nuestra trasaccion para poder insertar
        em.getTransaction().begin();
        System.out.println("trasaccion insertar iniciada");
        try {
            em.persist(cliente);
            em.getTransaction().commit();
//            hacemos que nuestro vector persista osea, que se guarde en nuestra persistencia y luego en nuestra base            
            flag = 1;
            System.out.println("trasaccion insertar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion
            System.out.println("trasaccion insertar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="actualizar un cliente"> 
//    esta funcion será utilizada para que podamos hacer actualizaciones de nuestros cliente
    public int actualizar_cliente(Cliente cliente) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder actualizar      
        System.out.println("trasaccion actualizar iniciada");
        try {
            em.merge(cliente);
            em.getTransaction().commit();
            flag = 1;
            System.out.println("trasaccion actualizar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion actualizar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="eliminar un cliente"> 
//    esta funcion será utilizada para que podamos hacer eliminaciones de nuestros cliente
    public int eliminar_cliente(int id) {
        int flag = 0;
        Cliente cliente = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder eliminar              
        System.out.println("trasaccion eliminar iniciada");
        try {
            cliente = em.find(Cliente.class, id);
            em.remove(cliente);
            em.getTransaction().commit();
            flag = 1;
            System.out.println("trasaccion eliminar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion eliminar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar un cliente">     
//    esta funcion será utilizada para que podamos mostrar a nuestros clientes por su id
    public Cliente mostrar_cliente_id(int id) {
        Cliente cliente = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder mostrar      
        System.out.println("trasaccion mostrar por id iniciada");
        try {
            cliente = em.find(Cliente.class, id);
            em.getTransaction().commit();
            System.out.println("trasaccion mostrar por id ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion mostrar por id cerrada");
        }
        return cliente;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar listado de cliente">  
//    esta funcion será utilizada para que podamos mostrar a todos nuestros clientes    
    public List<Cliente> mostrar_cliente() {
        List<Cliente> cliente = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder mostrar      
        System.out.println("trasaccion mostrar iniciada");
        try {
            Query query = em.createQuery("SELECT c FROM Cliente c");
//      aqui utilizamos una query para poder traer todos los datos de nuestra tabla                        
            cliente = query.getResultList();
            em.getTransaction().commit();
            System.out.println("trasaccion mostrar ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion mostrar cerrada");
        }
        return cliente;
    }
//  </editor-fold>

}
