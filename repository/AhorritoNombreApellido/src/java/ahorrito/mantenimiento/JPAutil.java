
package ahorrito.mantenimiento;
import javax.persistence.Persistence;
import javax.persistence.EntityManagerFactory;

public class JPAutil {
    private static final EntityManagerFactory emf;
    static {
        try {
            emf = Persistence.createEntityManagerFactory("AhorritoNombreApellidoPU");
        } catch (Exception e) {
            System.out.println("Error en: "+e);
            throw new ExceptionInInitializerError(e);
        }
    }
    public static EntityManagerFactory getEntityManagerFactory(){
        return emf;
    }
    
}
