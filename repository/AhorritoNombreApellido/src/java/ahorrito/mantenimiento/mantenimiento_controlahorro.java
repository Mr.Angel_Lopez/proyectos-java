package ahorrito.mantenimiento;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import ahorrito.persistencia.Controlahorro;

public class mantenimiento_controlahorro {

    //  <editor-fold defaultstate="collapsed" desc="insertar un nuevo control"> 
    //    esta funcion será utilizada para que podamos hacer inserciones de nuestros controles de ahorros
    public int insertar_control(Controlahorro control) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
//      aqui empieza nuestra trasaccion para poder insertar
        em.getTransaction().begin();
        System.out.println("trasaccion insertar iniciada");
        try {
            em.persist(control);
            em.getTransaction().commit();
//            hacemos que nuestro vector persista osea, que se guarde en nuestra persistencia y luego en nuestra base            
            flag = 1;
            System.out.println("trasaccion insertar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion
            System.out.println("trasaccion insertar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="actualizar un control"> 
//    esta funcion será utilizada para que podamos hacer modificaciones de nuestros controles de ahorros
    public int actualizar_control(Controlahorro control) {
        int flag = 0;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder modificar
        System.out.println("trasaccion actualizar iniciada");
        try {
            em.merge(control);
            em.getTransaction().commit();
//          pasamos el informacion modificada de nuestro vesctor
            flag = 1;
            System.out.println("trasaccion actualizar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
//            cerramos nuestra transaccion una vez ejecutada nuestra funcion            
            System.out.println("trasaccion actualizar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="eliminar un control"> 
//    esta funcion será utilizada para que podamos hacer eliminaciones de nuestros controles de ahorros
    public int eliminar_control(int id) {
        int flag = 0;
        Controlahorro control = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder eliminar
        System.out.println("trasaccion eliminar iniciada");
        try {
            control = em.find(Controlahorro.class, id);
            em.remove(control);
//      haciendo uso del "int id" que solicitamos para esta funcion, buscamos a nuestro vector para luego eliminarlo
            em.getTransaction().commit();
            flag = 1;
            System.out.println("trasaccion eliminar ejecutada");

        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion eliminar cerrada");
        }
        return flag;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar un control">     
//    esta funcion será utilizada para que podamos mostrar controles de ahorros mediante id
    public Controlahorro mostrar_control_id(int id) {
        Controlahorro control = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para poder mostrar
        System.out.println("trasaccion mostrar por id iniciada");
        try {
            control = em.find(Controlahorro.class, id);
            em.getTransaction().commit();
//      aqui buscamos nuestro vector por su id y luego igualamos un objeto al resultado de muestra busqueda
            System.out.println("trasaccion mostrar por id ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion mostrar por id cerrada");
        }
        return control;
    }
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="mostrar listado de control">     
//    esta funcion será utilizada para que podamos ver todo un listado de nuestro control de ahorro
    public List<Controlahorro> mostrar_control() {
        List<Controlahorro> control = null;
        EntityManager em = JPAutil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
//      aqui empieza nuestra trasaccion para mostrar
        System.out.println("trasaccion mostrar iniciada");
        try {
            Query query = em.createQuery("SELECT u FROM Controlahorro u");
//      aqui utilizamos una query para poder traer todos los datos de nuestra tabla
            control = query.getResultList();
            em.getTransaction().commit();
            System.out.println("trasaccion mostrar ejecutada");
        } catch (Exception e) {
            System.out.println("error en: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
            System.out.println("trasaccion mostrar cerrada");
        }
        return control;
    }
//  </editor-fold>
}
