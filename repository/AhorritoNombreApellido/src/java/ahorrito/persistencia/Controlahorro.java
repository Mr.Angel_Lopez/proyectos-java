/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ahorrito.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "controlahorro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Controlahorro.findAll", query = "SELECT c FROM Controlahorro c")
    , @NamedQuery(name = "Controlahorro.findByIdControlahorro", query = "SELECT c FROM Controlahorro c WHERE c.idControlahorro = :idControlahorro")
    , @NamedQuery(name = "Controlahorro.findByValorAhorradoControlahorro", query = "SELECT c FROM Controlahorro c WHERE c.valorAhorradoControlahorro = :valorAhorradoControlahorro")
    , @NamedQuery(name = "Controlahorro.findByValorDepositoControlahorro", query = "SELECT c FROM Controlahorro c WHERE c.valorDepositoControlahorro = :valorDepositoControlahorro")
    , @NamedQuery(name = "Controlahorro.findByValorExtraControlahorro", query = "SELECT c FROM Controlahorro c WHERE c.valorExtraControlahorro = :valorExtraControlahorro")})
public class Controlahorro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_controlahorro")
    private Integer idControlahorro;
    @Basic(optional = false)
    @Column(name = "valor_ahorrado_controlahorro")
    private double valorAhorradoControlahorro;
    @Basic(optional = false)
    @Column(name = "valor_deposito_controlahorro")
    private double valorDepositoControlahorro;
    @Basic(optional = false)
    @Column(name = "valor_extra_controlahorro")
    private double valorExtraControlahorro;
    @JoinColumn(name = "id_categoria_controlahorro", referencedColumnName = "id_categoriaahorro")
    @ManyToOne(optional = false)
    private Categoriaahorro idCategoriaControlahorro;
    @JoinColumn(name = "id_dui_cliente_controlahorro", referencedColumnName = "id_cliente")
    @ManyToOne(optional = false)
    private Cliente idDuiClienteControlahorro;

    public Controlahorro() {
    }

    public Controlahorro(Integer idControlahorro) {
        this.idControlahorro = idControlahorro;
    }

    public Controlahorro(Integer idControlahorro, double valorAhorradoControlahorro, double valorDepositoControlahorro, double valorExtraControlahorro) {
        this.idControlahorro = idControlahorro;
        this.valorAhorradoControlahorro = valorAhorradoControlahorro;
        this.valorDepositoControlahorro = valorDepositoControlahorro;
        this.valorExtraControlahorro = valorExtraControlahorro;
    }

    public Integer getIdControlahorro() {
        return idControlahorro;
    }

    public void setIdControlahorro(Integer idControlahorro) {
        this.idControlahorro = idControlahorro;
    }

    public double getValorAhorradoControlahorro() {
        return valorAhorradoControlahorro;
    }

    public void setValorAhorradoControlahorro(double valorAhorradoControlahorro) {
        this.valorAhorradoControlahorro = valorAhorradoControlahorro;
    }

    public double getValorDepositoControlahorro() {
        return valorDepositoControlahorro;
    }

    public void setValorDepositoControlahorro(double valorDepositoControlahorro) {
        this.valorDepositoControlahorro = valorDepositoControlahorro;
    }

    public double getValorExtraControlahorro() {
        return valorExtraControlahorro;
    }

    public void setValorExtraControlahorro(double valorExtraControlahorro) {
        this.valorExtraControlahorro = valorExtraControlahorro;
    }

    public Categoriaahorro getIdCategoriaControlahorro() {
        return idCategoriaControlahorro;
    }

    public void setIdCategoriaControlahorro(Categoriaahorro idCategoriaControlahorro) {
        this.idCategoriaControlahorro = idCategoriaControlahorro;
    }

    public Cliente getIdDuiClienteControlahorro() {
        return idDuiClienteControlahorro;
    }

    public void setIdDuiClienteControlahorro(Cliente idDuiClienteControlahorro) {
        this.idDuiClienteControlahorro = idDuiClienteControlahorro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idControlahorro != null ? idControlahorro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Controlahorro)) {
            return false;
        }
        Controlahorro other = (Controlahorro) object;
        if ((this.idControlahorro == null && other.idControlahorro != null) || (this.idControlahorro != null && !this.idControlahorro.equals(other.idControlahorro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ahorrito.persistencia.Controlahorro[ idControlahorro=" + idControlahorro + " ]";
    }
    
}
