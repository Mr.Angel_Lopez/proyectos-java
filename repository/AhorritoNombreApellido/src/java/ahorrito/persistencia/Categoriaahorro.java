/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ahorrito.persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "categoriaahorro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoriaahorro.findAll", query = "SELECT c FROM Categoriaahorro c")
    , @NamedQuery(name = "Categoriaahorro.findByIdCategoriaahorro", query = "SELECT c FROM Categoriaahorro c WHERE c.idCategoriaahorro = :idCategoriaahorro")
    , @NamedQuery(name = "Categoriaahorro.findByPlanCategoriaahorro", query = "SELECT c FROM Categoriaahorro c WHERE c.planCategoriaahorro = :planCategoriaahorro")
    , @NamedQuery(name = "Categoriaahorro.findByValorBeneficioCategoriaahorro", query = "SELECT c FROM Categoriaahorro c WHERE c.valorBeneficioCategoriaahorro = :valorBeneficioCategoriaahorro")})
public class Categoriaahorro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_categoriaahorro")
    private Integer idCategoriaahorro;
    @Basic(optional = false)
    @Column(name = "plan_categoriaahorro")
    private String planCategoriaahorro;
    @Basic(optional = false)
    @Column(name = "valor_beneficio_categoriaahorro")
    private double valorBeneficioCategoriaahorro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCategoriaControlahorro")
    private Collection<Controlahorro> controlahorroCollection;

    public Categoriaahorro() {
    }

    public Categoriaahorro(Integer idCategoriaahorro) {
        this.idCategoriaahorro = idCategoriaahorro;
    }

    public Categoriaahorro(Integer idCategoriaahorro, String planCategoriaahorro, double valorBeneficioCategoriaahorro) {
        this.idCategoriaahorro = idCategoriaahorro;
        this.planCategoriaahorro = planCategoriaahorro;
        this.valorBeneficioCategoriaahorro = valorBeneficioCategoriaahorro;
    }

    public Integer getIdCategoriaahorro() {
        return idCategoriaahorro;
    }

    public void setIdCategoriaahorro(Integer idCategoriaahorro) {
        this.idCategoriaahorro = idCategoriaahorro;
    }

    public String getPlanCategoriaahorro() {
        return planCategoriaahorro;
    }

    public void setPlanCategoriaahorro(String planCategoriaahorro) {
        this.planCategoriaahorro = planCategoriaahorro;
    }

    public double getValorBeneficioCategoriaahorro() {
        return valorBeneficioCategoriaahorro;
    }

    public void setValorBeneficioCategoriaahorro(double valorBeneficioCategoriaahorro) {
        this.valorBeneficioCategoriaahorro = valorBeneficioCategoriaahorro;
    }

    @XmlTransient
    public Collection<Controlahorro> getControlahorroCollection() {
        return controlahorroCollection;
    }

    public void setControlahorroCollection(Collection<Controlahorro> controlahorroCollection) {
        this.controlahorroCollection = controlahorroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoriaahorro != null ? idCategoriaahorro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriaahorro)) {
            return false;
        }
        Categoriaahorro other = (Categoriaahorro) object;
        if ((this.idCategoriaahorro == null && other.idCategoriaahorro != null) || (this.idCategoriaahorro != null && !this.idCategoriaahorro.equals(other.idCategoriaahorro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ahorrito.persistencia.Categoriaahorro[ idCategoriaahorro=" + idCategoriaahorro + " ]";
    }
    
}
