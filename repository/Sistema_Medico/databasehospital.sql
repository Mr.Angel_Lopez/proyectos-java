CREATE DATABASE  IF NOT EXISTS `ra` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `ra`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: ra
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cita`
--

DROP TABLE IF EXISTS `cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cita` (
  `id_cita` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `codigo_expediente` varchar(11) NOT NULL,
  `area` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `tipo_cita` int(2) NOT NULL,
  `triaje` varchar(50) NOT NULL,
  `estado` varchar(10) NOT NULL,
  PRIMARY KEY (`id_cita`),
  KEY `usuario` (`usuario`),
  KEY `codigo_expediente` (`codigo_expediente`),
  KEY `tipo_cita` (`tipo_cita`),
  KEY `cita_ibfk_4_idx` (`area`),
  CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `perfil_personal` (`usuario`),
  CONSTRAINT `cita_ibfk_2` FOREIGN KEY (`codigo_expediente`) REFERENCES `expediente` (`codigo_expediente`),
  CONSTRAINT `cita_ibfk_3` FOREIGN KEY (`tipo_cita`) REFERENCES `tipo_cita` (`id_tipo_cita`),
  CONSTRAINT `cita_ibfk_4` FOREIGN KEY (`area`) REFERENCES `zona` (`id_zona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cita`
--

LOCK TABLES `cita` WRITE;
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consulta`
--

DROP TABLE IF EXISTS `consulta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `consulta` (
  `id_consulta` int(11) NOT NULL AUTO_INCREMENT,
  `cita` int(11) NOT NULL,
  `diagnostico` text NOT NULL,
  `tratamiento` text NOT NULL,
  `internamiento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_consulta`),
  KEY `cita` (`cita`),
  KEY `internamiento` (`internamiento`),
  CONSTRAINT `consulta_ibfk_1` FOREIGN KEY (`cita`) REFERENCES `cita` (`id_cita`),
  CONSTRAINT `consulta_ibfk_2` FOREIGN KEY (`internamiento`) REFERENCES `internamiento` (`id_internamiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulta`
--

LOCK TABLES `consulta` WRITE;
/*!40000 ALTER TABLE `consulta` DISABLE KEYS */;
/*!40000 ALTER TABLE `consulta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consulta_examen`
--

DROP TABLE IF EXISTS `consulta_examen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `consulta_examen` (
  `id_consulta_examen` int(10) unsigned zerofill NOT NULL,
  `consulta` int(11) NOT NULL,
  `examen` int(11) NOT NULL,
  PRIMARY KEY (`id_consulta_examen`),
  KEY `consulta` (`consulta`),
  KEY `examen` (`examen`),
  CONSTRAINT `consulta_examen_ibfk_1` FOREIGN KEY (`consulta`) REFERENCES `consulta` (`id_consulta`),
  CONSTRAINT `consulta_examen_ibfk_2` FOREIGN KEY (`examen`) REFERENCES `examen` (`id_examen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulta_examen`
--

LOCK TABLES `consulta_examen` WRITE;
/*!40000 ALTER TABLE `consulta_examen` DISABLE KEYS */;
/*!40000 ALTER TABLE `consulta_examen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consulta_medicamento`
--

DROP TABLE IF EXISTS `consulta_medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `consulta_medicamento` (
  `id_consulta_medicamento` int(11) NOT NULL AUTO_INCREMENT,
  `consulta` int(11) NOT NULL,
  `medicamento` int(11) NOT NULL,
  PRIMARY KEY (`id_consulta_medicamento`),
  KEY `consulta` (`consulta`),
  KEY `medicamento` (`medicamento`),
  CONSTRAINT `consulta_medicamento_ibfk_1` FOREIGN KEY (`consulta`) REFERENCES `consulta` (`id_consulta`),
  CONSTRAINT `consulta_medicamento_ibfk_2` FOREIGN KEY (`medicamento`) REFERENCES `medicamento` (`id_medicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulta_medicamento`
--

LOCK TABLES `consulta_medicamento` WRITE;
/*!40000 ALTER TABLE `consulta_medicamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `consulta_medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datos_medicos`
--

DROP TABLE IF EXISTS `datos_medicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `datos_medicos` (
  `id_datos_medicos` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_expediente` varchar(11) NOT NULL,
  `peso` decimal(7,2) NOT NULL,
  `altura` decimal(5,2) NOT NULL,
  `edad` varchar(45) NOT NULL,
  `IMC` decimal(5,2) DEFAULT NULL,
  `tipo_sangre` varchar(15) NOT NULL,
  `alergias` text,
  PRIMARY KEY (`id_datos_medicos`),
  KEY `codigo_expediente` (`codigo_expediente`),
  CONSTRAINT `datos_medicos_ibfk_1` FOREIGN KEY (`codigo_expediente`) REFERENCES `expediente` (`codigo_expediente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos_medicos`
--

LOCK TABLES `datos_medicos` WRITE;
/*!40000 ALTER TABLE `datos_medicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `datos_medicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datos_personales`
--

DROP TABLE IF EXISTS `datos_personales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `datos_personales` (
  `id_datos_personales` int(11) NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(25) NOT NULL,
  `segundo_nombre` varchar(25) NOT NULL,
  `tercer_nombre` varchar(25) DEFAULT NULL,
  `primer_apellido` varchar(24) NOT NULL,
  `segundo_apellido` varchar(24) DEFAULT NULL,
  `tercer_apellido` varchar(24) DEFAULT NULL,
  `sexo` varchar(10) NOT NULL,
  `pais` int(11) NOT NULL,
  `dui` varchar(12) DEFAULT NULL,
  `nit` varchar(20) DEFAULT NULL,
  `pasaporte` varchar(12) DEFAULT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `edad` int(3) NOT NULL,
  PRIMARY KEY (`id_datos_personales`),
  KEY `rpais_idx` (`pais`),
  CONSTRAINT `rpais` FOREIGN KEY (`pais`) REFERENCES `pais` (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos_personales`
--

LOCK TABLES `datos_personales` WRITE;
/*!40000 ALTER TABLE `datos_personales` DISABLE KEYS */;
INSERT INTO `datos_personales` VALUES (1,'Roberto','Carlos','','Abrego','Hercules','','Masculino',50,'','','','1997-06-10',21),(2,'Roberto','Carlos','','Hernandes','Vasques','','Masculino',50,'','','','1997-06-10',24),(3,'Angel','Antonio','','Lopez','Sorto','','Masculino',50,'00000000-3','0000-000000-000-3','000','1997-06-10',19),(4,'Wendy','Beatris','','Abrego','Hercules','','Femenino',50,'','','','2018-09-02',18),(5,'Roberto','Antonio','','Hernandes','Sorto','','Masculino',50,'05555555-5','2113-213131-313-5','54545554454','2003-01-04',15),(6,'Carlos','Antonio','','Hernandes','Sorto','','Masculino',50,'05555555-5','2113-213131-313-5','54545554454','2003-01-04',15);
/*!40000 ALTER TABLE `datos_personales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialidad`
--

DROP TABLE IF EXISTS `especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `especialidad` (
  `id_especialidad` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_especialidad` varchar(45) NOT NULL,
  `especialidad` varchar(250) NOT NULL,
  PRIMARY KEY (`id_especialidad`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialidad`
--

LOCK TABLES `especialidad` WRITE;
/*!40000 ALTER TABLE `especialidad` DISABLE KEYS */;
INSERT INTO `especialidad` VALUES (1,'Especialidad Clínica','Alergología'),(2,'Especialidad Clínica','Anestesiología y reanimación'),(3,'Especialidad Clínica','Cardiología'),(4,'Especialidad Clínica','Gastroenterología'),(5,'Especialidad Clínica','Endocrinología'),(6,'Especialidad Clínica','Geriatría'),(7,'Especialidad Clínica','Hematología y hemoterapia'),(8,'Especialidad Clínica','Infectología'),(9,'Especialidad Clínica','Medicina aeroespacial'),(10,'Especialidad Clínica','Medicina del deporte'),(11,'Especialidad Clínica','Medicina del trabajo'),(12,'Especialidad Clínica','Medicina de urgencias'),(13,'Especialidad Clínica','Medicina familiar y comunitaria'),(14,'Especialidad Clínica','Medicina física y rehabilitación'),(15,'Especialidad Clínica','Medicina intensiva'),(16,'Especialidad Clínica','Medicina interna'),(17,'Especialidad Clínica','Medicina legal y forense'),(18,'Especialidad Clínica','Medicina preventiva y salud pública'),(19,'Especialidad Clínica','Medicina veterinaria'),(20,'Especialidad Clínica','Nefrología'),(21,'Especialidad Clínica','Neumología'),(22,'Especialidad Clínica','Neurología'),(23,'Especialidad Clínica','Nutriología'),(24,'Especialidad Clínica','Oftalmología'),(25,'Especialidad Clínica','Oncología médica'),(26,'Especialidad Clínica','Oncología radioterápica'),(27,'Especialidad Clínica','Pediatría'),(28,'Especialidad Clínica','Psiquiatría'),(29,'Especialidad Clínica','Rehabilitación'),(30,'Especialidad Clínica','Reumatología'),(31,'Especialidad Clínica','Toxicología'),(32,'Especialidad Clínica','Urología'),(33,'Especialidad Quirurgica','Cirugía cardiovascular'),(34,'Especialidad Quirurgica','Cirugía general y del aparato digestivo'),(35,'Especialidad Quirurgica','Cirugía oral y maxilofacial'),(36,'Especialidad Quirurgica','Cirugía ortopédica y traumatología'),(37,'Especialidad Quirurgica','Cirugía pediátrica'),(38,'Especialidad Quirurgica','Cirugía plástica, estética y reparadora'),(39,'Especialidad Quirurgica','Cirugía torácica'),(40,'Especialidad Quirurgica','Neurocirugía'),(41,'Especialidad Quirurgica','Proctología'),(42,'Especialidad Medico-quirurgicas','Angiología y cirugía vascular'),(43,'Especialidad Medico-quirurgicas','Dermatología médico-quirúrgica y venereología'),(44,'Especialidad Medico-quirurgicas','Odontología'),(45,'Especialidad Medico-quirurgicas','Ginecología y obstetricia o tocología'),(46,'Especialidad Medico-quirurgicas','Oftalmología'),(47,'Especialidad Medico-quirurgicas','Otorrinolaringología'),(48,'Especialidad Medico-quirurgicas','Urología'),(49,'Especialidad Medico-quirurgicas','Traumatología'),(50,'Especialidad de Laboratorio','Análisis clínicos'),(51,'Especialidad de Laboratorio','Anatomía patológica'),(52,'Especialidad de Laboratorio','Bioquímica clínica'),(53,'Especialidad de Laboratorio','Farmacología clínica'),(54,'Especialidad de Laboratorio','Genética médica'),(55,'Especialidad de Laboratorio','Inmunología'),(56,'Especialidad de Laboratorio','Medicina nuclear'),(57,'Especialidad de Laboratorio','Microbiología y parasitología'),(58,'Especialidad de Laboratorio','Neurofisiología clínica'),(59,'Especialidad de Laboratorio','Radiodiagnóstico o radiología');
/*!40000 ALTER TABLE `especialidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `estado` (
  `id_estado` int(2) NOT NULL AUTO_INCREMENT,
  `estado` varchar(30) NOT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (7,'Activo'),(8,'Inactivo'),(9,'Ausente'),(10,'Permiso por Maternidad'),(11,'Permiso por Enfermedad');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examen`
--

DROP TABLE IF EXISTS `examen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `examen` (
  `id_examen` int(11) NOT NULL AUTO_INCREMENT,
  `examen` int(11) NOT NULL,
  `fecha_examen` date NOT NULL,
  `resultado_examen` varchar(300) NOT NULL,
  PRIMARY KEY (`id_examen`),
  KEY `tipo_examen` (`examen`),
  CONSTRAINT `examen_ibfk_1` FOREIGN KEY (`examen`) REFERENCES `tipo_examen` (`id_tipo_examen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examen`
--

LOCK TABLES `examen` WRITE;
/*!40000 ALTER TABLE `examen` DISABLE KEYS */;
/*!40000 ALTER TABLE `examen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expediente`
--

DROP TABLE IF EXISTS `expediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `expediente` (
  `codigo_expediente` varchar(11) NOT NULL,
  `datos_personales` int(11) NOT NULL,
  PRIMARY KEY (`codigo_expediente`),
  KEY `datos_personales` (`datos_personales`),
  CONSTRAINT `expediente_ibfk_1` FOREIGN KEY (`datos_personales`) REFERENCES `datos_personales` (`id_datos_personales`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expediente`
--

LOCK TABLES `expediente` WRITE;
/*!40000 ALTER TABLE `expediente` DISABLE KEYS */;
INSERT INTO `expediente` VALUES ('1-97-RA',1),('2-97-RH',2),('3-97-AL',3),('4-18-WA',4),('5-03-RH',5),('6-03-CH',6);
/*!40000 ALTER TABLE `expediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `habitacion`
--

DROP TABLE IF EXISTS `habitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `habitacion` (
  `id_habitacion` int(11) NOT NULL AUTO_INCREMENT,
  `numero_habitacion` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `tipo_habitacion` varchar(15) NOT NULL,
  `numero_camas` int(2) NOT NULL,
  `camas_vacias` int(2) DEFAULT NULL,
  `camas_ocupadas` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_habitacion`),
  KEY `area` (`area`),
  KEY `tipo_habitacion` (`tipo_habitacion`),
  CONSTRAINT `habitacion_ibfk_1` FOREIGN KEY (`area`) REFERENCES `zona` (`id_zona`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `habitacion`
--

LOCK TABLES `habitacion` WRITE;
/*!40000 ALTER TABLE `habitacion` DISABLE KEYS */;
INSERT INTO `habitacion` VALUES (1,1,3,'privada',1,1,NULL),(2,2,3,'privada',1,1,NULL),(3,3,3,'semi privada',2,2,NULL),(4,4,3,'semi privada',2,2,NULL),(5,5,3,'semi privada',2,2,NULL),(6,1,4,'general',22,22,NULL),(7,1,5,'suite',1,1,NULL),(8,2,5,'privada',1,1,NULL),(9,3,5,'privada',1,1,NULL),(10,4,5,'privada',1,1,NULL),(11,5,5,'privada',1,1,NULL),(12,6,5,'semi privada',2,2,NULL),(13,7,5,'semi privada',2,2,NULL),(14,8,5,'semi privada',2,2,NULL),(15,9,5,'semi privada',2,2,NULL),(16,10,5,'semi privada',2,2,NULL),(17,11,5,'general',35,35,NULL),(18,1,7,'suite',1,1,NULL),(19,2,7,'privada',1,1,NULL),(20,3,7,'privada',1,1,NULL),(21,4,7,'privada',1,1,NULL),(22,5,7,'semi privada',2,2,NULL),(23,6,7,'semi privada',2,2,NULL),(24,7,7,'semi privada',2,2,NULL),(25,8,7,'semi privada',2,2,NULL),(26,9,7,'semi privada',2,2,NULL),(27,10,7,'general',22,22,NULL),(28,1,8,'privada',1,1,NULL),(29,2,8,'privada',1,1,NULL),(30,3,8,'semi privada',2,2,NULL),(31,4,8,'semi privada',2,2,NULL),(32,5,8,'semi privada',2,2,NULL),(33,6,8,'semi privada',2,2,NULL),(34,1,10,'suite',1,1,NULL),(35,2,10,'privada',1,1,NULL),(36,3,10,'privada',1,1,NULL),(37,4,10,'privada',1,1,NULL),(38,5,10,'privada',1,1,NULL),(39,6,10,'semi privada',2,2,NULL),(40,7,10,'semi privada',2,2,NULL),(41,8,10,'semi privada',2,2,NULL),(42,9,10,'semi privada',2,2,NULL),(43,10,10,'semi privada',2,2,NULL),(44,11,10,'general',35,35,NULL),(45,1,11,'privada',1,1,NULL),(46,2,11,'privada',1,1,NULL),(47,3,11,'privada',1,1,NULL),(48,4,11,'privada',1,1,NULL),(49,5,11,'semi privada',2,2,NULL),(50,6,11,'semi privada',2,2,NULL),(51,7,11,'semi privada',2,2,NULL),(52,8,11,'semi privada',2,2,NULL),(53,9,11,'semi privada',2,2,NULL),(54,10,11,'semi privada',2,2,NULL),(55,1,12,'suite',1,1,NULL),(56,2,12,'suite',1,1,NULL),(57,3,12,'privada',1,1,NULL),(58,4,12,'privada',1,1,NULL),(59,5,12,'privada',1,1,NULL),(60,6,12,'privada',1,1,NULL),(61,7,12,'privada',1,1,NULL),(62,8,12,'privada',1,1,NULL),(63,9,12,'privada',1,1,NULL),(64,10,12,'privada',1,1,NULL),(65,11,12,'semi privada',2,2,NULL),(66,12,12,'semi privada',2,2,NULL),(67,13,12,'semi privada',2,2,NULL),(68,14,12,'semi privada',2,2,NULL),(69,15,12,'semi privada',2,2,NULL),(70,16,12,'semi privada',2,2,NULL),(71,17,12,'semi privada',2,2,NULL),(72,18,12,'general',40,40,NULL),(73,1,13,'general',13,13,NULL),(74,1,14,'privada',1,1,NULL),(75,2,14,'privada',1,1,NULL),(76,3,14,'privada',1,1,NULL),(77,4,14,'privada',1,1,NULL),(78,5,14,'general',9,9,NULL);
/*!40000 ALTER TABLE `habitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `horario` (
  `id_horario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `internamiento`
--

DROP TABLE IF EXISTS `internamiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `internamiento` (
  `id_internamiento` int(11) NOT NULL AUTO_INCREMENT,
  `habitacion` int(11) NOT NULL,
  `numero_de_cama` int(3) NOT NULL,
  `hora_ingreso` time NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `hora_salida` time DEFAULT NULL,
  `fecha_salida` date DEFAULT NULL,
  PRIMARY KEY (`id_internamiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `internamiento`
--

LOCK TABLES `internamiento` WRITE;
/*!40000 ALTER TABLE `internamiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `internamiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicamento`
--

DROP TABLE IF EXISTS `medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `medicamento` (
  `id_medicamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_medicamento` varchar(50) NOT NULL,
  `via_de_administracion` varchar(50) NOT NULL,
  `cantidad_contenidad` varchar(8) NOT NULL,
  `laboratorio` varchar(50) NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `lote` int(11) NOT NULL,
  `cantidad_disponible` int(11) NOT NULL,
  PRIMARY KEY (`id_medicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicamento`
--

LOCK TABLES `medicamento` WRITE;
/*!40000 ALTER TABLE `medicamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_pais` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Afganistán'),(2,'Albania'),(3,'Alemania'),(4,'Andorra'),(5,'Angola'),(6,'Antigua y Barbuda'),(7,'Arabia Saudita'),(8,'Argelia'),(9,'Argentina'),(10,'Armenia'),(11,'Australia'),(12,'Austria'),(13,'Azerbaiyán'),(14,'Bahamas'),(15,'Bangladés'),(16,'Barbados'),(17,'Baréin'),(18,'Bélgica'),(19,'Belice'),(20,'Benín'),(21,'Bielorrusia'),(22,'Birmania'),(23,'Bolivia'),(24,'Bosnia y Herzegovina'),(25,'Botsuana'),(26,'Brasil'),(27,'Bulgaria'),(28,'Burkina Faso'),(29,'Bután'),(30,'Cabo Verde'),(31,'Camboya'),(32,'Cánada'),(33,'Catar'),(34,'Chile'),(35,'China'),(36,'Chipre'),(37,'Ciudad del Vaticano'),(38,'Colombia'),(39,'Comoras'),(40,'Corea del Norte'),(41,'Corea del Sur'),(42,'Costa de Marfil'),(43,'Costa Rica'),(44,'Croacia'),(45,'Cuba'),(46,'Dinamarca'),(47,'Dominicana'),(48,'Ecuador'),(49,'Egipto'),(50,'El Salvador'),(51,'Emiratos Árabes Unidos'),(52,'Guatemala'),(53,'Honduras'),(54,'Panama'),(55,'Mexico'),(56,'Peru'),(57,'Bolivia'),(58,'Uruguay'),(59,'Venezuela'),(60,'prueba'),(62,'prueba2');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_especialidad`
--

DROP TABLE IF EXISTS `perfil_especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `perfil_especialidad` (
  `id_perfil_especialidad` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `especialidad` int(11) NOT NULL,
  PRIMARY KEY (`id_perfil_especialidad`),
  KEY `especialidad` (`especialidad`),
  KEY `perfil_especialidad_ibfk_1` (`usuario`),
  CONSTRAINT `perfil_especialidad_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `perfil_personal` (`usuario`),
  CONSTRAINT `perfil_especialidad_ibfk_2` FOREIGN KEY (`especialidad`) REFERENCES `especialidad` (`id_especialidad`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_especialidad`
--

LOCK TABLES `perfil_especialidad` WRITE;
/*!40000 ALTER TABLE `perfil_especialidad` DISABLE KEYS */;
INSERT INTO `perfil_especialidad` VALUES (2,'PRUEBA',2),(4,'AngAntLo',3),(5,'AngAntLo',4),(6,'PRUEBA',5),(7,'AngAntLo',59),(8,'PRUEBA',59),(9,'AngAntLo',8),(10,'AngAntLo',32),(11,'AngAntLo',6),(13,'AngAntLo',59),(14,'AngAntLo',55),(15,'AngAntLo',22);
/*!40000 ALTER TABLE `perfil_especialidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_personal`
--

DROP TABLE IF EXISTS `perfil_personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `perfil_personal` (
  `usuario` varchar(20) NOT NULL,
  `pass` varchar(16) NOT NULL,
  `primer_nombre` varchar(25) NOT NULL,
  `segundo_nombre` varchar(25) NOT NULL,
  `tercer_nombre` varchar(25) DEFAULT NULL,
  `primer_apellido` varchar(24) NOT NULL,
  `segundo_apellido` varchar(24) DEFAULT NULL,
  `tercer_apellido` varchar(24) DEFAULT NULL,
  `edad` int(3) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `pais` int(11) NOT NULL,
  `dui` varchar(12) DEFAULT NULL,
  `nit` varchar(20) DEFAULT NULL,
  `pasaporte` varchar(12) DEFAULT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `celular` varchar(9) NOT NULL,
  `rol` int(2) NOT NULL,
  `fecha_de_contratacion` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `estado` int(2) NOT NULL,
  PRIMARY KEY (`usuario`),
  KEY `estado` (`estado`),
  KEY `rol` (`rol`),
  KEY `pais_idx` (`pais`),
  CONSTRAINT `pais` FOREIGN KEY (`pais`) REFERENCES `pais` (`id_pais`),
  CONSTRAINT `perfil_personal_ibfk_1` FOREIGN KEY (`estado`) REFERENCES `estado` (`id_estado`),
  CONSTRAINT `perfil_personal_ibfk_2` FOREIGN KEY (`rol`) REFERENCES `rol` (`id_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_personal`
--

LOCK TABLES `perfil_personal` WRITE;
/*!40000 ALTER TABLE `perfil_personal` DISABLE KEYS */;
INSERT INTO `perfil_personal` VALUES ('1','1','1','1','1','1','1','1',1,'Masculino',1,'11111111-1','1111-111111-111-1','1','1','1','1',14,'0001-01-01','1',8),('admin','123','123','123','123','0','0','0',0,'Masculino',50,'00000000-0','0000-000000-000-0','00','0','0000-0000','0000-0000',14,'2018-12-17','000',7),('AngAntLo','123','Angel','Antonio','','Lopez','Sorto','',19,'Masculino',50,'00000000-0','9465-270299-502-0','102853','lourdes colon la libertad','23181996','72630679',15,'2000-02-01','yelito1999@gmail.com',7),('Enfermera','123','prueba','prueba','prueba','prueba','prueba','prueba',18,'Masculino',50,'00000000-0','0000-000000-000-0','000','000','0000-0000','0000-0000',16,'2018-12-02','yelito1999@gmail.com',7),('Laboratorio','123','prueba','prueba','prueba','prueba','prueba','prueba',18,'Masculino',50,'00000000-0','0000-000000-000-0','000','000','0000-0000','0000-0000',18,'2018-12-02','yelito1999@gmail.com',7),('PRUEBA','123','PRUEBA','PRUEBA','PRUEBA','PRUEBA','PRUEBA','PRUEBA',7,'Masculino',50,'00000000-0','00000000-0','PRUEBA','PRUEBA','PRUEBA','PRUEBA',16,'2018-01-01','yelito1999@gmail.com',7),('qqqqqqqq','123','qqqqq','qqqq','qqqq','qqqq','qqqq','000',0,'Masculino',50,'00000000-0','0000-000000-000-0','0000','000','0000-0000','0000-0000',14,'2018-12-02','yelito1999@gmail.com',7),('Recepcion','123','prueba','prueba','prueba','prueba','prueba','prueba',18,'Masculino',50,'00000000-0','0000-000000-000-0','000','000','0000-0000','0000-0000',17,'2018-12-02','yelito1999@gmail.com',7);
/*!40000 ALTER TABLE `perfil_personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `prueba_vista`
--

DROP TABLE IF EXISTS `prueba_vista`;
/*!50001 DROP VIEW IF EXISTS `prueba_vista`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `prueba_vista` AS SELECT 
 1 AS `usuario`,
 1 AS `pass`,
 1 AS `primer_nombre`,
 1 AS `segundo_nombre`,
 1 AS `tercer_nombre`,
 1 AS `primer_apellido`,
 1 AS `segundo_apellido`,
 1 AS `tercer_apellido`,
 1 AS `edad`,
 1 AS `sexo`,
 1 AS `pais`,
 1 AS `dui`,
 1 AS `nit`,
 1 AS `pasaporte`,
 1 AS `direccion`,
 1 AS `telefono`,
 1 AS `celular`,
 1 AS `rol`,
 1 AS `fecha_de_contratacion`,
 1 AS `email`,
 1 AS `estado`,
 1 AS `id_pais`,
 1 AS `nombre_pais`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `responsable`
--

DROP TABLE IF EXISTS `responsable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `responsable` (
  `id_responsable` int(11) NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(25) NOT NULL,
  `segundo_nombre` varchar(25) NOT NULL,
  `tercer_nombre` varchar(25) DEFAULT NULL,
  `primer_apellido` varchar(24) NOT NULL,
  `segundo_apellido` varchar(24) DEFAULT NULL,
  `tercer_apellido` varchar(24) DEFAULT NULL,
  `edad` int(3) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `pais` varchar(30) NOT NULL,
  `dui` varchar(12) DEFAULT NULL,
  `nit` varchar(20) DEFAULT NULL,
  `pasaporte` varchar(12) DEFAULT NULL,
  `parentesco` varchar(50) DEFAULT NULL,
  `codigo_expediente` varchar(11) NOT NULL,
  PRIMARY KEY (`id_responsable`),
  KEY `codigo_expediente` (`codigo_expediente`),
  CONSTRAINT `responsable_ibfk_1` FOREIGN KEY (`codigo_expediente`) REFERENCES `expediente` (`codigo_expediente`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsable`
--

LOCK TABLES `responsable` WRITE;
/*!40000 ALTER TABLE `responsable` DISABLE KEYS */;
INSERT INTO `responsable` VALUES (1,'qqq','qqq','qqq','qqq','qqq','qqq',22,'qqq','qqq','qqq','qqq','qqq','qqq','6-03-CH');
/*!40000 ALTER TABLE `responsable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rol` (
  `id_rol` int(2) NOT NULL AUTO_INCREMENT,
  `rol` varchar(20) NOT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (14,'Administrador'),(15,'Medico'),(16,'Enfermera'),(17,'Recepcion'),(18,'Laboratorio');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_cita`
--

DROP TABLE IF EXISTS `tipo_cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipo_cita` (
  `id_tipo_cita` int(2) NOT NULL AUTO_INCREMENT,
  `tipo_cita` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipo_cita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_cita`
--

LOCK TABLES `tipo_cita` WRITE;
/*!40000 ALTER TABLE `tipo_cita` DISABLE KEYS */;
INSERT INTO `tipo_cita` VALUES (1,'Programada'),(2,'No Programada'),(3,'Emergencia'),(4,'Condicionada');
/*!40000 ALTER TABLE `tipo_cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_examen`
--

DROP TABLE IF EXISTS `tipo_examen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipo_examen` (
  `id_tipo_examen` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_examen` varchar(150) NOT NULL,
  `tipo_examen` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipo_examen`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_examen`
--

LOCK TABLES `tipo_examen` WRITE;
/*!40000 ALTER TABLE `tipo_examen` DISABLE KEYS */;
INSERT INTO `tipo_examen` VALUES (1,'AST o SGOT','Análisis de sangre'),(2,'fósforo','Análisis de sangre'),(3,'hemograma completo','Análisis de sangre'),(4,'LDH','Análisis de sangre'),(5,'magnesio','Análisis de sangre'),(6,'panel metabólico ampliado','Análisis de sangre'),(7,'panel metabólico básico','Análisis de sangre'),(8,'TTP','Análisis de sangre'),(9,'velocidad de sedimentación de los eritrocitos','Análisis de sangre'),(10,'ácido úrico','Análisis de sangre'),(11,'función hepática (hígado)','Análisis de sangre'),(12,'Cultivo de sangre','Análisis de sangre'),(13,'inmunoglobulina A (IgA)','Análisis de sangre'),(14,'inmunoglobulina E (IgE)','Análisis de sangre'),(15,'inmunoglobulina E (IgE) alérgeno específico','Análisis de sangre'),(16,'inmunoglobulinas (IgA, IgG, IgM)','Análisis de sangre'),(17,'insulina','Análisis de sangre'),(18,'hemoglobina A1c','Análisis de sangre'),(19,'hemoglobina','Análisis de sangre'),(20,'glucosa','Análisis de sangre'),(21,'inmunoglobulina A (IgA)','Análisis de sangre'),(22,'inmunoglobulina E (IgE)','Análisis de sangre'),(23,'inmunoglobulina E (IgE) alérgeno específico','Análisis de sangre'),(24,'inmunoglobulinas (IgA, IgG, IgM)','Análisis de sangre'),(25,'insulina','Análisis de sangre'),(26,'hemoglobina A1c','Análisis de sangre'),(27,'hemoglobina','Análisis de sangre'),(28,'glucosa','Análisis de sangre'),(29,'Electrocardiograma (ECG)','Otros'),(30,'Electroencefalograma (EEG)','Otros'),(31,'análisis de cloruro en el sudor','Fibrosis quística'),(32,'Análisis de heces','Otros'),(33,'Tomografía computada: abdomen','Tomografía computada'),(34,'Tomografía computada: cabeza','Tomografía computada'),(35,'Tomografía computada: cuello','Tomografía computada'),(36,'Tomografía computada: tórax','Tomografía computada'),(37,'Ultrasonido: abdomen','nálisis de heces'),(38,' análisis de 24 horas','Análisis de orina'),(39,' cultivos de rutina','Análisis de Orina'),(40,'Cribado múltiple','Examen prenatal'),(41,'Ecografía','Examen prenatal'),(42,' Muestra del vello coriónico','Examen prenatal'),(43,'antígeno de Giardia','Análisis de Heces'),(44,'antígeno de H. pylori','Análisis de Heces'),(45,'cultivos bacteriano','Análisis de Heces'),(46,'huevos y parásitos','Análisis de Heces'),(47,'toxina C. difficile','Análisis de Heces'),(48,'sangre oculta','Análisis de Heces'),(49,'RATE','Auditivo'),(50,'Prueba estreptocócica rápida','Prueba estreptocócica'),(51,'cultivo de exudado faríngeo','Prueba estreptocócica'),(52,'estudio de la edad ósea','Radiografía'),(53,'Radiografía: cuello','Radiografía'),(54,'longitud de miembros inferiores','Radiografía'),(55,'Radiografía: tibia y peroné','Radiografía'),(56,'Resonancia magnética fetal','Resonancia magnética'),(57,'Resonancia magnética: columna cervical','Resonancia magnética'),(58,'Ultrasonido: abdomen','Ultrasonido'),(59,'Ultrasonido: cabeza','Ultrasonido'),(60,'Ultrasonido: escroto','Ultrasonido'),(61,'Ultrasonido: vejiga','Ultrasonido');
/*!40000 ALTER TABLE `tipo_examen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zona`
--

DROP TABLE IF EXISTS `zona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zona` (
  `id_zona` int(11) NOT NULL AUTO_INCREMENT,
  `piso_area` varchar(15) NOT NULL,
  `area` varchar(40) NOT NULL,
  PRIMARY KEY (`id_zona`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zona`
--

LOCK TABLES `zona` WRITE;
/*!40000 ALTER TABLE `zona` DISABLE KEYS */;
INSERT INTO `zona` VALUES (1,'Planta Baja','Consulta Externa'),(2,'Planta Baja','Fisiatría'),(3,'Planta Baja','Cardiología'),(4,'Planta Baja','Emergencías'),(5,'Primer Piso','Pediaría'),(6,'Segundo Piso','Ginecología'),(7,'Segundo Piso','Obstetricia'),(8,'Segundo Piso','Perinatología'),(9,'Segundo Piso','Neonatos'),(10,'Tercer Piso','Medicina Interna'),(11,'Tercer Piso','Neurologia'),(12,'Cuarto Piso','Cirugía'),(13,'Quinto Piso','Recuperación'),(14,'Quinto Piso','UCI'),(15,'Planta Baja','Consulta Externa'),(16,'Planta Baja','Fisiatría'),(17,'Planta Baja','Cardiología'),(18,'Planta Baja','Emergencías'),(19,'Primer Piso','Pediaría'),(20,'Segundo Piso','Ginecología'),(21,'Segundo Piso','Obstetricia'),(22,'Segundo Piso','Perinatología'),(23,'Segundo Piso','Neonatos'),(24,'Tercer Piso','Medicina Interna'),(25,'Tercer Piso','Neurologia'),(26,'Cuarto Piso','Cirugía'),(27,'Quinto Piso','Recuperación'),(28,'Quinto Piso','UCI'),(29,'Planta Baja','Consulta Externa'),(30,'Planta Baja','Fisiatría'),(31,'Planta Baja','Cardiología'),(32,'Planta Baja','Emergencías'),(33,'Primer Piso','Pediaría'),(34,'Segundo Piso','Ginecología'),(35,'Segundo Piso','Obstetricia'),(36,'Segundo Piso','Perinatología'),(37,'Segundo Piso','Neonatos'),(38,'Tercer Piso','Medicina Interna'),(39,'Tercer Piso','Neurologia'),(40,'Cuarto Piso','Cirugía'),(41,'Quinto Piso','Recuperación'),(42,'Quinto Piso','UCI');
/*!40000 ALTER TABLE `zona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `prueba_vista`
--

/*!50001 DROP VIEW IF EXISTS `prueba_vista`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `prueba_vista` AS select `perfil_personal`.`usuario` AS `usuario`,`perfil_personal`.`pass` AS `pass`,`perfil_personal`.`primer_nombre` AS `primer_nombre`,`perfil_personal`.`segundo_nombre` AS `segundo_nombre`,`perfil_personal`.`tercer_nombre` AS `tercer_nombre`,`perfil_personal`.`primer_apellido` AS `primer_apellido`,`perfil_personal`.`segundo_apellido` AS `segundo_apellido`,`perfil_personal`.`tercer_apellido` AS `tercer_apellido`,`perfil_personal`.`edad` AS `edad`,`perfil_personal`.`sexo` AS `sexo`,`perfil_personal`.`pais` AS `pais`,`perfil_personal`.`dui` AS `dui`,`perfil_personal`.`nit` AS `nit`,`perfil_personal`.`pasaporte` AS `pasaporte`,`perfil_personal`.`direccion` AS `direccion`,`perfil_personal`.`telefono` AS `telefono`,`perfil_personal`.`celular` AS `celular`,`perfil_personal`.`rol` AS `rol`,`perfil_personal`.`fecha_de_contratacion` AS `fecha_de_contratacion`,`perfil_personal`.`email` AS `email`,`perfil_personal`.`estado` AS `estado`,`pais`.`id_pais` AS `id_pais`,`pais`.`nombre_pais` AS `nombre_pais` from (`perfil_personal` join `pais` on((`perfil_personal`.`pais` = `pais`.`id_pais`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-20 13:55:59
