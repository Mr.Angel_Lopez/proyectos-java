/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.util.Iterator;
import java.util.List;
import persistencia.PerfilPersonal;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author alejandra.centeno
 */
public class mantenimientoLogin {

//    public static void main(String[] args) {
//        mantenimientoLogin log = new mantenimientoLogin();
//        PerfilPersonal l=log.consultarLogin("AngAntLo");
//        System.out.println("\n\n\n\n"+l.getPass()+"\n\n\n\n");
//    }

    public PerfilPersonal consultarTodoLogin(String usuario, String pass) {
        List<PerfilPersonal> listaLogin = null;
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        PerfilPersonal l = null;

        try {
            Query query = em.createQuery("SELECT p FROM PerfilPersonal p WHERE p.usuario LIKE ?1 AND p.pass LIKE ?2");
//            SELECT p FROM PerfilPersonal p WHERE p.primerNombre = :primerNombre
            query.setParameter(1, usuario);
            query.setParameter(2, pass);

            List<PerfilPersonal> resultados = query.getResultList();
            for (PerfilPersonal resultado : resultados) {
                l = resultado;
            }

        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.clear();
            em.close();
        }
        return l;
    }

    public PerfilPersonal consultarLogin(String usuario) {
        List<PerfilPersonal> listaLogin = null;
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        PerfilPersonal l = null;

        try {
            Query query = em.createQuery("SELECT p FROM PerfilPersonal p WHERE p.usuario LIKE ?1");
//            SELECT p FROM PerfilPersonal p WHERE p.usuario = 
            query.setParameter(1, usuario);
            List<PerfilPersonal> resultados = query.getResultList();
            for (PerfilPersonal resultado : resultados) {
                l = resultado;
            }

        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.clear();
            em.close();
        }
        return l;
    }

    public boolean edit(PerfilPersonal login) {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        try {
            em.getTransaction().begin();
            login = em.merge(login);
            em.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            System.out.println("Error en \n\n" + ex);
            return false;
        } finally {
            em.clear();
            em.close();

        }
    }
    
    

}
