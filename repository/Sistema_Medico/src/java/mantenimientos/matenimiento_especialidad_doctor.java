package mantenimientos;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedList;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import java.util.List;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import persistencia.Especialidad;
import persistencia.PerfilEspecialidad;
import persistencia.PerfilPersonal;

public class matenimiento_especialidad_doctor {
    //traer una lista con la tabla de los doctores

    //<editor-fold defaultstate="collapsed" desc="lista de especialidad">
    public List<Especialidad> listadeespecialidad() {
        List<Especialidad> lista = null;
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        try {
            javax.persistence.criteria.CriteriaQuery query = em.getCriteriaBuilder().createQuery();
            em.getTransaction().commit();
            query.select(query.from(Especialidad.class));
            lista = em.createQuery(query).getResultList();
            if (lista == null || lista.isEmpty()) {
                System.out.println("esta vacio,ves no hay datos aqui :T");
            } else {
                System.out.println("Funciona");
            }
        } catch (Exception e) {
            System.out.println("ERROR EN :" + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }
//</editor-fold>

//      <editor-fold defaultstate="collapsed" desc="mostrar doctores">
    public List<PerfilPersonal> doctores(int especialidad) {
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        perfilpersonalmantenimiento p= new perfilpersonalmantenimiento();
        List<Object[]> lista = null;
        List<PerfilPersonal> listas = new LinkedList<>();
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        try {
            StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("doctorespecialidades");
            storedProcedure.registerStoredProcedureParameter("especialidad", int.class, ParameterMode.IN);
            storedProcedure.setParameter("especialidad", especialidad);
            storedProcedure.execute();
            em.getTransaction().commit();
            lista = storedProcedure.getResultList();
            System.out.println(lista);
            if (lista == null || lista.isEmpty()) {
                System.out.println("esta vacio,ves no hay datos aqui :T");
            } else {
                System.out.println("Se lleno el dato");
//                for (Iterator<Object[]> iterator = lista.iterator(); iterator.hasNext();) {
//                    Object[] next = iterator.next();
//
//                }
                Iterator<Object[]> it = lista.iterator();
                int ij=1;
                while (it.hasNext()) {
                    int i=0;
                    Object[] array;
                    array = it.next();
                    PerfilPersonal obj= new PerfilPersonal();
                    obj.setUsuario(array[i++].toString());
                    obj.setPass(array[i++].toString());
                    obj.setPrimerNombre(array[i++].toString());
                    obj.setSegundoNombre(array[i++].toString());
                    obj.setTercerNombre(array[i++].toString());
                    obj.setPrimerApellido(array[i++].toString());
                    obj.setSegundoApellido(array[i++].toString());
                    obj.setTercerApellido(array[i++].toString());
                    obj.setEdad(Integer.parseInt(array[i++].toString()));
                    obj.setSexo(array[i++].toString());
                    obj.setPais(p.paisseleccionado(Integer.parseInt(array[i++].toString())));
                    obj.setDui(array[i++].toString());
                    obj.setNit(array[i++].toString());
                    obj.setPasaporte(array[i++].toString());
                    obj.setDireccion(array[i++].toString());
                    obj.setTelefono(array[i++].toString());
                    obj.setCelular(array[i++].toString());
                    obj.setRol(p.rollseleccionado(Integer.parseInt(array[i++].toString())));
                    obj.setFechaDeContratacion(sdf.parse(array[i++].toString()));
                    obj.setEmail(array[i++].toString());
                    obj.setEstado(p.estadoseleccionado(Integer.parseInt(array[i++].toString())));
                    listas.add(obj);
                }

            }
        } catch (Exception e) {
            System.err.println("ERROR SIGUIENTE: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return listas;
    }
//      </editor-fold>

    public static void main(String[] args) {
        matenimiento_especialidad_doctor mt = new matenimiento_especialidad_doctor();
        List<PerfilPersonal> doctore = mt.doctores(59);
        System.out.println("\n\n========================================================\n\n");
        for (PerfilPersonal perfilPersonal : doctore) {
            System.out.println("usuario :"+perfilPersonal.getUsuario());
            System.out.println("pass :"+perfilPersonal.getPass());
            System.out.println("Primer Nombre "+ perfilPersonal.getPrimerNombre());
            System.out.println("Segundo Nombre :"+perfilPersonal.getSegundoNombre());
            System.out.println("Tercer Nombre :"+perfilPersonal.getTercerNombre());
            System.out.println("Primer Apellido :"+perfilPersonal.getPrimerApellido());
            System.out.println("Segundo Apellido :"+perfilPersonal.getSegundoApellido());
            System.out.println("Tercer Apellido :"+perfilPersonal.getTercerApellido());
            System.out.println("Edad :"+perfilPersonal.getEdad());
            System.out.println("Sexo :"+perfilPersonal.getSexo());
            System.out.println("Pais :"+perfilPersonal.getPais().getNombrePais());
            System.out.println("Dui :"+perfilPersonal.getDui());
            System.out.println("Nit :"+perfilPersonal.getNit());
            System.out.println("Pasaporte :"+perfilPersonal.getPasaporte());
            System.out.println("Direccion :"+perfilPersonal.getDireccion());
            System.out.println("Telefono :"+perfilPersonal.getTelefono());
            System.out.println("Celular :"+perfilPersonal.getCelular());
            System.out.println("Rol :"+perfilPersonal.getRol().getRol());
            System.out.println("Fecha de Contratacion :"+perfilPersonal.getFechaDeContratacion());
            System.out.println("Email :"+perfilPersonal.getEmail());
            System.out.println("Estado :"+perfilPersonal.getEstado().getEstado());
            System.out.println("\n\n========================================================\n\n");
        }
        
    }
}
