/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import javax.persistence.EntityManager;
import persistencia.DatosPersonales;

/**
 *
 * @author angel.lopezusam
 */
public class mantenimientoDatosPersonales {

    public boolean crear(DatosPersonales dp) {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(dp);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        }
    }

}
