package mantenimientos;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import persistencia.Responsable;
import persistencia.Expediente;

public class mantenimiento_responsables {

    public int guardar_responsable(Responsable responsable) {
        int flag = 0;
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("transaccion iniciada");
        try {
            em.persist(responsable);
            em.getTransaction().commit();
            flag = 1;
            System.out.println("responsable guardado");
        } catch (Exception e) {
            System.out.println("\n\n\n=======================================================\n\n\n");
            System.err.println("ERROR EN: " + e);
            System.out.println("\n\n\n=======================================================\n\n\n");
            em.getTransaction().rollback();
        }finally{
            em.close();
            System.out.println("trasaccion finalizada");
        }
        return flag;
    }
    public int actualizar_responsable(Responsable responsable) {
        int flag = 0;
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("transaccion iniciada");
        try {
            em.merge(responsable);
            em.getTransaction().commit();
            flag = 1;
            System.out.println("responsable actualizado");
        } catch (Exception e) {
            System.out.println("\n\n\n=======================================================\n\n\n");
            System.err.println("ERROR EN: " + e);
            System.out.println("\n\n\n=======================================================\n\n\n");
            em.getTransaction().rollback();
        }finally{
            em.close();
            System.out.println("trasaccion finalizada");
        }
        return flag;
    }
    public Responsable MostrarResponsablePorID(int id){
        Responsable responsable = null;
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("transaccion iniciada");
        try {
            responsable = em.find(Responsable.class, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("\n\n\n=======================================================\n\n\n");
            System.err.println("ERROR EN: " + e);
            System.out.println("\n\n\n=======================================================\n\n\n");
            em.getTransaction().rollback();
        }finally{
            em.close();
            System.out.println("trasaccion finalizada");
        }
        return responsable;
    }
    public int EliminarResponasble(int id){
         int flag = 0;
        Responsable responsable = null;
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        System.out.println("transaccion iniciada");
        try {
            responsable = em.find(Responsable.class, id);
            em.remove(responsable);
            em.getTransaction().commit();
            flag = 1;
            System.out.println("responsable Eliminado");
        } catch (Exception e) {
            System.out.println("\n\n\n=======================================================\n\n\n");
            System.err.println("ERROR EN: " + e);
            System.out.println("\n\n\n=======================================================\n\n\n");
            em.getTransaction().rollback();
        }finally{
            em.close();
            System.out.println("trasaccion finalizada");
        }
        return flag;
    }
    public List<Responsable> MostrarResponsable(){
     List<Responsable> lista = null;
     EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
     em.getTransaction().begin();
         try {
             Query jpql = em.createQuery("SELECT r FROM Responsable r");
             lista = jpql.getResultList();
             em.getTransaction().commit();
         } catch (Exception e) {
             System.out.println("\n\n\n========================================\n\n\n");
             System.out.println("ERROR EN: "+e);
             System.out.println("\n\n\n========================================\n\n\n");
             em.getTransaction().rollback();
         }finally{
             em.close();
             System.out.println("transaccion finalizada");
         }
     
     return lista;
     }
}
// <editor-fold defaultstate="collapsed" desc="test">
//  public static void main(String[] args) {
//        mantenimiento_responsables n = new mantenimiento_responsables();
//        Responsable r = new Responsable();
//        Expediente e = new Expediente();
//        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
//        em.getTransaction().begin();
//        try {
//             e = em.find(Expediente.class, "6-03-CH");
//             em.getTransaction().commit();
//         } catch(Exception p){
//             em.getTransaction().rollback();
//         }
//        r.setIdResponsable(1);
//        r.setPrimerNombre("qqq");
//        r.setSegundoNombre("qqq");
//        r.setTercerNombre("qqq");
//        r.setPrimerApellido("qqq");
//        r.setSegundoApellido("qqq");
//        r.setTercerApellido("qqq");
//        r.setEdad(22);
//        r.setSexo("qqq");
//        r.setPais("qqq");
//        r.setDui("qqq");
//        r.setNit("qqq");
//        r.setPasaporte("qqq");
//        r.setParentesco("qqq");
//        r.setCodigoExpediente(e);
//        
//         System.out.println(n.MostrarResponsablePorID(1));
//        }
// </editor-fold>