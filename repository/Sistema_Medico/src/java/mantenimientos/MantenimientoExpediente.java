/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import persistencia.DatosPersonales;
import persistencia.Expediente;

/**
 *
 * @author angel.lopezusam
 */
public class MantenimientoExpediente {

    public boolean crear(Expediente expediente) {
        perfilpersonalmantenimiento p = new perfilpersonalmantenimiento();
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        try {
            String serial = serial(expediente.getDatosPersonales());
            expediente.setCodigoExpediente(serial);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("Codigo", serial);
            System.out.println("\n\n\n\n");
            System.out.println(expediente.toString());
            System.out.println("\n\n\n\n");
            em.persist(expediente);

            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        }
    }

    public String serial(DatosPersonales dp) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String s1 = "";
        String s2 = "";
        int i = 1;
        List<Expediente> es = Consultar();
        String[] s = sdf.format(dp.getFechaDeNacimiento()).split("-");
        for (Expediente expediente : es) {
            i++;
        }
        for (String sout : s) {
            s1 = s1 + sout;
        }
        s2 = i + "-" + s1.substring(2, 4) + "-" + dp.getPrimerNombre().substring(0, 1).toUpperCase() + dp.getPrimerApellido().substring(0, 1).toUpperCase();

        return s2;
    }

    public List<Expediente> Consultar() {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            em.getTransaction().commit();
            cq.select(cq.from(Expediente.class));
            return em.createQuery(cq).getResultList();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }
    }

}
