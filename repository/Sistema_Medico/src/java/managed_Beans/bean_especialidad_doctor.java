/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed_Beans;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.matenimiento_especialidad_doctor;
import persistencia.Especialidad;
import persistencia.PerfilPersonal;

/**
 *
 * @author angel.lopezusam
 */
@ManagedBean
@RequestScoped
public class bean_especialidad_doctor {

    private int opcion;

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    private List<PerfilPersonal> doctores;

    public List<PerfilPersonal> getDoctores() {
        return doctores;
    }

    private List<Especialidad> especialidades;

    public List<Especialidad> getEspecialidades() {
        return especialidades;
    }

    public void setEspecialidades(List<Especialidad> especialidades) {
        this.especialidades = especialidades;
    }

    public void funciondoctor() {
        matenimiento_especialidad_doctor a = new matenimiento_especialidad_doctor();
        this.doctores = a.doctores(opcion);
    }

    @PostConstruct
    public void init() {
        matenimiento_especialidad_doctor n = new matenimiento_especialidad_doctor();
        this.especialidades = n.listadeespecialidad();
    }

    public bean_especialidad_doctor() {
    }

}
