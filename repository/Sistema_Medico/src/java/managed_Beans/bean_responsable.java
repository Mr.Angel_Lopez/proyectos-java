/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed_Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author angel.lopezusam
 */
@ManagedBean
@RequestScoped
public class bean_responsable {

    /**
     * Creates a new instance of bean_responsable
     */
    public bean_responsable() {
    }
    
     // <editor-fold defaultstate="collapsed" desc=""> // </editor-fold>
       
    
    // <editor-fold defaultstate="collapsed" desc="valiables">
    private String primerNombrer;
    private String segundoNombrer;
    private String tercerNombrer;
    private String primerApellidor;
    private String segundoApellidor;
    private String tercerApellidor;
    private String edadr;
    private String sexor;
    private String paisr;
    private String duir;
    private String nitr;
    private String pasaporter;
    private String parentescor;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getters y setters">
public String getPrimerNombrer() {
return primerNombrer;}
public void setPrimerNombrer(String primerNombrer) {this.primerNombrer = primerNombrer;}
public String getSegundoNombrer() {return segundoNombrer;}
public void setSegundoNombrer(String segundoNombrer) {this.segundoNombrer = segundoNombrer;}
public String getTercerNombrer() {return tercerNombrer;}
public void setTercerNombrer(String tercerNombrer) {this.tercerNombrer = tercerNombrer;}
public String getPrimerApellidor() {return primerApellidor;}
public void setPrimerApellidor(String primerApellidor) {this.primerApellidor = primerApellidor;}
public String getSegundoApellidor() {return segundoApellidor;}
public void setSegundoApellidor(String segundoApellidor) {this.segundoApellidor = segundoApellidor;}
public String getTercerApellidor() {return tercerApellidor;}
public void setTercerApellidor(String tercerApellidor) {this.tercerApellidor = tercerApellidor;}
public String getEdadr() {return edadr;}
public void setEdadr(String edadr) {this.edadr = edadr;}
public String getSexor() {return sexor;}
public void setSexor(String sexor) {this.sexor = sexor;}
public String getPaisr() {return paisr;}
public void setPaisr(String paisr) {this.paisr = paisr;}
public String getDuir() {return duir;}
public void setDuir(String duir) {this.duir = duir;}
public String getNitr() {return nitr;}
public void setNitr(String nitr) {this.nitr = nitr;}
public String getPasaporter() { return pasaporter;}
public void setPasaporter(String pasaporter) {this.pasaporter = pasaporter;}
public String getParentescor() {return parentescor;}
public void setParentescor(String parentescor) {this.parentescor = parentescor;}
 // </editor-fold>


    //<editor-fold defaultstate="collapsed" desc="funcion insertar">
        
    //</editor-fold>

}
