/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed_Beans;

import java.io.IOException;
import java.util.UUID;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


import mail.Mensajes;
import mantenimientos.mantenimientoLogin;
import persistencia.PerfilPersonal;

/**
 *
 * @author alejandra.centeno
 */
@ManagedBean
@RequestScoped
public class BeanLogin {

    private String usuario;
    private String pass;
    private String correo;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    //Para checkbuttons en formulario
    String console;
    String rol;

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getConsole() {
        return console;
    }

    public void setConsole(String console) {
        this.console = console;
    }

    public void validar() {
        PerfilPersonal l = (PerfilPersonal) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Login");
        if (l.getPass().equals(pass)) {
            FacesMessage msg = new FacesMessage("Contraseña Corecta");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            FacesMessage msg = new FacesMessage("Error en contraseña");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
          FacesMessage msg = new FacesMessage("Seccion sin iniciar");
            FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void consultardatos() throws IOException {
        mantenimientoLogin ml = new mantenimientoLogin();
        PerfilPersonal l = ml.consultarTodoLogin(usuario, pass);
        String advertencia = "Sesion Iniciada";
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("Login", l);
        if (l != null) {

            this.usuario = l.getUsuario();
            this.pass = l.getPass();
            FacesMessage msg = new FacesMessage(advertencia);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            switch (l.getRol().getRol()) {
                case "Administrador":
                    FacesContext.getCurrentInstance().getExternalContext().redirect("admin.xhtml");
                    break;
            //FacesContext.getCurrentInstance().getExternalContext().redirect("admin.xhtml");
                case "Medico":
                    FacesContext.getCurrentInstance().getExternalContext().redirect("medico.xhtml");
                    break;
                case "Enfermera":
                    FacesContext.getCurrentInstance().getExternalContext().redirect("enfermeria.xhtml");
                    break;
                case "Recepcion":
                    FacesContext.getCurrentInstance().getExternalContext().redirect("Recepcion.xhtml");
                    break;
                case "Laboratorio":
                    FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                    break;
                default:
                    break;
            }
        } else {
            advertencia = "Usuario o Contraseña incorrectos";
        }
        FacesMessage msg = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Creates a new instance of BeanLogin
     *
     * public BeanLogin() { } public static void main(String[] args) {
     * mantenimientoLogin m=new mantenimientoLogin(); Login l=new Login();
     * l.setUsuario("a"); try { l=m.consultarTodoLogin("AC123", "123"); } catch
     * (Exception e) { System.out.println("Error en\n\n"+e.getMessage()); }
     *
     * System.out.println(l.getUsuario()); }
     */
    
//    public void recuperacion() throws MessagingException {
//        FacesMessage msg = new FacesMessage("verificanco si el usuario existe");
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//        mantenimientoLogin m = new mantenimientoLogin();
//        Login l = m.consultarLogin(usuario);
//        Mensajes createMensaje = new Mensajes();
//        Mail mensaje = new Mail();
//        if (l != null) {
//            msg = new FacesMessage("El usuario si existe " + l.getUsuario());
//            FacesContext.getCurrentInstance().addMessage(null, msg);
//            String pass = UUID.randomUUID().toString().toUpperCase().substring(0, 3);
//            l.setPass(pass);
//            if (m.edit(l)) {
//                createMensaje.h1Titulo("Recuperación de Contraseña");
//                createMensaje.Mensaje("Se han generado nueva credencial para que pueda inciciar seción");
//                createMensaje.Pass(pass);
//                createMensaje.h1Titulo(createMensaje.getPass());
//                createMensaje.Despedida("Ya tiene acceso a su cuenta que pase un buen dia");
//                msg = new FacesMessage("Enviando Mensaje ");
//                FacesContext.getCurrentInstance().addMessage(null, msg);
//                mensaje.EnvioMensaje(correo, createMensaje.Mensaje(), "Generacion de Contraseña");
//                msg = new FacesMessage("Mensaje Enviado ");
//                FacesContext.getCurrentInstance().addMessage(null, msg);
//            }
//        } else {
//            msg = new FacesMessage("El usuario no existe");
//            FacesContext.getCurrentInstance().addMessage(null, msg);
//        }
//    }
}
