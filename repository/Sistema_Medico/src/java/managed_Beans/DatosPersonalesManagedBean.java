/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed_Beans;

import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import mantenimientos.MantenimientoExpediente;
import mantenimientos.perfilpersonalmantenimiento;
import persistencia.DatosPersonales;
import persistencia.Expediente;
import persistencia.Pais;

/**
 *
 * @author angel.lopezusam
 */
@ManagedBean(name = "DatosPersonales")
@RequestScoped
public class DatosPersonalesManagedBean {

    public DatosPersonales datosPersonales;
    public List<Pais> pais;
    
    private final Map<String, Object> context ;

    public List<Pais> getPais() {
        return pais;
    }

    public void setPais(List<Pais> pais) {
        this.pais = pais;
    }

    public DatosPersonales getDatosPersonales() {
        return datosPersonales;
    }

    public void setDatosPersonales(DatosPersonales datosPersonales) {
        this.datosPersonales = datosPersonales;
    }

    @PostConstruct
    public void init() {
        perfilpersonalmantenimiento p = new perfilpersonalmantenimiento();
        this.pais = p.ConsultaPais();
        this.datosPersonales=new DatosPersonales(0);
        this.datosPersonales.setPais(new Pais());
    }

    public void guardar() {
        Expediente expediente = new Expediente();
        MantenimientoExpediente me = new MantenimientoExpediente();
        expediente.setDatosPersonales(datosPersonales);
        try {
            if (me.crear(expediente)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Se a generado el expediente :"+context.get("Codigo")));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "A la hora de generar el expediente"));
            }
        } catch (Exception e) {
            System.out.println("A ocurrido un error "+e.getMessage());
        }
    }

    
    /**
     * Creates a new instance of DatosPersonalesManagedBean
     */
    public DatosPersonalesManagedBean() {
        this.context = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

}
