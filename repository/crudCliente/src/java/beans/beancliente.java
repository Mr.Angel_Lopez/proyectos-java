/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.IOException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import matenimiento_cliente.mantenimientoCliente;
import persistencia.Clientes;

/**
 *
 * @author angel.lopezusam
 */
@ManagedBean
@RequestScoped
public class beancliente {

    /**
     * ************************************************************************
     */
    private int idCliente;
    private String nombreCliente;
    private String apellidoCliente;
    private String direccionCliente;
    private String telefonoCliente;
    private String mailCliente;

    /**
     * ************************************************************************
     */
    /**
     * ***************************************************************************
     */
    public beancliente() {
    }

    /**
     * @return the idCliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * @param  the idCliente to set
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the nombreCliente
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * @param  the nombreCliente to set
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * @return the apellidoCliente
     */
    public String getApellidoCliente() {
        return apellidoCliente;
    }

    /**
     * @param  the apellidoCliente to set
     */
    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    /**
     * @return the direccionCliente
     */
    public String getDireccionCliente() {
        return direccionCliente;
    }

    /**
     * @param  the direccionCliente to set
     */
    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    /**
     * @return the telefonoCliente
     */
    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    /**
     * @param  the telefonoCliente to set
     */
    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    /**
     * @return the mailCliente
     */
    public String getMailCliente() {
        return mailCliente;
    }

    /**
     * @param  the mailCliente to set
     */
    public void setMailCliente(String mailCliente) {
        this.mailCliente = mailCliente;
    }

    /**
     * ************************************************************************
     */
    private List<Clientes> listacliente;

    public List<Clientes> getListacliente() {
        mantenimientoCliente mc = new mantenimientoCliente();
        return mc.Mostrar_clientes();
    }

    public void eliminar(int id) {
        mantenimientoCliente mc = new mantenimientoCliente();
        mc.Eliminar_Cliente(id);
    }

    public void mostrarporid(int id) {
        mantenimientoCliente mc = new mantenimientoCliente();
        Clientes c = mc.Mostara_cliente_por_ID(id);
        String advertencia = "";
        if (c != null) {
            this.idCliente = c.getIdCliente();
            this.nombreCliente = c.getNombreCliente();
            this.apellidoCliente = c.getApellidoCliente();
            this.direccionCliente = c.getDireccionCliente();
            this.telefonoCliente = c.getTelefonoCliente();
            this.mailCliente = c.getMailCliente();
        } else {
            advertencia = "Datos no encontrados";
            FacesMessage msg = new FacesMessage(advertencia);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void actializarcliente() {
        mantenimientoCliente mc = new mantenimientoCliente();
        String advertencia = "";

        Clientes cliente = new Clientes();
        cliente.setNombreCliente(nombreCliente);
        cliente.setApellidoCliente(apellidoCliente);
        cliente.setDireccionCliente(direccionCliente);
        cliente.setTelefonoCliente(telefonoCliente);
        cliente.setMailCliente(mailCliente);
        cliente.setIdCliente(idCliente);
        if (mc.Actualizar_cliente(cliente) == 1) {
            advertencia = "Informacion actualizada Correctamente";
        } else {
            advertencia = "Error al ingresar informacion";
        }
        FacesMessage msg = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
//    public static void main(String[] args) throws IOException {
//        beancliente b = new beancliente();
////        b.setApellidoCliente("dsd");
////        b.setDireccionCliente("sds");
////        b.setIdCliente(5);
////        b.setMailCliente("hghgh");
////        b.setNombreCliente("klkljkl");
////        b.setTelefonoCliente("54564654654");
//        b.actializarcliente(b.idCliente);
//    }

    public void insertar() throws IOException {
        mantenimientoCliente mc = new mantenimientoCliente();
        Clientes cliente = new Clientes();
        cliente.setIdCliente(getIdCliente());
        cliente.setNombreCliente(getNombreCliente());
        cliente.setApellidoCliente(getApellidoCliente());
        cliente.setDireccionCliente(getDireccionCliente());
        cliente.setTelefonoCliente(getTelefonoCliente());
        cliente.setMailCliente(getMailCliente());
        if (mc.Insertar_Cliente(cliente) == 1) {
            this.setNombreCliente("");
            this.setApellidoCliente("");
            this.setDireccionCliente("");
            this.setTelefonoCliente("");
            this.setMailCliente("");
            FacesContext.getCurrentInstance().getExternalContext().redirect("mostrarcliente.xhtml");
        String advertencia = "Informacion guardada correctamente";
        FacesMessage msg = new FacesMessage(advertencia);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}
