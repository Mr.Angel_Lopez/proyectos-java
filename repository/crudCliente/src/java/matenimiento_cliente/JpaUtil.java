package matenimiento_cliente;

import javax.persistence.Persistence;
import javax.persistence.EntityManagerFactory;

public class JpaUtil {

    private static final EntityManagerFactory emf;

    static {
        try {
            emf = Persistence.createEntityManagerFactory("crudClientePU");
        } catch (Exception e) {
            System.err.println("creacion de secion fallo" + e);
            throw new ExceptionInInitializerError(e);
        }
    }
    public static EntityManagerFactory getEntityManagerFactory(){
        return emf;
    }

}
