/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matenimiento_cliente;

import java.util.List;
import persistencia.Clientes;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author angel.lopezusam
 */
public class mantenimientoCliente {

    // <editor-fold defaultstate="collapsed" desc="Insertar clientes">
    public int Insertar_Cliente(Clientes cliente) {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        int flag = 0;
        em.getTransaction().begin();
        try {
            em.persist(cliente);
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception ex) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return flag;
    }
    // </editor-fold>   

    // <editor-fold defaultstate="collapsed" desc="Eliminar Clientes">
    public int Eliminar_Cliente(int id) {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        int flag = 0;
        Clientes cliente = null;
        em.getTransaction().begin();
        try {
            cliente = em.find(Clientes.class, id);
            em.remove(cliente);
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return flag;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Actualizar Clientes">
    public int Actualizar_cliente(Clientes cliente) {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        int flag = 0;
        em.getTransaction().begin();
        try {
            em.merge(cliente);
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }
        return flag;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Mostrar Clientes">
    public List<Clientes> Mostrar_clientes() {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        List<Clientes> listadodeclientes = null;
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("SELECT c FROM Clientes c");
            em.getTransaction().commit();
            listadodeclientes = query.getResultList();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return listadodeclientes;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Mostrar Por ID Clientes">
    public Clientes Mostara_cliente_por_ID(int id) {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        Clientes cliente = null;
        em.getTransaction().begin();
        try {
            cliente = em.find(Clientes.class, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return cliente;
    }    // </editor-fold> 

}

//    public static void main(String[] args) {
//        mantenimientoCliente m = new mantenimientoCliente();
//        Clientes cliente = new Clientes();
//        cliente.setIdCliente(5);
//        cliente.setNombreCliente("xxxxxx");
//        cliente.setApellidoCliente("xxxxxx");
//        cliente.setDireccionCliente("xxxxxx");
//        cliente.setMailCliente("xxxxxxxx");
//        cliente.setTelefonoCliente("xxxxx");
//        if (m.Actualizar_cliente(cliente) == 1) {
//            System.out.println("correcto");
//        }
//    }
//    public static void main(String[] args) {
//        mantenimientoCliente c = new mantenimientoCliente();
//        Clientes t = new Clientes();
//        t.setIdCliente(Integer.SIZE);
//        t.setNombreCliente("pureba");
//        t.setApellidoCliente("");
//        t.setDireccionCliente("pureba");
//        t.setTelefonoCliente("pureba");
//        t.setMailCliente("pureba");
//        if(c.Actualizar_cliente(t)!=0){
//             System.out.println("sirve");       
//        }else{
//            System.out.println("No sirve");
//        }
//        
//        if(c.Eliminar_Cliente(3)!=0){
//         System.out.println("sirve");       
//        }else{
//            System.out.println("No sirve");
//        }
//        
//    }

