/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboDepartament;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboDepartamentFacadeLocal {

    void create(DboDepartament dboDepartament);

    void edit(DboDepartament dboDepartament);

    void remove(DboDepartament dboDepartament);

    DboDepartament find(Object id);

    List<DboDepartament> findAll();

    List<DboDepartament> findRange(int[] range);

    int count();
    
}
