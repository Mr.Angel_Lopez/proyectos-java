/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboEmployeeJobLog;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboEmployeeJobLogFacadeLocal {

    void create(DboEmployeeJobLog dboEmployeeJobLog);

    void edit(DboEmployeeJobLog dboEmployeeJobLog);

    void remove(DboEmployeeJobLog dboEmployeeJobLog);

    DboEmployeeJobLog find(Object id);

    List<DboEmployeeJobLog> findAll();

    List<DboEmployeeJobLog> findRange(int[] range);

    int count();
    
}
