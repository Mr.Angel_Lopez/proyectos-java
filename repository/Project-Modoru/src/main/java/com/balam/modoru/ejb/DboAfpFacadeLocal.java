/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboAfp;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboAfpFacadeLocal {

    void create(DboAfp dboAfp);

    void edit(DboAfp dboAfp);

    void remove(DboAfp dboAfp);

    DboAfp find(Object id);

    List<DboAfp> findAll();

    List<DboAfp> findRange(int[] range);

    int count();
    
}
