/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_employee_branch_log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboEmployeeBranchLog.findAll", query = "SELECT d FROM DboEmployeeBranchLog d")
    , @NamedQuery(name = "DboEmployeeBranchLog.findByIdEbl", query = "SELECT d FROM DboEmployeeBranchLog d WHERE d.idEbl = :idEbl")
    , @NamedQuery(name = "DboEmployeeBranchLog.findByEblStartDate", query = "SELECT d FROM DboEmployeeBranchLog d WHERE d.eblStartDate = :eblStartDate")
    , @NamedQuery(name = "DboEmployeeBranchLog.findByEblEndDate", query = "SELECT d FROM DboEmployeeBranchLog d WHERE d.eblEndDate = :eblEndDate")
    , @NamedQuery(name = "DboEmployeeBranchLog.findByEblBranch", query = "SELECT d FROM DboEmployeeBranchLog d WHERE d.eblBranch = :eblBranch")})
public class DboEmployeeBranchLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_ebl")
    private String idEbl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ebl_start_date")
    @Temporal(TemporalType.DATE)
    private Date eblStartDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ebl_end_date")
    @Temporal(TemporalType.DATE)
    private Date eblEndDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ebl_branch")
    private String eblBranch;
    @JoinColumn(name = "ebl_id_profile", referencedColumnName = "id_profile")
    @ManyToOne(optional = false)
    private DboEmployeeProfile eblIdProfile;

    public DboEmployeeBranchLog() {
    }

    public DboEmployeeBranchLog(String idEbl) {
        this.idEbl = idEbl;
    }

    public DboEmployeeBranchLog(String idEbl, Date eblStartDate, Date eblEndDate, String eblBranch) {
        this.idEbl = idEbl;
        this.eblStartDate = eblStartDate;
        this.eblEndDate = eblEndDate;
        this.eblBranch = eblBranch;
    }

    public String getIdEbl() {
        return idEbl;
    }

    public void setIdEbl(String idEbl) {
        this.idEbl = idEbl;
    }

    public Date getEblStartDate() {
        return eblStartDate;
    }

    public void setEblStartDate(Date eblStartDate) {
        this.eblStartDate = eblStartDate;
    }

    public Date getEblEndDate() {
        return eblEndDate;
    }

    public void setEblEndDate(Date eblEndDate) {
        this.eblEndDate = eblEndDate;
    }

    public String getEblBranch() {
        return eblBranch;
    }

    public void setEblBranch(String eblBranch) {
        this.eblBranch = eblBranch;
    }

    public DboEmployeeProfile getEblIdProfile() {
        return eblIdProfile;
    }

    public void setEblIdProfile(DboEmployeeProfile eblIdProfile) {
        this.eblIdProfile = eblIdProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEbl != null ? idEbl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboEmployeeBranchLog)) {
            return false;
        }
        DboEmployeeBranchLog other = (DboEmployeeBranchLog) object;
        if ((this.idEbl == null && other.idEbl != null) || (this.idEbl != null && !this.idEbl.equals(other.idEbl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboEmployeeBranchLog[ idEbl=" + idEbl + " ]";
    }
    
}
