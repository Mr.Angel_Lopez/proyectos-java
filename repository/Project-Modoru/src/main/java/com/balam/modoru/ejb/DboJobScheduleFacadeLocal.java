/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboJobSchedule;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboJobScheduleFacadeLocal {

    void create(DboJobSchedule dboJobSchedule);

    void edit(DboJobSchedule dboJobSchedule);

    void remove(DboJobSchedule dboJobSchedule);

    DboJobSchedule find(Object id);

    List<DboJobSchedule> findAll();

    List<DboJobSchedule> findRange(int[] range);

    int count();
    
}
