/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboAddress;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboAddressFacadeLocal {

    void create(DboAddress dboAddress);

    void edit(DboAddress dboAddress);

    void remove(DboAddress dboAddress);

    DboAddress find(Object id);

    List<DboAddress> findAll();

    List<DboAddress> findRange(int[] range);

    int count();
    
}
