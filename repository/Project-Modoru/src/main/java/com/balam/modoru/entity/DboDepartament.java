/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_departament")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboDepartament.findAll", query = "SELECT d FROM DboDepartament d")
    , @NamedQuery(name = "DboDepartament.findByIdDepartament", query = "SELECT d FROM DboDepartament d WHERE d.idDepartament = :idDepartament")
    , @NamedQuery(name = "DboDepartament.findByDepartamentName", query = "SELECT d FROM DboDepartament d WHERE d.departamentName = :departamentName")})
public class DboDepartament implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_departament")
    private Integer idDepartament;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "departament_name")
    private String departamentName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "municipalityDepartament")
    private Collection<DboMunicipality> dboMunicipalityCollection;

    public DboDepartament() {
    }

    public DboDepartament(Integer idDepartament) {
        this.idDepartament = idDepartament;
    }

    public DboDepartament(Integer idDepartament, String departamentName) {
        this.idDepartament = idDepartament;
        this.departamentName = departamentName;
    }

    public Integer getIdDepartament() {
        return idDepartament;
    }

    public void setIdDepartament(Integer idDepartament) {
        this.idDepartament = idDepartament;
    }

    public String getDepartamentName() {
        return departamentName;
    }

    public void setDepartamentName(String departamentName) {
        this.departamentName = departamentName;
    }

    @XmlTransient
    public Collection<DboMunicipality> getDboMunicipalityCollection() {
        return dboMunicipalityCollection;
    }

    public void setDboMunicipalityCollection(Collection<DboMunicipality> dboMunicipalityCollection) {
        this.dboMunicipalityCollection = dboMunicipalityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDepartament != null ? idDepartament.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboDepartament)) {
            return false;
        }
        DboDepartament other = (DboDepartament) object;
        if ((this.idDepartament == null && other.idDepartament != null) || (this.idDepartament != null && !this.idDepartament.equals(other.idDepartament))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboDepartament[ idDepartament=" + idDepartament + " ]";
    }
    
}
