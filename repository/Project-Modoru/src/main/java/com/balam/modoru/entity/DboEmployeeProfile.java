/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_employee_profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboEmployeeProfile.findAll", query = "SELECT d FROM DboEmployeeProfile d")
    , @NamedQuery(name = "DboEmployeeProfile.findByIdProfile", query = "SELECT d FROM DboEmployeeProfile d WHERE d.idProfile = :idProfile")
    , @NamedQuery(name = "DboEmployeeProfile.findByProfileDate", query = "SELECT d FROM DboEmployeeProfile d WHERE d.profileDate = :profileDate")})
public class DboEmployeeProfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_profile")
    private String idProfile;
    @Column(name = "profile_date")
    @Temporal(TemporalType.DATE)
    private Date profileDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ejlEmployee")
    private Collection<DboEmployeeJobLog> dboEmployeeJobLogCollection;
    @JoinColumn(name = "profile_agreement", referencedColumnName = "id_agreement_employee")
    @ManyToOne(optional = false)
    private DboAgreementEmployee profileAgreement;
    @JoinColumn(name = "profile_branch_office", referencedColumnName = "id_branch_office")
    @ManyToOne(optional = false)
    private DboBranchOffice profileBranchOffice;
    @JoinColumn(name = "profile_employee", referencedColumnName = "id_employee")
    @ManyToOne(optional = false)
    private DboEmployee profileEmployee;
    @JoinColumn(name = "profile_job", referencedColumnName = "id_job")
    @ManyToOne(optional = false)
    private DboJob profileJob;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "dboEmployeeProfile")
    private DboStatusEmployee dboStatusEmployee;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eblIdProfile")
    private Collection<DboEmployeeBranchLog> dboEmployeeBranchLogCollection;

    public DboEmployeeProfile() {
    }

    public DboEmployeeProfile(String idProfile) {
        this.idProfile = idProfile;
    }

    public String getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(String idProfile) {
        this.idProfile = idProfile;
    }

    public Date getProfileDate() {
        return profileDate;
    }

    public void setProfileDate(Date profileDate) {
        this.profileDate = profileDate;
    }

    @XmlTransient
    public Collection<DboEmployeeJobLog> getDboEmployeeJobLogCollection() {
        return dboEmployeeJobLogCollection;
    }

    public void setDboEmployeeJobLogCollection(Collection<DboEmployeeJobLog> dboEmployeeJobLogCollection) {
        this.dboEmployeeJobLogCollection = dboEmployeeJobLogCollection;
    }

    public DboAgreementEmployee getProfileAgreement() {
        return profileAgreement;
    }

    public void setProfileAgreement(DboAgreementEmployee profileAgreement) {
        this.profileAgreement = profileAgreement;
    }

    public DboBranchOffice getProfileBranchOffice() {
        return profileBranchOffice;
    }

    public void setProfileBranchOffice(DboBranchOffice profileBranchOffice) {
        this.profileBranchOffice = profileBranchOffice;
    }

    public DboEmployee getProfileEmployee() {
        return profileEmployee;
    }

    public void setProfileEmployee(DboEmployee profileEmployee) {
        this.profileEmployee = profileEmployee;
    }

    public DboJob getProfileJob() {
        return profileJob;
    }

    public void setProfileJob(DboJob profileJob) {
        this.profileJob = profileJob;
    }

    public DboStatusEmployee getDboStatusEmployee() {
        return dboStatusEmployee;
    }

    public void setDboStatusEmployee(DboStatusEmployee dboStatusEmployee) {
        this.dboStatusEmployee = dboStatusEmployee;
    }

    @XmlTransient
    public Collection<DboEmployeeBranchLog> getDboEmployeeBranchLogCollection() {
        return dboEmployeeBranchLogCollection;
    }

    public void setDboEmployeeBranchLogCollection(Collection<DboEmployeeBranchLog> dboEmployeeBranchLogCollection) {
        this.dboEmployeeBranchLogCollection = dboEmployeeBranchLogCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfile != null ? idProfile.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboEmployeeProfile)) {
            return false;
        }
        DboEmployeeProfile other = (DboEmployeeProfile) object;
        if ((this.idProfile == null && other.idProfile != null) || (this.idProfile != null && !this.idProfile.equals(other.idProfile))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboEmployeeProfile[ idProfile=" + idProfile + " ]";
    }
    
}
