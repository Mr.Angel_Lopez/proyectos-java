/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboStatus.findAll", query = "SELECT d FROM DboStatus d")
    , @NamedQuery(name = "DboStatus.findByIdStatus", query = "SELECT d FROM DboStatus d WHERE d.idStatus = :idStatus")
    , @NamedQuery(name = "DboStatus.findByStatusName", query = "SELECT d FROM DboStatus d WHERE d.statusName = :statusName")})
public class DboStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_status")
    private Integer idStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "status_name")
    private String statusName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusEmployee")
    private Collection<DboStatusEmployee> dboStatusEmployeeCollection;

    public DboStatus() {
    }

    public DboStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public DboStatus(Integer idStatus, String statusName) {
        this.idStatus = idStatus;
        this.statusName = statusName;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @XmlTransient
    public Collection<DboStatusEmployee> getDboStatusEmployeeCollection() {
        return dboStatusEmployeeCollection;
    }

    public void setDboStatusEmployeeCollection(Collection<DboStatusEmployee> dboStatusEmployeeCollection) {
        this.dboStatusEmployeeCollection = dboStatusEmployeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatus != null ? idStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboStatus)) {
            return false;
        }
        DboStatus other = (DboStatus) object;
        if ((this.idStatus == null && other.idStatus != null) || (this.idStatus != null && !this.idStatus.equals(other.idStatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboStatus[ idStatus=" + idStatus + " ]";
    }
    
}
