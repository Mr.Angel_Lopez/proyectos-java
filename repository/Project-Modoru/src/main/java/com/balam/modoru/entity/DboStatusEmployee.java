/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_status_employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboStatusEmployee.findAll", query = "SELECT d FROM DboStatusEmployee d")
    , @NamedQuery(name = "DboStatusEmployee.findByIdStatusEmployee", query = "SELECT d FROM DboStatusEmployee d WHERE d.idStatusEmployee = :idStatusEmployee")
    , @NamedQuery(name = "DboStatusEmployee.findByStatusDate", query = "SELECT d FROM DboStatusEmployee d WHERE d.statusDate = :statusDate")})
public class DboStatusEmployee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_status_employee")
    private String idStatusEmployee;
    @Column(name = "status_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "selEmployee")
    private Collection<DboStatusEmployeeLog> dboStatusEmployeeLogCollection;
    @JoinColumn(name = "id_status_employee", referencedColumnName = "id_profile", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private DboEmployeeProfile dboEmployeeProfile;
    @JoinColumn(name = "status_employee", referencedColumnName = "id_status")
    @ManyToOne(optional = false)
    private DboStatus statusEmployee;

    public DboStatusEmployee() {
    }

    public DboStatusEmployee(String idStatusEmployee) {
        this.idStatusEmployee = idStatusEmployee;
    }

    public String getIdStatusEmployee() {
        return idStatusEmployee;
    }

    public void setIdStatusEmployee(String idStatusEmployee) {
        this.idStatusEmployee = idStatusEmployee;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    @XmlTransient
    public Collection<DboStatusEmployeeLog> getDboStatusEmployeeLogCollection() {
        return dboStatusEmployeeLogCollection;
    }

    public void setDboStatusEmployeeLogCollection(Collection<DboStatusEmployeeLog> dboStatusEmployeeLogCollection) {
        this.dboStatusEmployeeLogCollection = dboStatusEmployeeLogCollection;
    }

    public DboEmployeeProfile getDboEmployeeProfile() {
        return dboEmployeeProfile;
    }

    public void setDboEmployeeProfile(DboEmployeeProfile dboEmployeeProfile) {
        this.dboEmployeeProfile = dboEmployeeProfile;
    }

    public DboStatus getStatusEmployee() {
        return statusEmployee;
    }

    public void setStatusEmployee(DboStatus statusEmployee) {
        this.statusEmployee = statusEmployee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatusEmployee != null ? idStatusEmployee.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboStatusEmployee)) {
            return false;
        }
        DboStatusEmployee other = (DboStatusEmployee) object;
        if ((this.idStatusEmployee == null && other.idStatusEmployee != null) || (this.idStatusEmployee != null && !this.idStatusEmployee.equals(other.idStatusEmployee))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboStatusEmployee[ idStatusEmployee=" + idStatusEmployee + " ]";
    }
    
}
