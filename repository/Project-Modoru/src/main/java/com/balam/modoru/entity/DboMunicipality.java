/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_municipality")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboMunicipality.findAll", query = "SELECT d FROM DboMunicipality d")
    , @NamedQuery(name = "DboMunicipality.findByIdMunicipality", query = "SELECT d FROM DboMunicipality d WHERE d.idMunicipality = :idMunicipality")
    , @NamedQuery(name = "DboMunicipality.findByMunicipalityName", query = "SELECT d FROM DboMunicipality d WHERE d.municipalityName = :municipalityName")})
public class DboMunicipality implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_municipality")
    private Integer idMunicipality;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "municipality_name")
    private String municipalityName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "boMunicipality")
    private Collection<DboBranchOffice> dboBranchOfficeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeMunicipality")
    private Collection<DboAddress> dboAddressCollection;
    @JoinColumn(name = "municipality_departament", referencedColumnName = "id_departament")
    @ManyToOne(optional = false)
    private DboDepartament municipalityDepartament;

    public DboMunicipality() {
    }

    public DboMunicipality(Integer idMunicipality) {
        this.idMunicipality = idMunicipality;
    }

    public DboMunicipality(Integer idMunicipality, String municipalityName) {
        this.idMunicipality = idMunicipality;
        this.municipalityName = municipalityName;
    }

    public Integer getIdMunicipality() {
        return idMunicipality;
    }

    public void setIdMunicipality(Integer idMunicipality) {
        this.idMunicipality = idMunicipality;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public void setMunicipalityName(String municipalityName) {
        this.municipalityName = municipalityName;
    }

    @XmlTransient
    public Collection<DboBranchOffice> getDboBranchOfficeCollection() {
        return dboBranchOfficeCollection;
    }

    public void setDboBranchOfficeCollection(Collection<DboBranchOffice> dboBranchOfficeCollection) {
        this.dboBranchOfficeCollection = dboBranchOfficeCollection;
    }

    @XmlTransient
    public Collection<DboAddress> getDboAddressCollection() {
        return dboAddressCollection;
    }

    public void setDboAddressCollection(Collection<DboAddress> dboAddressCollection) {
        this.dboAddressCollection = dboAddressCollection;
    }

    public DboDepartament getMunicipalityDepartament() {
        return municipalityDepartament;
    }

    public void setMunicipalityDepartament(DboDepartament municipalityDepartament) {
        this.municipalityDepartament = municipalityDepartament;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMunicipality != null ? idMunicipality.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboMunicipality)) {
            return false;
        }
        DboMunicipality other = (DboMunicipality) object;
        if ((this.idMunicipality == null && other.idMunicipality != null) || (this.idMunicipality != null && !this.idMunicipality.equals(other.idMunicipality))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboMunicipality[ idMunicipality=" + idMunicipality + " ]";
    }
    
}
