/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_status_employee_log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboStatusEmployeeLog.findAll", query = "SELECT d FROM DboStatusEmployeeLog d")
    , @NamedQuery(name = "DboStatusEmployeeLog.findByIdStatusEmployeeLog", query = "SELECT d FROM DboStatusEmployeeLog d WHERE d.idStatusEmployeeLog = :idStatusEmployeeLog")
    , @NamedQuery(name = "DboStatusEmployeeLog.findBySelStatus", query = "SELECT d FROM DboStatusEmployeeLog d WHERE d.selStatus = :selStatus")
    , @NamedQuery(name = "DboStatusEmployeeLog.findBySelStartDate", query = "SELECT d FROM DboStatusEmployeeLog d WHERE d.selStartDate = :selStartDate")
    , @NamedQuery(name = "DboStatusEmployeeLog.findBySelEndDate", query = "SELECT d FROM DboStatusEmployeeLog d WHERE d.selEndDate = :selEndDate")})
public class DboStatusEmployeeLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_status_employee_log")
    private Integer idStatusEmployeeLog;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sel_status")
    private int selStatus;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sel_start_date")
    @Temporal(TemporalType.DATE)
    private Date selStartDate;
    @Column(name = "sel_end_date")
    @Temporal(TemporalType.DATE)
    private Date selEndDate;
    @JoinColumn(name = "sel_employee", referencedColumnName = "id_status_employee")
    @ManyToOne(optional = false)
    private DboStatusEmployee selEmployee;

    public DboStatusEmployeeLog() {
    }

    public DboStatusEmployeeLog(Integer idStatusEmployeeLog) {
        this.idStatusEmployeeLog = idStatusEmployeeLog;
    }

    public DboStatusEmployeeLog(Integer idStatusEmployeeLog, int selStatus, Date selStartDate) {
        this.idStatusEmployeeLog = idStatusEmployeeLog;
        this.selStatus = selStatus;
        this.selStartDate = selStartDate;
    }

    public Integer getIdStatusEmployeeLog() {
        return idStatusEmployeeLog;
    }

    public void setIdStatusEmployeeLog(Integer idStatusEmployeeLog) {
        this.idStatusEmployeeLog = idStatusEmployeeLog;
    }

    public int getSelStatus() {
        return selStatus;
    }

    public void setSelStatus(int selStatus) {
        this.selStatus = selStatus;
    }

    public Date getSelStartDate() {
        return selStartDate;
    }

    public void setSelStartDate(Date selStartDate) {
        this.selStartDate = selStartDate;
    }

    public Date getSelEndDate() {
        return selEndDate;
    }

    public void setSelEndDate(Date selEndDate) {
        this.selEndDate = selEndDate;
    }

    public DboStatusEmployee getSelEmployee() {
        return selEmployee;
    }

    public void setSelEmployee(DboStatusEmployee selEmployee) {
        this.selEmployee = selEmployee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatusEmployeeLog != null ? idStatusEmployeeLog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboStatusEmployeeLog)) {
            return false;
        }
        DboStatusEmployeeLog other = (DboStatusEmployeeLog) object;
        if ((this.idStatusEmployeeLog == null && other.idStatusEmployeeLog != null) || (this.idStatusEmployeeLog != null && !this.idStatusEmployeeLog.equals(other.idStatusEmployeeLog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboStatusEmployeeLog[ idStatusEmployeeLog=" + idStatusEmployeeLog + " ]";
    }
    
}
