/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_gender")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboGender.findAll", query = "SELECT d FROM DboGender d")
    , @NamedQuery(name = "DboGender.findByIdGender", query = "SELECT d FROM DboGender d WHERE d.idGender = :idGender")
    , @NamedQuery(name = "DboGender.findByGenderName", query = "SELECT d FROM DboGender d WHERE d.genderName = :genderName")})
public class DboGender implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_gender")
    private Integer idGender;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "gender_name")
    private String genderName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeGender")
    private Collection<DboEmployee> dboEmployeeCollection;

    public DboGender() {
    }

    public DboGender(Integer idGender) {
        this.idGender = idGender;
    }

    public DboGender(Integer idGender, String genderName) {
        this.idGender = idGender;
        this.genderName = genderName;
    }

    public Integer getIdGender() {
        return idGender;
    }

    public void setIdGender(Integer idGender) {
        this.idGender = idGender;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    @XmlTransient
    public Collection<DboEmployee> getDboEmployeeCollection() {
        return dboEmployeeCollection;
    }

    public void setDboEmployeeCollection(Collection<DboEmployee> dboEmployeeCollection) {
        this.dboEmployeeCollection = dboEmployeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGender != null ? idGender.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboGender)) {
            return false;
        }
        DboGender other = (DboGender) object;
        if ((this.idGender == null && other.idGender != null) || (this.idGender != null && !this.idGender.equals(other.idGender))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboGender[ idGender=" + idGender + " ]";
    }
    
}
