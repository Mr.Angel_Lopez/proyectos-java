/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_job_schedule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboJobSchedule.findAll", query = "SELECT d FROM DboJobSchedule d")
    , @NamedQuery(name = "DboJobSchedule.findByIdJobSchedule", query = "SELECT d FROM DboJobSchedule d WHERE d.idJobSchedule = :idJobSchedule")
    , @NamedQuery(name = "DboJobSchedule.findByJobSchedule", query = "SELECT d FROM DboJobSchedule d WHERE d.jobSchedule = :jobSchedule")})
public class DboJobSchedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_job_schedule")
    private String idJobSchedule;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "job_schedule")
    private String jobSchedule;
    @JoinColumn(name = "job_job", referencedColumnName = "id_job")
    @ManyToOne(optional = false)
    private DboJob jobJob;

    public DboJobSchedule() {
    }

    public DboJobSchedule(String idJobSchedule) {
        this.idJobSchedule = idJobSchedule;
    }

    public DboJobSchedule(String idJobSchedule, String jobSchedule) {
        this.idJobSchedule = idJobSchedule;
        this.jobSchedule = jobSchedule;
    }

    public String getIdJobSchedule() {
        return idJobSchedule;
    }

    public void setIdJobSchedule(String idJobSchedule) {
        this.idJobSchedule = idJobSchedule;
    }

    public String getJobSchedule() {
        return jobSchedule;
    }

    public void setJobSchedule(String jobSchedule) {
        this.jobSchedule = jobSchedule;
    }

    public DboJob getJobJob() {
        return jobJob;
    }

    public void setJobJob(DboJob jobJob) {
        this.jobJob = jobJob;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJobSchedule != null ? idJobSchedule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboJobSchedule)) {
            return false;
        }
        DboJobSchedule other = (DboJobSchedule) object;
        if ((this.idJobSchedule == null && other.idJobSchedule != null) || (this.idJobSchedule != null && !this.idJobSchedule.equals(other.idJobSchedule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboJobSchedule[ idJobSchedule=" + idJobSchedule + " ]";
    }
    
}
