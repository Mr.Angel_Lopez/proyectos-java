/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_schedule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboSchedule.findAll", query = "SELECT d FROM DboSchedule d")
    , @NamedQuery(name = "DboSchedule.findByIdSchedule", query = "SELECT d FROM DboSchedule d WHERE d.idSchedule = :idSchedule")
    , @NamedQuery(name = "DboSchedule.findByScheduleName", query = "SELECT d FROM DboSchedule d WHERE d.scheduleName = :scheduleName")
    , @NamedQuery(name = "DboSchedule.findByScheduleDay", query = "SELECT d FROM DboSchedule d WHERE d.scheduleDay = :scheduleDay")
    , @NamedQuery(name = "DboSchedule.findByScheduleStartTime", query = "SELECT d FROM DboSchedule d WHERE d.scheduleStartTime = :scheduleStartTime")
    , @NamedQuery(name = "DboSchedule.findByScheduleEndTime", query = "SELECT d FROM DboSchedule d WHERE d.scheduleEndTime = :scheduleEndTime")})
public class DboSchedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_schedule")
    private Integer idSchedule;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "schedule_name")
    private String scheduleName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "schedule_day")
    private String scheduleDay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "schedule_start_time")
    @Temporal(TemporalType.TIME)
    private Date scheduleStartTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "schedule_end_time")
    @Temporal(TemporalType.TIME)
    private Date scheduleEndTime;

    public DboSchedule() {
    }

    public DboSchedule(Integer idSchedule) {
        this.idSchedule = idSchedule;
    }

    public DboSchedule(Integer idSchedule, String scheduleName, String scheduleDay, Date scheduleStartTime, Date scheduleEndTime) {
        this.idSchedule = idSchedule;
        this.scheduleName = scheduleName;
        this.scheduleDay = scheduleDay;
        this.scheduleStartTime = scheduleStartTime;
        this.scheduleEndTime = scheduleEndTime;
    }

    public Integer getIdSchedule() {
        return idSchedule;
    }

    public void setIdSchedule(Integer idSchedule) {
        this.idSchedule = idSchedule;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getScheduleDay() {
        return scheduleDay;
    }

    public void setScheduleDay(String scheduleDay) {
        this.scheduleDay = scheduleDay;
    }

    public Date getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(Date scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public Date getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(Date scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSchedule != null ? idSchedule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboSchedule)) {
            return false;
        }
        DboSchedule other = (DboSchedule) object;
        if ((this.idSchedule == null && other.idSchedule != null) || (this.idSchedule != null && !this.idSchedule.equals(other.idSchedule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboSchedule[ idSchedule=" + idSchedule + " ]";
    }
    
}
