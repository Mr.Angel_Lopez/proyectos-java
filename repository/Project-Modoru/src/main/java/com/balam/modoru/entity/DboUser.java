/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboUser.findAll", query = "SELECT d FROM DboUser d")
    , @NamedQuery(name = "DboUser.findByIdUser", query = "SELECT d FROM DboUser d WHERE d.idUser = :idUser")
    , @NamedQuery(name = "DboUser.findByUserName", query = "SELECT d FROM DboUser d WHERE d.userName = :userName")
    , @NamedQuery(name = "DboUser.findByUserPass", query = "SELECT d FROM DboUser d WHERE d.userPass = :userPass")
    , @NamedQuery(name = "DboUser.findByUserEmail", query = "SELECT d FROM DboUser d WHERE d.userEmail = :userEmail")
    , @NamedQuery(name = "DboUser.findByUserStatus", query = "SELECT d FROM DboUser d WHERE d.userStatus = :userStatus")})
public class DboUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_user")
    private String idUser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_name")
    private String userName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "user_pass")
    private String userPass;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_email")
    private String userEmail;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "user_recovery")
    private String userRecovery;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_status")
    private int userStatus;
    @JoinColumn(name = "id_user", referencedColumnName = "id_employee", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private DboEmployee dboEmployee;
    @JoinColumn(name = "user_group", referencedColumnName = "id_group")
    @ManyToOne(optional = false)
    private DboGroup userGroup;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recoveryUser")
    private Collection<DboRecovery> dboRecoveryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activityUser")
    private Collection<DboActivityLog> dboActivityLogCollection;

    public DboUser() {
    }

    public DboUser(String idUser) {
        this.idUser = idUser;
    }

    public DboUser(String idUser, String userName, String userPass, String userEmail, String userRecovery, int userStatus) {
        this.idUser = idUser;
        this.userName = userName;
        this.userPass = userPass;
        this.userEmail = userEmail;
        this.userRecovery = userRecovery;
        this.userStatus = userStatus;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserRecovery() {
        return userRecovery;
    }

    public void setUserRecovery(String userRecovery) {
        this.userRecovery = userRecovery;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    public DboEmployee getDboEmployee() {
        return dboEmployee;
    }

    public void setDboEmployee(DboEmployee dboEmployee) {
        this.dboEmployee = dboEmployee;
    }

    public DboGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(DboGroup userGroup) {
        this.userGroup = userGroup;
    }

    @XmlTransient
    public Collection<DboRecovery> getDboRecoveryCollection() {
        return dboRecoveryCollection;
    }

    public void setDboRecoveryCollection(Collection<DboRecovery> dboRecoveryCollection) {
        this.dboRecoveryCollection = dboRecoveryCollection;
    }

    @XmlTransient
    public Collection<DboActivityLog> getDboActivityLogCollection() {
        return dboActivityLogCollection;
    }

    public void setDboActivityLogCollection(Collection<DboActivityLog> dboActivityLogCollection) {
        this.dboActivityLogCollection = dboActivityLogCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUser != null ? idUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboUser)) {
            return false;
        }
        DboUser other = (DboUser) object;
        if ((this.idUser == null && other.idUser != null) || (this.idUser != null && !this.idUser.equals(other.idUser))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboUser[ idUser=" + idUser + " ]";
    }
    
}
