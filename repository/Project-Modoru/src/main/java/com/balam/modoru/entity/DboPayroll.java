/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_payroll")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboPayroll.findAll", query = "SELECT d FROM DboPayroll d")
    , @NamedQuery(name = "DboPayroll.findByIddboPayroll", query = "SELECT d FROM DboPayroll d WHERE d.iddboPayroll = :iddboPayroll")})
public class DboPayroll implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "iddbo_payroll")
    private Integer iddboPayroll;

    public DboPayroll() {
    }

    public DboPayroll(Integer iddboPayroll) {
        this.iddboPayroll = iddboPayroll;
    }

    public Integer getIddboPayroll() {
        return iddboPayroll;
    }

    public void setIddboPayroll(Integer iddboPayroll) {
        this.iddboPayroll = iddboPayroll;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddboPayroll != null ? iddboPayroll.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboPayroll)) {
            return false;
        }
        DboPayroll other = (DboPayroll) object;
        if ((this.iddboPayroll == null && other.iddboPayroll != null) || (this.iddboPayroll != null && !this.iddboPayroll.equals(other.iddboPayroll))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboPayroll[ iddboPayroll=" + iddboPayroll + " ]";
    }
    
}
