package com.balam.modoru.controller;

import com.balam.modoru.ejb.DboAddressFacadeLocal;
import com.balam.modoru.ejb.DboAfpFacadeLocal;
import com.balam.modoru.ejb.DboBranchOfficeStatusFacadeLocal;
import com.balam.modoru.ejb.DboCivilStatusFacadeLocal;
import com.balam.modoru.ejb.DboCountryFacadeLocal;
import com.balam.modoru.ejb.DboGenderFacadeLocal;
import com.balam.modoru.ejb.DboMunicipalityFacadeLocal;
import com.balam.modoru.entity.DboAddress;
import com.balam.modoru.entity.DboAfp;
import com.balam.modoru.entity.DboBranchOfficeStatus;
import com.balam.modoru.entity.DboCivilStatus;
import com.balam.modoru.entity.DboCountry;
import com.balam.modoru.entity.DboGender;
import com.balam.modoru.entity.DboMunicipality;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "select")
@ViewScoped
public class SelecOneMenu implements Serializable{

    
    @EJB
    private DboCountryFacadeLocal countryEJB;
    @EJB
    private DboAfpFacadeLocal afpEJB;
    @EJB
    private DboGenderFacadeLocal genderEJB;
    @EJB
    private DboCivilStatusFacadeLocal civilStatusEJB;
    @EJB
    private DboAddressFacadeLocal addressEJB;
    @EJB
    private DboMunicipalityFacadeLocal municipalityEJB;
    @EJB
    private DboBranchOfficeStatusFacadeLocal bosEJB;
    
    
    /* SELECT COUNTRY */
    public List<DboCountry> getListCountry(){
     return countryEJB.findAll();
    }
    
    /* SELECT AFP */
    public List<DboAfp> getListAFP(){
        return afpEJB.findAll();
    }
    
    /* SELECT GENDER */
    public List<DboGender> getListGender(){
        return genderEJB.findAll();
    }
    
    /* SELECT CIVIL STATUS */
    public List<DboCivilStatus> getListCivilStatus(){
        return civilStatusEJB.findAll();
    }
    
    /* SELECT ADDRESS */
    public List<DboAddress> getListAddress(){
        return addressEJB.findAll();
    }
    
    /*SELECT MUNICIPALITY*/
    public List<DboMunicipality> getListMunicipality(){
    return municipalityEJB.findAll();
    }
    
    /*SELECT BRANCH OFFICE STATUS*/
    public List<DboBranchOfficeStatus> getListbos(){
    return bosEJB.findAll();
    }

}
