package com.balam.modoru.controller;

import com.balam.modoru.ejb.DboBranchOfficeFacadeLocal;
import com.balam.modoru.ejb.DboMunicipalityFacadeLocal;
import com.balam.modoru.entity.DboBranchOffice;
import com.balam.modoru.entity.DboBranchOfficeStatus;
import com.balam.modoru.ejb.DboBranchOfficeStatusFacadeLocal;
import com.balam.modoru.entity.DboMunicipality;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "branchOfficeController")
@ViewScoped
public class branchOfficeController implements Serializable{

    @EJB
    private DboBranchOfficeFacadeLocal branchEJB;
    @EJB
    private DboMunicipalityFacadeLocal municipalityEJB;
    @EJB
    private DboBranchOfficeStatusFacadeLocal statusEJB;
    
    private DboBranchOffice branch;
    private DboMunicipality municipality;
    private DboBranchOfficeStatus status;

    public DboBranchOfficeStatus getStatus() {
        return status;
    }

    public void setStatus(DboBranchOfficeStatus status) {
        this.status = status;
    }
    
    public DboBranchOffice getBranch() {
        return branch;
    }

    public void setBranch(DboBranchOffice branch) {
        this.branch = branch;
    }
    
    private int municipalityid;
    private int statusid;

    public int getStatusid() {
        return statusid;
    }

    public void setStatusid(int statusid) {
        this.statusid = statusid;
    }

    public int getMunicipalityid() {
        return municipalityid;
    }

    public void setMunicipalityid(int municipalityid) {
        this.municipalityid = municipalityid;
    }
    
    @PostConstruct
    public void init(){
    branch = new DboBranchOffice();
    }
    
    /*FacesContext for all the massage we need to show*/
    FacesContext context = FacesContext.getCurrentInstance();
    
    /*here starts thes crud´s functions*/
    
    /*FUNCTION FOR CREATING A NEW BRACH*/
    public int createBranch(){
    int flag = 0;
        try {
            String nombre = branch.getBoName().replace(" ", "").toUpperCase();
            branch.setIdBranchOffice("BO-"+municipalityid+"-"+nombre);
            branch.setBoMunicipality(municipalityEJB.find(municipalityid));
            branch.setBoStatus(statusEJB.find(statusid));
            branchEJB.create(branch);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Exito","Guardado de branch exitoso"));
        } catch (Exception e) {
            e.printStackTrace();
          context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error","Error en guardado de branch"));
        }
    return flag;
    }
    
    /*FUNCTION FOR EDITING A BRANCH*/
    public int editBranch(){
    int flag = 0;
        try {
            branch.setBoMunicipality(municipalityEJB.find(municipalityid));
            branch.setBoStatus(statusEJB.find(statusid));
            branchEJB.edit(branch);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Exito","Actualizado de branch exitoso"));
        } catch (Exception e) {
            e.printStackTrace();
          context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error","Error en Actualizado de branch"));
        }
    return flag;
    }
    
    /*FUNCTION FOR DELETING A BRANCH*/
    public int revomeBranch(String id){
    int flag = 0;
        try {
            branch = branchEJB.find(id);
            branchEJB.remove(branch);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Exito","Eliminación de branch exitoso"));
        } catch (Exception e) {
            e.printStackTrace();
          context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error","Error en Eliminación de branch"));
        }
    return flag;
    }
    
    /*FUNCTION FOR FINDING A SPECIFIC BRANCH*/
    public void findBranch(String id){
            branch = branchEJB.find(id);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Exito","Branch encontrado exitosamente"));
    }
    
    /*FUNCTION FOR FINDING ALL BRANCHES*/
    public List<DboBranchOffice> findAllBranch() {
    return branchEJB.findAll();
        }
    
    /*FUNCTION FOR CHANGING THE STATUS OF THE */
    
    
    
}
