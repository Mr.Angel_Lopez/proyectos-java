/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboStatusEmployee;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author angel.lopezusam
 */
@Stateless
public class DboStatusEmployeeFacade extends AbstractFacade<DboStatusEmployee> implements DboStatusEmployeeFacadeLocal {

    @PersistenceContext(unitName = "modoruPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DboStatusEmployeeFacade() {
        super(DboStatusEmployee.class);
    }
    
}
