/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_employee_contact")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboEmployeeContact.findAll", query = "SELECT d FROM DboEmployeeContact d")
    , @NamedQuery(name = "DboEmployeeContact.findByIdEmployeeContact", query = "SELECT d FROM DboEmployeeContact d WHERE d.idEmployeeContact = :idEmployeeContact")})
public class DboEmployeeContact implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_employee_contact")
    private Integer idEmployeeContact;
    @JoinColumn(name = "ec_contact", referencedColumnName = "id_contact")
    @ManyToOne(optional = false)
    private DboContact ecContact;
    @JoinColumn(name = "ec_employee", referencedColumnName = "id_employee")
    @ManyToOne(optional = false)
    private DboEmployee ecEmployee;

    public DboEmployeeContact() {
    }

    public DboEmployeeContact(Integer idEmployeeContact) {
        this.idEmployeeContact = idEmployeeContact;
    }

    public Integer getIdEmployeeContact() {
        return idEmployeeContact;
    }

    public void setIdEmployeeContact(Integer idEmployeeContact) {
        this.idEmployeeContact = idEmployeeContact;
    }

    public DboContact getEcContact() {
        return ecContact;
    }

    public void setEcContact(DboContact ecContact) {
        this.ecContact = ecContact;
    }

    public DboEmployee getEcEmployee() {
        return ecEmployee;
    }

    public void setEcEmployee(DboEmployee ecEmployee) {
        this.ecEmployee = ecEmployee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmployeeContact != null ? idEmployeeContact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboEmployeeContact)) {
            return false;
        }
        DboEmployeeContact other = (DboEmployeeContact) object;
        if ((this.idEmployeeContact == null && other.idEmployeeContact != null) || (this.idEmployeeContact != null && !this.idEmployeeContact.equals(other.idEmployeeContact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboEmployeeContact[ idEmployeeContact=" + idEmployeeContact + " ]";
    }
    
}
