/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboJob;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboJobFacadeLocal {

    void create(DboJob dboJob);

    void edit(DboJob dboJob);

    void remove(DboJob dboJob);

    DboJob find(Object id);

    List<DboJob> findAll();

    List<DboJob> findRange(int[] range);

    int count();
    
}
