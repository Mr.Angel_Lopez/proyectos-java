/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_employee_job_log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboEmployeeJobLog.findAll", query = "SELECT d FROM DboEmployeeJobLog d")
    , @NamedQuery(name = "DboEmployeeJobLog.findByIdEjl", query = "SELECT d FROM DboEmployeeJobLog d WHERE d.idEjl = :idEjl")
    , @NamedQuery(name = "DboEmployeeJobLog.findByEjlEmployeeJob", query = "SELECT d FROM DboEmployeeJobLog d WHERE d.ejlEmployeeJob = :ejlEmployeeJob")
    , @NamedQuery(name = "DboEmployeeJobLog.findByEjlStartJob", query = "SELECT d FROM DboEmployeeJobLog d WHERE d.ejlStartJob = :ejlStartJob")
    , @NamedQuery(name = "DboEmployeeJobLog.findByEjlEndJob", query = "SELECT d FROM DboEmployeeJobLog d WHERE d.ejlEndJob = :ejlEndJob")})
public class DboEmployeeJobLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_ejl")
    private String idEjl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ejl_employee_job")
    private String ejlEmployeeJob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ejl_start_job")
    @Temporal(TemporalType.DATE)
    private Date ejlStartJob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ejl_end_job")
    @Temporal(TemporalType.DATE)
    private Date ejlEndJob;
    @JoinColumn(name = "ejl_employee", referencedColumnName = "profile_employee")
    @ManyToOne(optional = false)
    private DboEmployeeProfile ejlEmployee;

    public DboEmployeeJobLog() {
    }

    public DboEmployeeJobLog(String idEjl) {
        this.idEjl = idEjl;
    }

    public DboEmployeeJobLog(String idEjl, String ejlEmployeeJob, Date ejlStartJob, Date ejlEndJob) {
        this.idEjl = idEjl;
        this.ejlEmployeeJob = ejlEmployeeJob;
        this.ejlStartJob = ejlStartJob;
        this.ejlEndJob = ejlEndJob;
    }

    public String getIdEjl() {
        return idEjl;
    }

    public void setIdEjl(String idEjl) {
        this.idEjl = idEjl;
    }

    public String getEjlEmployeeJob() {
        return ejlEmployeeJob;
    }

    public void setEjlEmployeeJob(String ejlEmployeeJob) {
        this.ejlEmployeeJob = ejlEmployeeJob;
    }

    public Date getEjlStartJob() {
        return ejlStartJob;
    }

    public void setEjlStartJob(Date ejlStartJob) {
        this.ejlStartJob = ejlStartJob;
    }

    public Date getEjlEndJob() {
        return ejlEndJob;
    }

    public void setEjlEndJob(Date ejlEndJob) {
        this.ejlEndJob = ejlEndJob;
    }

    public DboEmployeeProfile getEjlEmployee() {
        return ejlEmployee;
    }

    public void setEjlEmployee(DboEmployeeProfile ejlEmployee) {
        this.ejlEmployee = ejlEmployee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEjl != null ? idEjl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboEmployeeJobLog)) {
            return false;
        }
        DboEmployeeJobLog other = (DboEmployeeJobLog) object;
        if ((this.idEjl == null && other.idEjl != null) || (this.idEjl != null && !this.idEjl.equals(other.idEjl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboEmployeeJobLog[ idEjl=" + idEjl + " ]";
    }
    
}
