/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_recovery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboRecovery.findAll", query = "SELECT d FROM DboRecovery d")
    , @NamedQuery(name = "DboRecovery.findByIdRecovery", query = "SELECT d FROM DboRecovery d WHERE d.idRecovery = :idRecovery")})
public class DboRecovery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_recovery")
    private String idRecovery;
    @Lob
    @Size(max = 65535)
    @Column(name = "recovery_anwser")
    private String recoveryAnwser;
    @JoinColumn(name = "recovery_question", referencedColumnName = "id_question")
    @ManyToOne
    private DboQuestion recoveryQuestion;
    @JoinColumn(name = "recovery_user", referencedColumnName = "id_user")
    @ManyToOne(optional = false)
    private DboUser recoveryUser;

    public DboRecovery() {
    }

    public DboRecovery(String idRecovery) {
        this.idRecovery = idRecovery;
    }

    public String getIdRecovery() {
        return idRecovery;
    }

    public void setIdRecovery(String idRecovery) {
        this.idRecovery = idRecovery;
    }

    public String getRecoveryAnwser() {
        return recoveryAnwser;
    }

    public void setRecoveryAnwser(String recoveryAnwser) {
        this.recoveryAnwser = recoveryAnwser;
    }

    public DboQuestion getRecoveryQuestion() {
        return recoveryQuestion;
    }

    public void setRecoveryQuestion(DboQuestion recoveryQuestion) {
        this.recoveryQuestion = recoveryQuestion;
    }

    public DboUser getRecoveryUser() {
        return recoveryUser;
    }

    public void setRecoveryUser(DboUser recoveryUser) {
        this.recoveryUser = recoveryUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecovery != null ? idRecovery.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboRecovery)) {
            return false;
        }
        DboRecovery other = (DboRecovery) object;
        if ((this.idRecovery == null && other.idRecovery != null) || (this.idRecovery != null && !this.idRecovery.equals(other.idRecovery))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboRecovery[ idRecovery=" + idRecovery + " ]";
    }
    
}
