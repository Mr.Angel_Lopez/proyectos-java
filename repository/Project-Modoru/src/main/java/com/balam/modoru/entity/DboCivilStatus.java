/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_civil_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboCivilStatus.findAll", query = "SELECT d FROM DboCivilStatus d")
    , @NamedQuery(name = "DboCivilStatus.findByIdCivilStatus", query = "SELECT d FROM DboCivilStatus d WHERE d.idCivilStatus = :idCivilStatus")
    , @NamedQuery(name = "DboCivilStatus.findByCivilStatusName", query = "SELECT d FROM DboCivilStatus d WHERE d.civilStatusName = :civilStatusName")})
public class DboCivilStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_civil_status")
    private Integer idCivilStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "civil_status_name")
    private String civilStatusName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeCivilStatus")
    private Collection<DboEmployee> dboEmployeeCollection;

    public DboCivilStatus() {
    }

    public DboCivilStatus(Integer idCivilStatus) {
        this.idCivilStatus = idCivilStatus;
    }

    public DboCivilStatus(Integer idCivilStatus, String civilStatusName) {
        this.idCivilStatus = idCivilStatus;
        this.civilStatusName = civilStatusName;
    }

    public Integer getIdCivilStatus() {
        return idCivilStatus;
    }

    public void setIdCivilStatus(Integer idCivilStatus) {
        this.idCivilStatus = idCivilStatus;
    }

    public String getCivilStatusName() {
        return civilStatusName;
    }

    public void setCivilStatusName(String civilStatusName) {
        this.civilStatusName = civilStatusName;
    }

    @XmlTransient
    public Collection<DboEmployee> getDboEmployeeCollection() {
        return dboEmployeeCollection;
    }

    public void setDboEmployeeCollection(Collection<DboEmployee> dboEmployeeCollection) {
        this.dboEmployeeCollection = dboEmployeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCivilStatus != null ? idCivilStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboCivilStatus)) {
            return false;
        }
        DboCivilStatus other = (DboCivilStatus) object;
        if ((this.idCivilStatus == null && other.idCivilStatus != null) || (this.idCivilStatus != null && !this.idCivilStatus.equals(other.idCivilStatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboCivilStatus[ idCivilStatus=" + idCivilStatus + " ]";
    }
    
}
