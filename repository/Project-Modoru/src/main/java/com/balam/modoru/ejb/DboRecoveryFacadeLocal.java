/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboRecovery;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboRecoveryFacadeLocal {

    void create(DboRecovery dboRecovery);

    void edit(DboRecovery dboRecovery);

    void remove(DboRecovery dboRecovery);

    DboRecovery find(Object id);

    List<DboRecovery> findAll();

    List<DboRecovery> findRange(int[] range);

    int count();
    
}
