/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_division")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboDivision.findAll", query = "SELECT d FROM DboDivision d")
    , @NamedQuery(name = "DboDivision.findByIdDivision", query = "SELECT d FROM DboDivision d WHERE d.idDivision = :idDivision")
    , @NamedQuery(name = "DboDivision.findByDivisonName", query = "SELECT d FROM DboDivision d WHERE d.divisonName = :divisonName")})
public class DboDivision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_division")
    private Integer idDivision;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "divison_name")
    private String divisonName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jobDivision")
    private Collection<DboJob> dboJobCollection;

    public DboDivision() {
    }

    public DboDivision(Integer idDivision) {
        this.idDivision = idDivision;
    }

    public DboDivision(Integer idDivision, String divisonName) {
        this.idDivision = idDivision;
        this.divisonName = divisonName;
    }

    public Integer getIdDivision() {
        return idDivision;
    }

    public void setIdDivision(Integer idDivision) {
        this.idDivision = idDivision;
    }

    public String getDivisonName() {
        return divisonName;
    }

    public void setDivisonName(String divisonName) {
        this.divisonName = divisonName;
    }

    @XmlTransient
    public Collection<DboJob> getDboJobCollection() {
        return dboJobCollection;
    }

    public void setDboJobCollection(Collection<DboJob> dboJobCollection) {
        this.dboJobCollection = dboJobCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDivision != null ? idDivision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboDivision)) {
            return false;
        }
        DboDivision other = (DboDivision) object;
        if ((this.idDivision == null && other.idDivision != null) || (this.idDivision != null && !this.idDivision.equals(other.idDivision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboDivision[ idDivision=" + idDivision + " ]";
    }
    
}
