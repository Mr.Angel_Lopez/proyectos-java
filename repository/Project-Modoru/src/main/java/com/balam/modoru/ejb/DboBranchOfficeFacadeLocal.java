/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboBranchOffice;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboBranchOfficeFacadeLocal {

    void create(DboBranchOffice dboBranchOffice);

    void edit(DboBranchOffice dboBranchOffice);

    void remove(DboBranchOffice dboBranchOffice);

    DboBranchOffice find(Object id);

    List<DboBranchOffice> findAll();

    List<DboBranchOffice> findRange(int[] range);

    int count();
    
}
