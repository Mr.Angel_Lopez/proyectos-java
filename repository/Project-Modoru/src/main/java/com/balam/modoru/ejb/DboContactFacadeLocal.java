/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboContact;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboContactFacadeLocal {

    void create(DboContact dboContact);

    void edit(DboContact dboContact);

    void remove(DboContact dboContact);

    DboContact find(Object id);

    List<DboContact> findAll();

    List<DboContact> findRange(int[] range);

    int count();
    
}
