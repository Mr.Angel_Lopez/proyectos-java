/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboQuestion;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboQuestionFacadeLocal {

    void create(DboQuestion dboQuestion);

    void edit(DboQuestion dboQuestion);

    void remove(DboQuestion dboQuestion);

    DboQuestion find(Object id);

    List<DboQuestion> findAll();

    List<DboQuestion> findRange(int[] range);

    int count();
    
}
