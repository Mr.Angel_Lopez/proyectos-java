/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboDivision;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboDivisionFacadeLocal {

    void create(DboDivision dboDivision);

    void edit(DboDivision dboDivision);

    void remove(DboDivision dboDivision);

    DboDivision find(Object id);

    List<DboDivision> findAll();

    List<DboDivision> findRange(int[] range);

    int count();
    
}
