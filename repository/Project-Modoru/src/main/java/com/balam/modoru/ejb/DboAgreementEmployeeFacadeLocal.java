/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboAgreementEmployee;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboAgreementEmployeeFacadeLocal {

    void create(DboAgreementEmployee dboAgreementEmployee);

    void edit(DboAgreementEmployee dboAgreementEmployee);

    void remove(DboAgreementEmployee dboAgreementEmployee);

    DboAgreementEmployee find(Object id);

    List<DboAgreementEmployee> findAll();

    List<DboAgreementEmployee> findRange(int[] range);

    int count();
    
}
