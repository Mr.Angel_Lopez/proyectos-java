package com.balam.modoru.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.balam.modoru.ejb.ViewUserEmployeeFacadeLocal;
import com.balam.modoru.entity.ViewUserEmployee;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author angel.lopezusam
 */
@Named(value = "employee")
@ViewScoped
public class employeeController implements Serializable {

    @EJB
    private ViewUserEmployeeFacadeLocal employeeEJB;

    private ViewUserEmployee employee;

    public ViewUserEmployee getEmployee() {
        return employee;
    }

    public void setEmployee(ViewUserEmployee employee) {
        this.employee = employee;
    }

    @PostConstruct
    public void init() {
        employee = new ViewUserEmployee();
    }

//    public void createTabla() {
//        FacesContext context = FacesContext.getCurrentInstance();
//        try {
//            employeeEJB.create(employee);
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "guardado"));
//        } catch (Exception e) {
//            e.printStackTrace();
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Guardado"));
//        }
//}
    
    private List<ViewUserEmployee> findAllTabla;
    
    public List<ViewUserEmployee> getFindAllTabla() {
        return employeeEJB.findAll();
    }

}
