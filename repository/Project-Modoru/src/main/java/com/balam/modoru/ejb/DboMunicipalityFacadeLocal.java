/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboMunicipality;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboMunicipalityFacadeLocal {

    void create(DboMunicipality dboMunicipality);

    void edit(DboMunicipality dboMunicipality);

    void remove(DboMunicipality dboMunicipality);

    DboMunicipality find(Object id);

    List<DboMunicipality> findAll();

    List<DboMunicipality> findRange(int[] range);

    int count();
    
}
