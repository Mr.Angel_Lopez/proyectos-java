/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboEmployeeContact;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboEmployeeContactFacadeLocal {

    void create(DboEmployeeContact dboEmployeeContact);

    void edit(DboEmployeeContact dboEmployeeContact);

    void remove(DboEmployeeContact dboEmployeeContact);

    DboEmployeeContact find(Object id);

    List<DboEmployeeContact> findAll();

    List<DboEmployeeContact> findRange(int[] range);

    int count();
    
}
