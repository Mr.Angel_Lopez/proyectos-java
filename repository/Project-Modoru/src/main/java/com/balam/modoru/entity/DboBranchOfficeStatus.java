/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_branch_office_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboBranchOfficeStatus.findAll", query = "SELECT d FROM DboBranchOfficeStatus d")
    , @NamedQuery(name = "DboBranchOfficeStatus.findByIdBranchOfficeStatus", query = "SELECT d FROM DboBranchOfficeStatus d WHERE d.idBranchOfficeStatus = :idBranchOfficeStatus")
    , @NamedQuery(name = "DboBranchOfficeStatus.findByBosName", query = "SELECT d FROM DboBranchOfficeStatus d WHERE d.bosName = :bosName")})
public class DboBranchOfficeStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_branch_office_status")
    private Integer idBranchOfficeStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "bos_name")
    private String bosName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "boStatus")
    private Collection<DboBranchOffice> dboBranchOfficeCollection;

    public DboBranchOfficeStatus() {
    }

    public DboBranchOfficeStatus(Integer idBranchOfficeStatus) {
        this.idBranchOfficeStatus = idBranchOfficeStatus;
    }

    public DboBranchOfficeStatus(Integer idBranchOfficeStatus, String bosName) {
        this.idBranchOfficeStatus = idBranchOfficeStatus;
        this.bosName = bosName;
    }

    public Integer getIdBranchOfficeStatus() {
        return idBranchOfficeStatus;
    }

    public void setIdBranchOfficeStatus(Integer idBranchOfficeStatus) {
        this.idBranchOfficeStatus = idBranchOfficeStatus;
    }

    public String getBosName() {
        return bosName;
    }

    public void setBosName(String bosName) {
        this.bosName = bosName;
    }

    @XmlTransient
    public Collection<DboBranchOffice> getDboBranchOfficeCollection() {
        return dboBranchOfficeCollection;
    }

    public void setDboBranchOfficeCollection(Collection<DboBranchOffice> dboBranchOfficeCollection) {
        this.dboBranchOfficeCollection = dboBranchOfficeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBranchOfficeStatus != null ? idBranchOfficeStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboBranchOfficeStatus)) {
            return false;
        }
        DboBranchOfficeStatus other = (DboBranchOfficeStatus) object;
        if ((this.idBranchOfficeStatus == null && other.idBranchOfficeStatus != null) || (this.idBranchOfficeStatus != null && !this.idBranchOfficeStatus.equals(other.idBranchOfficeStatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboBranchOfficeStatus[ idBranchOfficeStatus=" + idBranchOfficeStatus + " ]";
    }
    
}
