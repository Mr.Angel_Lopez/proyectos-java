/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_job")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboJob.findAll", query = "SELECT d FROM DboJob d")
    , @NamedQuery(name = "DboJob.findByIdJob", query = "SELECT d FROM DboJob d WHERE d.idJob = :idJob")
    , @NamedQuery(name = "DboJob.findByJobName", query = "SELECT d FROM DboJob d WHERE d.jobName = :jobName")
    , @NamedQuery(name = "DboJob.findByJobSalary", query = "SELECT d FROM DboJob d WHERE d.jobSalary = :jobSalary")
    , @NamedQuery(name = "DboJob.findByJobLocal", query = "SELECT d FROM DboJob d WHERE d.jobLocal = :jobLocal")})
public class DboJob implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_job")
    private String idJob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "job_name")
    private String jobName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "job_salary")
    private BigDecimal jobSalary;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "job_local")
    private String jobLocal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jobJob")
    private Collection<DboJobSchedule> dboJobScheduleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profileJob")
    private Collection<DboEmployeeProfile> dboEmployeeProfileCollection;
    @JoinColumn(name = "job_division", referencedColumnName = "id_division")
    @ManyToOne(optional = false)
    private DboDivision jobDivision;

    public DboJob() {
    }

    public DboJob(String idJob) {
        this.idJob = idJob;
    }

    public DboJob(String idJob, String jobName, BigDecimal jobSalary, String jobLocal) {
        this.idJob = idJob;
        this.jobName = jobName;
        this.jobSalary = jobSalary;
        this.jobLocal = jobLocal;
    }

    public String getIdJob() {
        return idJob;
    }

    public void setIdJob(String idJob) {
        this.idJob = idJob;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public BigDecimal getJobSalary() {
        return jobSalary;
    }

    public void setJobSalary(BigDecimal jobSalary) {
        this.jobSalary = jobSalary;
    }

    public String getJobLocal() {
        return jobLocal;
    }

    public void setJobLocal(String jobLocal) {
        this.jobLocal = jobLocal;
    }

    @XmlTransient
    public Collection<DboJobSchedule> getDboJobScheduleCollection() {
        return dboJobScheduleCollection;
    }

    public void setDboJobScheduleCollection(Collection<DboJobSchedule> dboJobScheduleCollection) {
        this.dboJobScheduleCollection = dboJobScheduleCollection;
    }

    @XmlTransient
    public Collection<DboEmployeeProfile> getDboEmployeeProfileCollection() {
        return dboEmployeeProfileCollection;
    }

    public void setDboEmployeeProfileCollection(Collection<DboEmployeeProfile> dboEmployeeProfileCollection) {
        this.dboEmployeeProfileCollection = dboEmployeeProfileCollection;
    }

    public DboDivision getJobDivision() {
        return jobDivision;
    }

    public void setJobDivision(DboDivision jobDivision) {
        this.jobDivision = jobDivision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJob != null ? idJob.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboJob)) {
            return false;
        }
        DboJob other = (DboJob) object;
        if ((this.idJob == null && other.idJob != null) || (this.idJob != null && !this.idJob.equals(other.idJob))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboJob[ idJob=" + idJob + " ]";
    }
    
}
