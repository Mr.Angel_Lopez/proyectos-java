/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboBranchOfficeStatus;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboBranchOfficeStatusFacadeLocal {

    void create(DboBranchOfficeStatus dboBranchOfficeStatus);

    void edit(DboBranchOfficeStatus dboBranchOfficeStatus);

    void remove(DboBranchOfficeStatus dboBranchOfficeStatus);

    DboBranchOfficeStatus find(Object id);

    List<DboBranchOfficeStatus> findAll();

    List<DboBranchOfficeStatus> findRange(int[] range);

    int count();
    
}
