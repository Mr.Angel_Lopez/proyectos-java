/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboEmployee;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboEmployeeFacadeLocal {

    void create(DboEmployee dboEmployee);

    void edit(DboEmployee dboEmployee);

    void remove(DboEmployee dboEmployee);

    DboEmployee find(Object id);

    List<DboEmployee> findAll();

    List<DboEmployee> findRange(int[] range);

    int count();
    
}
