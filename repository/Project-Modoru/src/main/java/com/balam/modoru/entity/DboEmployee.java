/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboEmployee.findAll", query = "SELECT d FROM DboEmployee d")
    , @NamedQuery(name = "DboEmployee.findByIdEmployee", query = "SELECT d FROM DboEmployee d WHERE d.idEmployee = :idEmployee")
    , @NamedQuery(name = "DboEmployee.findByEmployeeName", query = "SELECT d FROM DboEmployee d WHERE d.employeeName = :employeeName")
    , @NamedQuery(name = "DboEmployee.findByEmployeeLastName", query = "SELECT d FROM DboEmployee d WHERE d.employeeLastName = :employeeLastName")
    , @NamedQuery(name = "DboEmployee.findByEmployeeBirthday", query = "SELECT d FROM DboEmployee d WHERE d.employeeBirthday = :employeeBirthday")
    , @NamedQuery(name = "DboEmployee.findByEmployeeDui", query = "SELECT d FROM DboEmployee d WHERE d.employeeDui = :employeeDui")
    , @NamedQuery(name = "DboEmployee.findByEmployeeNit", query = "SELECT d FROM DboEmployee d WHERE d.employeeNit = :employeeNit")
    , @NamedQuery(name = "DboEmployee.findByEmployeeIsss", query = "SELECT d FROM DboEmployee d WHERE d.employeeIsss = :employeeIsss")
    , @NamedQuery(name = "DboEmployee.findByEmployeeNup", query = "SELECT d FROM DboEmployee d WHERE d.employeeNup = :employeeNup")
    , @NamedQuery(name = "DboEmployee.findByEmployeePassport", query = "SELECT d FROM DboEmployee d WHERE d.employeePassport = :employeePassport")
    , @NamedQuery(name = "DboEmployee.findByEmployeeHiredDate", query = "SELECT d FROM DboEmployee d WHERE d.employeeHiredDate = :employeeHiredDate")})
public class DboEmployee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_employee")
    private String idEmployee;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "employee_name")
    private String employeeName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "employee_last_name")
    private String employeeLastName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "employee_birthday")
    @Temporal(TemporalType.DATE)
    private Date employeeBirthday;
    @Size(max = 10)
    @Column(name = "employee_dui")
    private String employeeDui;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "employee_nit")
    private String employeeNit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "employee_isss")
    private String employeeIsss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "employee_nup")
    private String employeeNup;
    @Size(max = 45)
    @Column(name = "employee_passport")
    private String employeePassport;
    @Column(name = "employee_hired_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date employeeHiredDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profileEmployee")
    private Collection<DboEmployeeProfile> dboEmployeeProfileCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "dboEmployee")
    private DboUser dboUser;
    @JoinColumn(name = "employee_address", referencedColumnName = "id_address")
    @ManyToOne(optional = false)
    private DboAddress employeeAddress;
    @JoinColumn(name = "employee_afp", referencedColumnName = "id_afp")
    @ManyToOne(optional = false)
    private DboAfp employeeAfp;
    @JoinColumn(name = "employee_civil_status", referencedColumnName = "id_civil_status")
    @ManyToOne(optional = false)
    private DboCivilStatus employeeCivilStatus;
    @JoinColumn(name = "employee_country", referencedColumnName = "id_country")
    @ManyToOne(optional = false)
    private DboCountry employeeCountry;
    @JoinColumn(name = "employee_gender", referencedColumnName = "id_gender")
    @ManyToOne(optional = false)
    private DboGender employeeGender;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ecEmployee")
    private Collection<DboEmployeeContact> dboEmployeeContactCollection;

    public DboEmployee() {
    }

    public DboEmployee(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public DboEmployee(String idEmployee, String employeeName, String employeeLastName, Date employeeBirthday, String employeeNit, String employeeIsss, String employeeNup) {
        this.idEmployee = idEmployee;
        this.employeeName = employeeName;
        this.employeeLastName = employeeLastName;
        this.employeeBirthday = employeeBirthday;
        this.employeeNit = employeeNit;
        this.employeeIsss = employeeIsss;
        this.employeeNup = employeeNup;
    }

    public String getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public void setEmployeeLastName(String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }

    public Date getEmployeeBirthday() {
        return employeeBirthday;
    }

    public void setEmployeeBirthday(Date employeeBirthday) {
        this.employeeBirthday = employeeBirthday;
    }

    public String getEmployeeDui() {
        return employeeDui;
    }

    public void setEmployeeDui(String employeeDui) {
        this.employeeDui = employeeDui;
    }

    public String getEmployeeNit() {
        return employeeNit;
    }

    public void setEmployeeNit(String employeeNit) {
        this.employeeNit = employeeNit;
    }

    public String getEmployeeIsss() {
        return employeeIsss;
    }

    public void setEmployeeIsss(String employeeIsss) {
        this.employeeIsss = employeeIsss;
    }

    public String getEmployeeNup() {
        return employeeNup;
    }

    public void setEmployeeNup(String employeeNup) {
        this.employeeNup = employeeNup;
    }

    public String getEmployeePassport() {
        return employeePassport;
    }

    public void setEmployeePassport(String employeePassport) {
        this.employeePassport = employeePassport;
    }

    public Date getEmployeeHiredDate() {
        return employeeHiredDate;
    }

    public void setEmployeeHiredDate(Date employeeHiredDate) {
        this.employeeHiredDate = employeeHiredDate;
    }

    @XmlTransient
    public Collection<DboEmployeeProfile> getDboEmployeeProfileCollection() {
        return dboEmployeeProfileCollection;
    }

    public void setDboEmployeeProfileCollection(Collection<DboEmployeeProfile> dboEmployeeProfileCollection) {
        this.dboEmployeeProfileCollection = dboEmployeeProfileCollection;
    }

    public DboUser getDboUser() {
        return dboUser;
    }

    public void setDboUser(DboUser dboUser) {
        this.dboUser = dboUser;
    }

    public DboAddress getEmployeeAddress() {
        return employeeAddress;
    }

    public void setEmployeeAddress(DboAddress employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public DboAfp getEmployeeAfp() {
        return employeeAfp;
    }

    public void setEmployeeAfp(DboAfp employeeAfp) {
        this.employeeAfp = employeeAfp;
    }

    public DboCivilStatus getEmployeeCivilStatus() {
        return employeeCivilStatus;
    }

    public void setEmployeeCivilStatus(DboCivilStatus employeeCivilStatus) {
        this.employeeCivilStatus = employeeCivilStatus;
    }

    public DboCountry getEmployeeCountry() {
        return employeeCountry;
    }

    public void setEmployeeCountry(DboCountry employeeCountry) {
        this.employeeCountry = employeeCountry;
    }

    public DboGender getEmployeeGender() {
        return employeeGender;
    }

    public void setEmployeeGender(DboGender employeeGender) {
        this.employeeGender = employeeGender;
    }

    @XmlTransient
    public Collection<DboEmployeeContact> getDboEmployeeContactCollection() {
        return dboEmployeeContactCollection;
    }

    public void setDboEmployeeContactCollection(Collection<DboEmployeeContact> dboEmployeeContactCollection) {
        this.dboEmployeeContactCollection = dboEmployeeContactCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmployee != null ? idEmployee.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboEmployee)) {
            return false;
        }
        DboEmployee other = (DboEmployee) object;
        if ((this.idEmployee == null && other.idEmployee != null) || (this.idEmployee != null && !this.idEmployee.equals(other.idEmployee))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboEmployee[ idEmployee=" + idEmployee + " ]";
    }
    
}
