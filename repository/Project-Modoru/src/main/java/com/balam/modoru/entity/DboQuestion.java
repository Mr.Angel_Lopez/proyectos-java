/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_question")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboQuestion.findAll", query = "SELECT d FROM DboQuestion d")
    , @NamedQuery(name = "DboQuestion.findByIdQuestion", query = "SELECT d FROM DboQuestion d WHERE d.idQuestion = :idQuestion")
    , @NamedQuery(name = "DboQuestion.findByQuestionQuestion", query = "SELECT d FROM DboQuestion d WHERE d.questionQuestion = :questionQuestion")})
public class DboQuestion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_question")
    private Integer idQuestion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "question_question")
    private String questionQuestion;
    @OneToMany(mappedBy = "recoveryQuestion")
    private Collection<DboRecovery> dboRecoveryCollection;

    public DboQuestion() {
    }

    public DboQuestion(Integer idQuestion) {
        this.idQuestion = idQuestion;
    }

    public DboQuestion(Integer idQuestion, String questionQuestion) {
        this.idQuestion = idQuestion;
        this.questionQuestion = questionQuestion;
    }

    public Integer getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Integer idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getQuestionQuestion() {
        return questionQuestion;
    }

    public void setQuestionQuestion(String questionQuestion) {
        this.questionQuestion = questionQuestion;
    }

    @XmlTransient
    public Collection<DboRecovery> getDboRecoveryCollection() {
        return dboRecoveryCollection;
    }

    public void setDboRecoveryCollection(Collection<DboRecovery> dboRecoveryCollection) {
        this.dboRecoveryCollection = dboRecoveryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idQuestion != null ? idQuestion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboQuestion)) {
            return false;
        }
        DboQuestion other = (DboQuestion) object;
        if ((this.idQuestion == null && other.idQuestion != null) || (this.idQuestion != null && !this.idQuestion.equals(other.idQuestion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboQuestion[ idQuestion=" + idQuestion + " ]";
    }
    
}
