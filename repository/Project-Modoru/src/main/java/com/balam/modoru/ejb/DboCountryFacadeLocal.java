/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboCountry;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboCountryFacadeLocal {

    void create(DboCountry dboCountry);

    void edit(DboCountry dboCountry);

    void remove(DboCountry dboCountry);

    DboCountry find(Object id);

    List<DboCountry> findAll();

    List<DboCountry> findRange(int[] range);

    int count();
    
}
