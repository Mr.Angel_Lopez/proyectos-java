/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_activity_log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboActivityLog.findAll", query = "SELECT d FROM DboActivityLog d")
    , @NamedQuery(name = "DboActivityLog.findByIdActivityLog", query = "SELECT d FROM DboActivityLog d WHERE d.idActivityLog = :idActivityLog")
    , @NamedQuery(name = "DboActivityLog.findByActivityAction", query = "SELECT d FROM DboActivityLog d WHERE d.activityAction = :activityAction")
    , @NamedQuery(name = "DboActivityLog.findByActivityDate", query = "SELECT d FROM DboActivityLog d WHERE d.activityDate = :activityDate")})
public class DboActivityLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "id_activity_log")
    private String idActivityLog;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "activity_action")
    private String activityAction;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "activity_description")
    private String activityDescription;
    @Column(name = "activity_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activityDate;
    @JoinColumn(name = "activity_user", referencedColumnName = "id_user")
    @ManyToOne(optional = false)
    private DboUser activityUser;

    public DboActivityLog() {
    }

    public DboActivityLog(String idActivityLog) {
        this.idActivityLog = idActivityLog;
    }

    public DboActivityLog(String idActivityLog, String activityAction, String activityDescription) {
        this.idActivityLog = idActivityLog;
        this.activityAction = activityAction;
        this.activityDescription = activityDescription;
    }

    public String getIdActivityLog() {
        return idActivityLog;
    }

    public void setIdActivityLog(String idActivityLog) {
        this.idActivityLog = idActivityLog;
    }

    public String getActivityAction() {
        return activityAction;
    }

    public void setActivityAction(String activityAction) {
        this.activityAction = activityAction;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public Date getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    public DboUser getActivityUser() {
        return activityUser;
    }

    public void setActivityUser(DboUser activityUser) {
        this.activityUser = activityUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idActivityLog != null ? idActivityLog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboActivityLog)) {
            return false;
        }
        DboActivityLog other = (DboActivityLog) object;
        if ((this.idActivityLog == null && other.idActivityLog != null) || (this.idActivityLog != null && !this.idActivityLog.equals(other.idActivityLog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboActivityLog[ idActivityLog=" + idActivityLog + " ]";
    }
    
}
