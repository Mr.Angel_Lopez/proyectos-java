/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboGender;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboGenderFacadeLocal {

    void create(DboGender dboGender);

    void edit(DboGender dboGender);

    void remove(DboGender dboGender);

    DboGender find(Object id);

    List<DboGender> findAll();

    List<DboGender> findRange(int[] range);

    int count();
    
}
