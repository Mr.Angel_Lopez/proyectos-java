/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboCivilStatus;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboCivilStatusFacadeLocal {

    void create(DboCivilStatus dboCivilStatus);

    void edit(DboCivilStatus dboCivilStatus);

    void remove(DboCivilStatus dboCivilStatus);

    DboCivilStatus find(Object id);

    List<DboCivilStatus> findAll();

    List<DboCivilStatus> findRange(int[] range);

    int count();
    
}
