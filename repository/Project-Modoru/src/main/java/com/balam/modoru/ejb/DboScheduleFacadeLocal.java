/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboSchedule;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboScheduleFacadeLocal {

    void create(DboSchedule dboSchedule);

    void edit(DboSchedule dboSchedule);

    void remove(DboSchedule dboSchedule);

    DboSchedule find(Object id);

    List<DboSchedule> findAll();

    List<DboSchedule> findRange(int[] range);

    int count();
    
}
