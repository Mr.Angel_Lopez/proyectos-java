/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboStatus;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboStatusFacadeLocal {

    void create(DboStatus dboStatus);

    void edit(DboStatus dboStatus);

    void remove(DboStatus dboStatus);

    DboStatus find(Object id);

    List<DboStatus> findAll();

    List<DboStatus> findRange(int[] range);

    int count();
    
}
