/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "view_user_employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ViewUserEmployee.findAll", query = "SELECT v FROM ViewUserEmployee v")
    , @NamedQuery(name = "ViewUserEmployee.findByIdE", query = "SELECT v FROM ViewUserEmployee v WHERE v.idE = :idE")
    , @NamedQuery(name = "ViewUserEmployee.findByNameE", query = "SELECT v FROM ViewUserEmployee v WHERE v.nameE = :nameE")
    , @NamedQuery(name = "ViewUserEmployee.findByLastNameE", query = "SELECT v FROM ViewUserEmployee v WHERE v.lastNameE = :lastNameE")
    , @NamedQuery(name = "ViewUserEmployee.findByBirthdayE", query = "SELECT v FROM ViewUserEmployee v WHERE v.birthdayE = :birthdayE")
    , @NamedQuery(name = "ViewUserEmployee.findByDuiE", query = "SELECT v FROM ViewUserEmployee v WHERE v.duiE = :duiE")
    , @NamedQuery(name = "ViewUserEmployee.findByNitE", query = "SELECT v FROM ViewUserEmployee v WHERE v.nitE = :nitE")
    , @NamedQuery(name = "ViewUserEmployee.findByIsssE", query = "SELECT v FROM ViewUserEmployee v WHERE v.isssE = :isssE")
    , @NamedQuery(name = "ViewUserEmployee.findByCountryE", query = "SELECT v FROM ViewUserEmployee v WHERE v.countryE = :countryE")
    , @NamedQuery(name = "ViewUserEmployee.findByAfpE", query = "SELECT v FROM ViewUserEmployee v WHERE v.afpE = :afpE")
    , @NamedQuery(name = "ViewUserEmployee.findByNupE", query = "SELECT v FROM ViewUserEmployee v WHERE v.nupE = :nupE")
    , @NamedQuery(name = "ViewUserEmployee.findByGenderE", query = "SELECT v FROM ViewUserEmployee v WHERE v.genderE = :genderE")
    , @NamedQuery(name = "ViewUserEmployee.findByCivilStatusE", query = "SELECT v FROM ViewUserEmployee v WHERE v.civilStatusE = :civilStatusE")
    , @NamedQuery(name = "ViewUserEmployee.findByPassportE", query = "SELECT v FROM ViewUserEmployee v WHERE v.passportE = :passportE")
    , @NamedQuery(name = "ViewUserEmployee.findByAdressE", query = "SELECT v FROM ViewUserEmployee v WHERE v.adressE = :adressE")
    , @NamedQuery(name = "ViewUserEmployee.findByHireDateE", query = "SELECT v FROM ViewUserEmployee v WHERE v.hireDateE = :hireDateE")
    , @NamedQuery(name = "ViewUserEmployee.findByIdU", query = "SELECT v FROM ViewUserEmployee v WHERE v.idU = :idU")
    , @NamedQuery(name = "ViewUserEmployee.findByNameU", query = "SELECT v FROM ViewUserEmployee v WHERE v.nameU = :nameU")
    , @NamedQuery(name = "ViewUserEmployee.findByPassU", query = "SELECT v FROM ViewUserEmployee v WHERE v.passU = :passU")
    , @NamedQuery(name = "ViewUserEmployee.findByGroupU", query = "SELECT v FROM ViewUserEmployee v WHERE v.groupU = :groupU")
    , @NamedQuery(name = "ViewUserEmployee.findByEmailU", query = "SELECT v FROM ViewUserEmployee v WHERE v.emailU = :emailU")
    , @NamedQuery(name = "ViewUserEmployee.findByStatusU", query = "SELECT v FROM ViewUserEmployee v WHERE v.statusU = :statusU")})
public class ViewUserEmployee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_e")
    private String idE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name_e")
    private String nameE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "last_name_e")
    private String lastNameE;
    @Basic(optional = false)
    @NotNull
    @Column(name = "birthday_e")
    @Temporal(TemporalType.DATE)
    private Date birthdayE;
    @Size(max = 10)
    @Column(name = "dui_e")
    private String duiE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "nit_e")
    private String nitE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "isss_e")
    private String isssE;
    @Basic(optional = false)
    @NotNull
    @Column(name = "country_e")
    private int countryE;
    @Basic(optional = false)
    @NotNull
    @Column(name = "afp_e")
    private int afpE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nup_e")
    private String nupE;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gender_e")
    private int genderE;
    @Basic(optional = false)
    @NotNull
    @Column(name = "civil_status_e")
    private int civilStatusE;
    @Size(max = 45)
    @Column(name = "passport_e")
    private String passportE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "adress_e")
    private String adressE;
    @Column(name = "hire_date_e")
    @Temporal(TemporalType.TIMESTAMP)
    private Date hireDateE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_u")
    private String idU;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name_u")
    private String nameU;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "pass_u")
    private String passU;
    @Basic(optional = false)
    @NotNull
    @Column(name = "group_u")
    private int groupU;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "email_u")
    private String emailU;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "recovery_u")
    private String recoveryU;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status_u")
    private int statusU;

    public ViewUserEmployee() {
    }

    public String getIdE() {
        return idE;
    }

    public void setIdE(String idE) {
        this.idE = idE;
    }

    public String getNameE() {
        return nameE;
    }

    public void setNameE(String nameE) {
        this.nameE = nameE;
    }

    public String getLastNameE() {
        return lastNameE;
    }

    public void setLastNameE(String lastNameE) {
        this.lastNameE = lastNameE;
    }

    public Date getBirthdayE() {
        return birthdayE;
    }

    public void setBirthdayE(Date birthdayE) {
        this.birthdayE = birthdayE;
    }

    public String getDuiE() {
        return duiE;
    }

    public void setDuiE(String duiE) {
        this.duiE = duiE;
    }

    public String getNitE() {
        return nitE;
    }

    public void setNitE(String nitE) {
        this.nitE = nitE;
    }

    public String getIsssE() {
        return isssE;
    }

    public void setIsssE(String isssE) {
        this.isssE = isssE;
    }

    public int getCountryE() {
        return countryE;
    }

    public void setCountryE(int countryE) {
        this.countryE = countryE;
    }

    public int getAfpE() {
        return afpE;
    }

    public void setAfpE(int afpE) {
        this.afpE = afpE;
    }

    public String getNupE() {
        return nupE;
    }

    public void setNupE(String nupE) {
        this.nupE = nupE;
    }

    public int getGenderE() {
        return genderE;
    }

    public void setGenderE(int genderE) {
        this.genderE = genderE;
    }

    public int getCivilStatusE() {
        return civilStatusE;
    }

    public void setCivilStatusE(int civilStatusE) {
        this.civilStatusE = civilStatusE;
    }

    public String getPassportE() {
        return passportE;
    }

    public void setPassportE(String passportE) {
        this.passportE = passportE;
    }

    public String getAdressE() {
        return adressE;
    }

    public void setAdressE(String adressE) {
        this.adressE = adressE;
    }

    public Date getHireDateE() {
        return hireDateE;
    }

    public void setHireDateE(Date hireDateE) {
        this.hireDateE = hireDateE;
    }

    public String getIdU() {
        return idU;
    }

    public void setIdU(String idU) {
        this.idU = idU;
    }

    public String getNameU() {
        return nameU;
    }

    public void setNameU(String nameU) {
        this.nameU = nameU;
    }

    public String getPassU() {
        return passU;
    }

    public void setPassU(String passU) {
        this.passU = passU;
    }

    public int getGroupU() {
        return groupU;
    }

    public void setGroupU(int groupU) {
        this.groupU = groupU;
    }

    public String getEmailU() {
        return emailU;
    }

    public void setEmailU(String emailU) {
        this.emailU = emailU;
    }

    public String getRecoveryU() {
        return recoveryU;
    }

    public void setRecoveryU(String recoveryU) {
        this.recoveryU = recoveryU;
    }

    public int getStatusU() {
        return statusU;
    }

    public void setStatusU(int statusU) {
        this.statusU = statusU;
    }
    
}
