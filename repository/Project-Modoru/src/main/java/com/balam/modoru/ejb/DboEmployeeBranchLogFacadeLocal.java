/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboEmployeeBranchLog;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboEmployeeBranchLogFacadeLocal {

    void create(DboEmployeeBranchLog dboEmployeeBranchLog);

    void edit(DboEmployeeBranchLog dboEmployeeBranchLog);

    void remove(DboEmployeeBranchLog dboEmployeeBranchLog);

    DboEmployeeBranchLog find(Object id);

    List<DboEmployeeBranchLog> findAll();

    List<DboEmployeeBranchLog> findRange(int[] range);

    int count();
    
}
