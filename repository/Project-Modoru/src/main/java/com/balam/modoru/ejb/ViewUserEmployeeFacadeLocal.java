/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.ViewUserEmployee;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface ViewUserEmployeeFacadeLocal {

    void create(ViewUserEmployee viewUserEmployee);

    void edit(ViewUserEmployee viewUserEmployee);

    void remove(ViewUserEmployee viewUserEmployee);

    ViewUserEmployee find(Object id);

    List<ViewUserEmployee> findAll();

    List<ViewUserEmployee> findRange(int[] range);

    int count();
    
}
