/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_contact")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboContact.findAll", query = "SELECT d FROM DboContact d")
    , @NamedQuery(name = "DboContact.findByIdContact", query = "SELECT d FROM DboContact d WHERE d.idContact = :idContact")
    , @NamedQuery(name = "DboContact.findByContactPriority", query = "SELECT d FROM DboContact d WHERE d.contactPriority = :contactPriority")
    , @NamedQuery(name = "DboContact.findByContactName", query = "SELECT d FROM DboContact d WHERE d.contactName = :contactName")
    , @NamedQuery(name = "DboContact.findByContactLastName", query = "SELECT d FROM DboContact d WHERE d.contactLastName = :contactLastName")
    , @NamedQuery(name = "DboContact.findByContactEmail", query = "SELECT d FROM DboContact d WHERE d.contactEmail = :contactEmail")
    , @NamedQuery(name = "DboContact.findByContactPhone", query = "SELECT d FROM DboContact d WHERE d.contactPhone = :contactPhone")
    , @NamedQuery(name = "DboContact.findByContactMovil", query = "SELECT d FROM DboContact d WHERE d.contactMovil = :contactMovil")})
public class DboContact implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_contact")
    private String idContact;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contact_priority")
    private int contactPriority;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "contact_name")
    private String contactName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "contact_last_name")
    private String contactLastName;
    @Size(max = 100)
    @Column(name = "contact_email")
    private String contactEmail;
    @Size(max = 45)
    @Column(name = "contact_phone")
    private String contactPhone;
    @Size(max = 45)
    @Column(name = "contact_movil")
    private String contactMovil;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ecContact")
    private Collection<DboEmployeeContact> dboEmployeeContactCollection;

    public DboContact() {
    }

    public DboContact(String idContact) {
        this.idContact = idContact;
    }

    public DboContact(String idContact, int contactPriority, String contactName, String contactLastName) {
        this.idContact = idContact;
        this.contactPriority = contactPriority;
        this.contactName = contactName;
        this.contactLastName = contactLastName;
    }

    public String getIdContact() {
        return idContact;
    }

    public void setIdContact(String idContact) {
        this.idContact = idContact;
    }

    public int getContactPriority() {
        return contactPriority;
    }

    public void setContactPriority(int contactPriority) {
        this.contactPriority = contactPriority;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactMovil() {
        return contactMovil;
    }

    public void setContactMovil(String contactMovil) {
        this.contactMovil = contactMovil;
    }

    @XmlTransient
    public Collection<DboEmployeeContact> getDboEmployeeContactCollection() {
        return dboEmployeeContactCollection;
    }

    public void setDboEmployeeContactCollection(Collection<DboEmployeeContact> dboEmployeeContactCollection) {
        this.dboEmployeeContactCollection = dboEmployeeContactCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContact != null ? idContact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboContact)) {
            return false;
        }
        DboContact other = (DboContact) object;
        if ((this.idContact == null && other.idContact != null) || (this.idContact != null && !this.idContact.equals(other.idContact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboContact[ idContact=" + idContact + " ]";
    }
    
}
