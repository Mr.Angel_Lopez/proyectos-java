/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_branch_office")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboBranchOffice.findAll", query = "SELECT d FROM DboBranchOffice d")
    , @NamedQuery(name = "DboBranchOffice.findByIdBranchOffice", query = "SELECT d FROM DboBranchOffice d WHERE d.idBranchOffice = :idBranchOffice")
    , @NamedQuery(name = "DboBranchOffice.findByBoName", query = "SELECT d FROM DboBranchOffice d WHERE d.boName = :boName")})
public class DboBranchOffice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_branch_office")
    private String idBranchOffice;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "bo_name")
    private String boName;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "bo_address")
    private String boAddress;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profileBranchOffice")
    private Collection<DboEmployeeProfile> dboEmployeeProfileCollection;
    @JoinColumn(name = "bo_status", referencedColumnName = "id_branch_office_status")
    @ManyToOne(optional = false)
    private DboBranchOfficeStatus boStatus;
    @JoinColumn(name = "bo_municipality", referencedColumnName = "id_municipality")
    @ManyToOne(optional = false)
    private DboMunicipality boMunicipality;

    public DboBranchOffice() {
    }

    public DboBranchOffice(String idBranchOffice) {
        this.idBranchOffice = idBranchOffice;
    }

    public DboBranchOffice(String idBranchOffice, String boName, String boAddress) {
        this.idBranchOffice = idBranchOffice;
        this.boName = boName;
        this.boAddress = boAddress;
    }

    public String getIdBranchOffice() {
        return idBranchOffice;
    }

    public void setIdBranchOffice(String idBranchOffice) {
        this.idBranchOffice = idBranchOffice;
    }

    public String getBoName() {
        return boName;
    }

    public void setBoName(String boName) {
        this.boName = boName;
    }

    public String getBoAddress() {
        return boAddress;
    }

    public void setBoAddress(String boAddress) {
        this.boAddress = boAddress;
    }

    @XmlTransient
    public Collection<DboEmployeeProfile> getDboEmployeeProfileCollection() {
        return dboEmployeeProfileCollection;
    }

    public void setDboEmployeeProfileCollection(Collection<DboEmployeeProfile> dboEmployeeProfileCollection) {
        this.dboEmployeeProfileCollection = dboEmployeeProfileCollection;
    }

    public DboBranchOfficeStatus getBoStatus() {
        return boStatus;
    }

    public void setBoStatus(DboBranchOfficeStatus boStatus) {
        this.boStatus = boStatus;
    }

    public DboMunicipality getBoMunicipality() {
        return boMunicipality;
    }

    public void setBoMunicipality(DboMunicipality boMunicipality) {
        this.boMunicipality = boMunicipality;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBranchOffice != null ? idBranchOffice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboBranchOffice)) {
            return false;
        }
        DboBranchOffice other = (DboBranchOffice) object;
        if ((this.idBranchOffice == null && other.idBranchOffice != null) || (this.idBranchOffice != null && !this.idBranchOffice.equals(other.idBranchOffice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboBranchOffice[ idBranchOffice=" + idBranchOffice + " ]";
    }
    
}
