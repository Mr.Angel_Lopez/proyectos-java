/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboUser;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboUserFacadeLocal {

    void create(DboUser dboUser);

    void edit(DboUser dboUser);

    void remove(DboUser dboUser);

    DboUser find(Object id);

    List<DboUser> findAll();

    List<DboUser> findRange(int[] range);

    int count();
    
}
