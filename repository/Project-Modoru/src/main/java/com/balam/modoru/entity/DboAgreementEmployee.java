/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_agreement_employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboAgreementEmployee.findAll", query = "SELECT d FROM DboAgreementEmployee d")
    , @NamedQuery(name = "DboAgreementEmployee.findByIdAgreementEmployee", query = "SELECT d FROM DboAgreementEmployee d WHERE d.idAgreementEmployee = :idAgreementEmployee")
    , @NamedQuery(name = "DboAgreementEmployee.findByAeEmployee", query = "SELECT d FROM DboAgreementEmployee d WHERE d.aeEmployee = :aeEmployee")
    , @NamedQuery(name = "DboAgreementEmployee.findByAeJob", query = "SELECT d FROM DboAgreementEmployee d WHERE d.aeJob = :aeJob")
    , @NamedQuery(name = "DboAgreementEmployee.findByAeJobName", query = "SELECT d FROM DboAgreementEmployee d WHERE d.aeJobName = :aeJobName")
    , @NamedQuery(name = "DboAgreementEmployee.findByAeSalary", query = "SELECT d FROM DboAgreementEmployee d WHERE d.aeSalary = :aeSalary")
    , @NamedQuery(name = "DboAgreementEmployee.findByAeBranchOffice", query = "SELECT d FROM DboAgreementEmployee d WHERE d.aeBranchOffice = :aeBranchOffice")
    , @NamedQuery(name = "DboAgreementEmployee.findByAeDate", query = "SELECT d FROM DboAgreementEmployee d WHERE d.aeDate = :aeDate")})
public class DboAgreementEmployee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_agreement_employee")
    private String idAgreementEmployee;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ae_employee")
    private String aeEmployee;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ae_job")
    private String aeJob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ae_job_name")
    private String aeJobName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ae_salary")
    private BigDecimal aeSalary;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ae_branch_office")
    private String aeBranchOffice;
    @Column(name = "ae_date")
    @Temporal(TemporalType.DATE)
    private Date aeDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profileAgreement")
    private Collection<DboEmployeeProfile> dboEmployeeProfileCollection;

    public DboAgreementEmployee() {
    }

    public DboAgreementEmployee(String idAgreementEmployee) {
        this.idAgreementEmployee = idAgreementEmployee;
    }

    public DboAgreementEmployee(String idAgreementEmployee, String aeEmployee, String aeJob, String aeJobName, BigDecimal aeSalary, String aeBranchOffice) {
        this.idAgreementEmployee = idAgreementEmployee;
        this.aeEmployee = aeEmployee;
        this.aeJob = aeJob;
        this.aeJobName = aeJobName;
        this.aeSalary = aeSalary;
        this.aeBranchOffice = aeBranchOffice;
    }

    public String getIdAgreementEmployee() {
        return idAgreementEmployee;
    }

    public void setIdAgreementEmployee(String idAgreementEmployee) {
        this.idAgreementEmployee = idAgreementEmployee;
    }

    public String getAeEmployee() {
        return aeEmployee;
    }

    public void setAeEmployee(String aeEmployee) {
        this.aeEmployee = aeEmployee;
    }

    public String getAeJob() {
        return aeJob;
    }

    public void setAeJob(String aeJob) {
        this.aeJob = aeJob;
    }

    public String getAeJobName() {
        return aeJobName;
    }

    public void setAeJobName(String aeJobName) {
        this.aeJobName = aeJobName;
    }

    public BigDecimal getAeSalary() {
        return aeSalary;
    }

    public void setAeSalary(BigDecimal aeSalary) {
        this.aeSalary = aeSalary;
    }

    public String getAeBranchOffice() {
        return aeBranchOffice;
    }

    public void setAeBranchOffice(String aeBranchOffice) {
        this.aeBranchOffice = aeBranchOffice;
    }

    public Date getAeDate() {
        return aeDate;
    }

    public void setAeDate(Date aeDate) {
        this.aeDate = aeDate;
    }

    @XmlTransient
    public Collection<DboEmployeeProfile> getDboEmployeeProfileCollection() {
        return dboEmployeeProfileCollection;
    }

    public void setDboEmployeeProfileCollection(Collection<DboEmployeeProfile> dboEmployeeProfileCollection) {
        this.dboEmployeeProfileCollection = dboEmployeeProfileCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAgreementEmployee != null ? idAgreementEmployee.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboAgreementEmployee)) {
            return false;
        }
        DboAgreementEmployee other = (DboAgreementEmployee) object;
        if ((this.idAgreementEmployee == null && other.idAgreementEmployee != null) || (this.idAgreementEmployee != null && !this.idAgreementEmployee.equals(other.idAgreementEmployee))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboAgreementEmployee[ idAgreementEmployee=" + idAgreementEmployee + " ]";
    }
    
}
