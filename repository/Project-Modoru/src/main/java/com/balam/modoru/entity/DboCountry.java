/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_country")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboCountry.findAll", query = "SELECT d FROM DboCountry d")
    , @NamedQuery(name = "DboCountry.findByIdCountry", query = "SELECT d FROM DboCountry d WHERE d.idCountry = :idCountry")
    , @NamedQuery(name = "DboCountry.findByCountryName", query = "SELECT d FROM DboCountry d WHERE d.countryName = :countryName")
    , @NamedQuery(name = "DboCountry.findByCountryNacionalityM", query = "SELECT d FROM DboCountry d WHERE d.countryNacionalityM = :countryNacionalityM")
    , @NamedQuery(name = "DboCountry.findByCountryNacionalityF", query = "SELECT d FROM DboCountry d WHERE d.countryNacionalityF = :countryNacionalityF")})
public class DboCountry implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_country")
    private Integer idCountry;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "country_name")
    private String countryName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "country_nacionality_m")
    private String countryNacionalityM;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "country_nacionality_f")
    private String countryNacionalityF;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeCountry")
    private Collection<DboEmployee> dboEmployeeCollection;

    public DboCountry() {
    }

    public DboCountry(Integer idCountry) {
        this.idCountry = idCountry;
    }

    public DboCountry(Integer idCountry, String countryName, String countryNacionalityM, String countryNacionalityF) {
        this.idCountry = idCountry;
        this.countryName = countryName;
        this.countryNacionalityM = countryNacionalityM;
        this.countryNacionalityF = countryNacionalityF;
    }

    public Integer getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Integer idCountry) {
        this.idCountry = idCountry;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryNacionalityM() {
        return countryNacionalityM;
    }

    public void setCountryNacionalityM(String countryNacionalityM) {
        this.countryNacionalityM = countryNacionalityM;
    }

    public String getCountryNacionalityF() {
        return countryNacionalityF;
    }

    public void setCountryNacionalityF(String countryNacionalityF) {
        this.countryNacionalityF = countryNacionalityF;
    }

    @XmlTransient
    public Collection<DboEmployee> getDboEmployeeCollection() {
        return dboEmployeeCollection;
    }

    public void setDboEmployeeCollection(Collection<DboEmployee> dboEmployeeCollection) {
        this.dboEmployeeCollection = dboEmployeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCountry != null ? idCountry.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboCountry)) {
            return false;
        }
        DboCountry other = (DboCountry) object;
        if ((this.idCountry == null && other.idCountry != null) || (this.idCountry != null && !this.idCountry.equals(other.idCountry))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboCountry[ idCountry=" + idCountry + " ]";
    }
    
}
