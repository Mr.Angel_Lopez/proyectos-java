/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_afp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboAfp.findAll", query = "SELECT d FROM DboAfp d")
    , @NamedQuery(name = "DboAfp.findByIdAfp", query = "SELECT d FROM DboAfp d WHERE d.idAfp = :idAfp")
    , @NamedQuery(name = "DboAfp.findByAfpName", query = "SELECT d FROM DboAfp d WHERE d.afpName = :afpName")})
public class DboAfp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_afp")
    private Integer idAfp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "afp_name")
    private String afpName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeAfp")
    private Collection<DboEmployee> dboEmployeeCollection;

    public DboAfp() {
    }

    public DboAfp(Integer idAfp) {
        this.idAfp = idAfp;
    }

    public DboAfp(Integer idAfp, String afpName) {
        this.idAfp = idAfp;
        this.afpName = afpName;
    }

    public Integer getIdAfp() {
        return idAfp;
    }

    public void setIdAfp(Integer idAfp) {
        this.idAfp = idAfp;
    }

    public String getAfpName() {
        return afpName;
    }

    public void setAfpName(String afpName) {
        this.afpName = afpName;
    }

    @XmlTransient
    public Collection<DboEmployee> getDboEmployeeCollection() {
        return dboEmployeeCollection;
    }

    public void setDboEmployeeCollection(Collection<DboEmployee> dboEmployeeCollection) {
        this.dboEmployeeCollection = dboEmployeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAfp != null ? idAfp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboAfp)) {
            return false;
        }
        DboAfp other = (DboAfp) object;
        if ((this.idAfp == null && other.idAfp != null) || (this.idAfp != null && !this.idAfp.equals(other.idAfp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboAfp[ idAfp=" + idAfp + " ]";
    }
    
}
