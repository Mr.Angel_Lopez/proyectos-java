/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel.lopezusam
 */
@Entity
@Table(name = "dbo_address")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DboAddress.findAll", query = "SELECT d FROM DboAddress d")
    , @NamedQuery(name = "DboAddress.findByIdAddress", query = "SELECT d FROM DboAddress d WHERE d.idAddress = :idAddress")})
public class DboAddress implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "id_address")
    private String idAddress;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "employee_address")
    private String employeeAddress;
    @JoinColumn(name = "employee_municipality", referencedColumnName = "id_municipality")
    @ManyToOne(optional = false)
    private DboMunicipality employeeMunicipality;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeAddress")
    private Collection<DboEmployee> dboEmployeeCollection;

    public DboAddress() {
    }

    public DboAddress(String idAddress) {
        this.idAddress = idAddress;
    }

    public DboAddress(String idAddress, String employeeAddress) {
        this.idAddress = idAddress;
        this.employeeAddress = employeeAddress;
    }

    public String getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(String idAddress) {
        this.idAddress = idAddress;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public DboMunicipality getEmployeeMunicipality() {
        return employeeMunicipality;
    }

    public void setEmployeeMunicipality(DboMunicipality employeeMunicipality) {
        this.employeeMunicipality = employeeMunicipality;
    }

    @XmlTransient
    public Collection<DboEmployee> getDboEmployeeCollection() {
        return dboEmployeeCollection;
    }

    public void setDboEmployeeCollection(Collection<DboEmployee> dboEmployeeCollection) {
        this.dboEmployeeCollection = dboEmployeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAddress != null ? idAddress.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DboAddress)) {
            return false;
        }
        DboAddress other = (DboAddress) object;
        if ((this.idAddress == null && other.idAddress != null) || (this.idAddress != null && !this.idAddress.equals(other.idAddress))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.balam.modoru.entity.DboAddress[ idAddress=" + idAddress + " ]";
    }
    
}
