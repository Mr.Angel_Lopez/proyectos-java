/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboPayroll;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboPayrollFacadeLocal {

    void create(DboPayroll dboPayroll);

    void edit(DboPayroll dboPayroll);

    void remove(DboPayroll dboPayroll);

    DboPayroll find(Object id);

    List<DboPayroll> findAll();

    List<DboPayroll> findRange(int[] range);

    int count();
    
}
