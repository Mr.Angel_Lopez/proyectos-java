/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboActivityLog;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboActivityLogFacadeLocal {

    void create(DboActivityLog dboActivityLog);

    void edit(DboActivityLog dboActivityLog);

    void remove(DboActivityLog dboActivityLog);

    DboActivityLog find(Object id);

    List<DboActivityLog> findAll();

    List<DboActivityLog> findRange(int[] range);

    int count();
    
}
