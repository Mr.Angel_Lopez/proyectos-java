/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboStatusEmployeeLog;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboStatusEmployeeLogFacadeLocal {

    void create(DboStatusEmployeeLog dboStatusEmployeeLog);

    void edit(DboStatusEmployeeLog dboStatusEmployeeLog);

    void remove(DboStatusEmployeeLog dboStatusEmployeeLog);

    DboStatusEmployeeLog find(Object id);

    List<DboStatusEmployeeLog> findAll();

    List<DboStatusEmployeeLog> findRange(int[] range);

    int count();
    
}
