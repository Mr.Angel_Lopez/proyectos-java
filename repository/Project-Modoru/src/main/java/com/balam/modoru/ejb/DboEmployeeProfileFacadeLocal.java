/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboEmployeeProfile;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboEmployeeProfileFacadeLocal {

    void create(DboEmployeeProfile dboEmployeeProfile);

    void edit(DboEmployeeProfile dboEmployeeProfile);

    void remove(DboEmployeeProfile dboEmployeeProfile);

    DboEmployeeProfile find(Object id);

    List<DboEmployeeProfile> findAll();

    List<DboEmployeeProfile> findRange(int[] range);

    int count();
    
}
