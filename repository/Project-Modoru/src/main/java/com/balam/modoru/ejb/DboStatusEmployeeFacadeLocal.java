/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboStatusEmployee;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboStatusEmployeeFacadeLocal {

    void create(DboStatusEmployee dboStatusEmployee);

    void edit(DboStatusEmployee dboStatusEmployee);

    void remove(DboStatusEmployee dboStatusEmployee);

    DboStatusEmployee find(Object id);

    List<DboStatusEmployee> findAll();

    List<DboStatusEmployee> findRange(int[] range);

    int count();
    
}
