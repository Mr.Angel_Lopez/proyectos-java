/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.balam.modoru.ejb;

import com.balam.modoru.entity.DboGroup;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author angel.lopezusam
 */
@Local
public interface DboGroupFacadeLocal {

    void create(DboGroup dboGroup);

    void edit(DboGroup dboGroup);

    void remove(DboGroup dboGroup);

    DboGroup find(Object id);

    List<DboGroup> findAll();

    List<DboGroup> findRange(int[] range);

    int count();
    
}
