CREATE DATABASE  IF NOT EXISTS `dbmodoru` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dbmodoru`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: dbmodoru
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dbo_activity_log`
--

DROP TABLE IF EXISTS `dbo_activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_activity_log` (
  `id_activity_log` varchar(200) NOT NULL,
  `activity_action` varchar(45) NOT NULL,
  `activity_description` text NOT NULL,
  `activity_user` varchar(100) NOT NULL,
  `activity_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_activity_log`),
  KEY `fk_dbo_activity_log_dbo_user1_idx` (`activity_user`),
  CONSTRAINT `fk_dbo_activity_log_dbo_user1` FOREIGN KEY (`activity_user`) REFERENCES `dbo_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_activity_log`
--

LOCK TABLES `dbo_activity_log` WRITE;
/*!40000 ALTER TABLE `dbo_activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_address`
--

DROP TABLE IF EXISTS `dbo_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_address` (
  `id_address` varchar(45) NOT NULL,
  `employee_municipality` int(10) unsigned NOT NULL,
  `employee_address` text NOT NULL,
  PRIMARY KEY (`id_address`),
  KEY `fk_dbo_employee_address_dbo_municipality1_idx` (`employee_municipality`),
  CONSTRAINT `fk_dbo_employee_address_dbo_municipality1` FOREIGN KEY (`employee_municipality`) REFERENCES `dbo_municipality` (`id_municipality`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_address`
--

LOCK TABLES `dbo_address` WRITE;
/*!40000 ALTER TABLE `dbo_address` DISABLE KEYS */;
INSERT INTO `dbo_address` VALUES ('1',2,'Colonia Rosa, pasaje 2 casa#12'),('111',111,'111'),('123456789',1,'prueba'),('2',3,'Villa Linda, casa #120 '),('3',2,'Colonia El Rosario, casa #20 pol.1'),('4',7,'Res. San Jose, casa #1 pol. 2'),('5',5,'Colonia Los Navos, casa #15 '),('6',6,'6'),('qwe',1,'qweqwe');
/*!40000 ALTER TABLE `dbo_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_afp`
--

DROP TABLE IF EXISTS `dbo_afp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_afp` (
  `id_afp` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `afp_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_afp`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_afp`
--

LOCK TABLES `dbo_afp` WRITE;
/*!40000 ALTER TABLE `dbo_afp` DISABLE KEYS */;
INSERT INTO `dbo_afp` VALUES (1,'AFP Crecer'),(2,'AFP Confia');
/*!40000 ALTER TABLE `dbo_afp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_agreement_employee`
--

DROP TABLE IF EXISTS `dbo_agreement_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_agreement_employee` (
  `id_agreement_employee` varchar(100) NOT NULL,
  `ae_employee` varchar(100) NOT NULL,
  `ae_job` varchar(100) NOT NULL,
  `ae_job_name` varchar(200) NOT NULL,
  `ae_salary` decimal(8,2) NOT NULL,
  `ae_branch_office` varchar(100) NOT NULL,
  `ae_date` date DEFAULT NULL,
  PRIMARY KEY (`id_agreement_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_agreement_employee`
--

LOCK TABLES `dbo_agreement_employee` WRITE;
/*!40000 ALTER TABLE `dbo_agreement_employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_agreement_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_branch_office`
--

DROP TABLE IF EXISTS `dbo_branch_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_branch_office` (
  `id_branch_office` varchar(100) NOT NULL,
  `bo_name` varchar(200) NOT NULL,
  `bo_address` text CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `bo_municipality` int(10) unsigned NOT NULL,
  `bo_status` int(11) NOT NULL,
  PRIMARY KEY (`id_branch_office`),
  KEY `fk_dbo_local_dbo_municipality1_idx` (`bo_municipality`),
  KEY `fk_dbo_branch_office_dbo_branch_office_status1_idx` (`bo_status`),
  CONSTRAINT `fk_dbo_branch_office_dbo_branch_office_status1` FOREIGN KEY (`bo_status`) REFERENCES `dbo_branch_office_status` (`id_branch_office_status`),
  CONSTRAINT `fk_dbo_local_dbo_municipality1` FOREIGN KEY (`bo_municipality`) REFERENCES `dbo_municipality` (`id_municipality`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_branch_office`
--

LOCK TABLES `dbo_branch_office` WRITE;
/*!40000 ALTER TABLE `dbo_branch_office` DISABLE KEYS */;
INSERT INTO `dbo_branch_office` VALUES ('BO-1-QWER','qwer','qwer',1,1),('BO-4-DFHBSDFGDSR','dfhbsdfgdsr','sdfgser ',4,1);
/*!40000 ALTER TABLE `dbo_branch_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_branch_office_status`
--

DROP TABLE IF EXISTS `dbo_branch_office_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_branch_office_status` (
  `id_branch_office_status` int(11) NOT NULL AUTO_INCREMENT,
  `bos_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_branch_office_status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_branch_office_status`
--

LOCK TABLES `dbo_branch_office_status` WRITE;
/*!40000 ALTER TABLE `dbo_branch_office_status` DISABLE KEYS */;
INSERT INTO `dbo_branch_office_status` VALUES (1,'Activo'),(2,'Inactivo'),(3,'Cerrado'),(4,'Clausurado');
/*!40000 ALTER TABLE `dbo_branch_office_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_civil_status`
--

DROP TABLE IF EXISTS `dbo_civil_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_civil_status` (
  `id_civil_status` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `civil_status_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_civil_status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_civil_status`
--

LOCK TABLES `dbo_civil_status` WRITE;
/*!40000 ALTER TABLE `dbo_civil_status` DISABLE KEYS */;
INSERT INTO `dbo_civil_status` VALUES (1,'Soltero/a'),(2,'Casado/a'),(3,'Viudo/a');
/*!40000 ALTER TABLE `dbo_civil_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_contact`
--

DROP TABLE IF EXISTS `dbo_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_contact` (
  `id_contact` varchar(100) NOT NULL,
  `contact_priority` int(10) unsigned NOT NULL,
  `contact_name` varchar(200) NOT NULL,
  `contact_last_name` varchar(45) NOT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  `contact_phone` varchar(45) DEFAULT NULL,
  `contact_movil` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_contact`
--

LOCK TABLES `dbo_contact` WRITE;
/*!40000 ALTER TABLE `dbo_contact` DISABLE KEYS */;
INSERT INTO `dbo_contact` VALUES ('prueba',1,'prueba','prueba','prueba@prueba.com','12345678','12345678');
/*!40000 ALTER TABLE `dbo_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_country`
--

DROP TABLE IF EXISTS `dbo_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_country` (
  `id_country` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_name` varchar(45) NOT NULL,
  `country_nacionality_m` varchar(45) NOT NULL,
  `country_nacionality_f` varchar(45) NOT NULL,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_country`
--

LOCK TABLES `dbo_country` WRITE;
/*!40000 ALTER TABLE `dbo_country` DISABLE KEYS */;
INSERT INTO `dbo_country` VALUES (1,'Afganistán','Afgano','Afgana'),(2,'Alemania','Alemán','Alemana'),(3,'Arabia Saudita','Árabe','Árabe'),(4,'Argentina','Argentino','Argentina'),(5,'Australia','Australiano','Australiana'),(6,'Bélgica ','Belga','Belga'),(7,'Bolivia','Boliviano','Boliviana'),(8,'Brasil','Brasileño','Brasileña'),(9,'Camboya','Camboyano','Camboyana'),(10,'Canadá','Canadience','Canadiense'),(11,'Chile','Chileno','Chilena'),(12,'China','Chino','China'),(13,'Colombia','Colombiano','Colombiana'),(14,'Corea','Coreano','Coreana'),(15,'Costa Rica','Costarricense','Costarricense'),(16,'Cuba','Cubano','Cubana'),(17,'Dinamarca','Danés','Danesa'),(18,'Ecuador','Ecuatoriano','Ecuatoriana'),(19,'Egipto','Egipcio','Egipcia'),(20,'El Salvador','Salvadoreño ','Salvadoreña'),(21,'Escocia','Escocés','Escocesa'),(22,'Estados Unidos','Estadounidense','Estadounidense'),(23,'Estonia','Estonio','Estonia'),(24,'Etiopia','Etiope','Etiope'),(25,'Filipinas','Filipino','Filipina'),(26,'Finlandia','Finlandés','Finlandesa'),(27,'Francia','Francés','Francesa'),(28,'Gales','Galés','Galesa'),(29,'Grecia','Griego','Griega'),(30,'Guatemala','Guatemalteco','Guatemalteca'),(31,'Haití','Haitiano','Haitiana'),(32,'Holanda','Holandés','Holandesa'),(33,'Honduras','Hondureño','Hondureña'),(34,'Indonesia','Indonés','Indonesa'),(35,'Inglaterra','Inglés','Inglesa'),(36,'Irak','Iraquí','Iraquí'),(37,'Iran ','Iraní','Iraní'),(38,'Irlanda','Irlandés','Irlandesa'),(39,'Israel','Israelí','Israelí'),(40,'Italia','Italiano','Italiana'),(41,'Japón','Japonés','Japonesa'),(42,'Jordania ','Jordano','Jordana'),(43,'Laos','Laosiano','Laosiana'),(44,'Letonia','Letón','Letona'),(45,'Lituania','Letonés','Letonesa'),(46,'Malasia','Malayo','Malaya'),(47,'Marruecos','Marroquí','Marroquí'),(48,'México',' Mexicano','Mexicana'),(49,'Nicaragua','Nicaragüense','Nicaragüense'),(50,'Noruega','Noruego','Noruega'),(51,'Nueva Zelanda','Neozelandés','Neozelandesa'),(52,'Panamá','Panameño','Panameña'),(53,'Paraguay','Paraguayo','Paraguaya'),(54,'Polonia','Polaco','Polaca'),(55,'Portugarl','Portugués','Portuguesa'),(56,'Puerto Rico','Puertorriqueño','Puertorriqueña'),(57,'Republica Dominicana','Dominicano','Dominicana'),(58,'Rumania','Rumano','Rumana'),(59,'Rusia','Ruso','Rusa'),(60,'Suecia','Sueco','Sueca'),(61,'Suiza','Suizo','Suiza'),(63,'Tailandia','Tailoandés','Tailandesa'),(64,'Taiwán','Taiwanes','Taiwanesa'),(65,'Turquía','Turco','Turca'),(66,'Ucrania','Ucranianao','Ucraniana'),(67,'Uruguay','Uruguayo','Uruguaya'),(68,'Venezuela','Venezolano','Venezolana'),(69,'Vietnam','Vietnamita','Vietnamita');
/*!40000 ALTER TABLE `dbo_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_departament`
--

DROP TABLE IF EXISTS `dbo_departament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_departament` (
  `id_departament` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departament_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_departament`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_departament`
--

LOCK TABLES `dbo_departament` WRITE;
/*!40000 ALTER TABLE `dbo_departament` DISABLE KEYS */;
INSERT INTO `dbo_departament` VALUES (1,'Ahuachapán'),(2,'Cabañas'),(3,'Chalatenango'),(4,'Cuscatlán'),(5,'Morazán'),(6,'La Libertad'),(7,'La paz'),(8,'La Unión'),(9,'San Miguel'),(10,'San Salvador'),(11,'San Vicente'),(12,'Santa Ana'),(13,'Sonsonate'),(14,'Usulutan');
/*!40000 ALTER TABLE `dbo_departament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_division`
--

DROP TABLE IF EXISTS `dbo_division`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_division` (
  `id_division` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `divison_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_division`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_division`
--

LOCK TABLES `dbo_division` WRITE;
/*!40000 ALTER TABLE `dbo_division` DISABLE KEYS */;
INSERT INTO `dbo_division` VALUES (1,'1'),(2,'2'),(3,'3');
/*!40000 ALTER TABLE `dbo_division` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_employee`
--

DROP TABLE IF EXISTS `dbo_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_employee` (
  `id_employee` varchar(100) NOT NULL,
  `employee_name` varchar(45) NOT NULL,
  `employee_last_name` varchar(45) NOT NULL,
  `employee_birthday` date NOT NULL,
  `employee_dui` varchar(10) DEFAULT NULL,
  `employee_nit` varchar(17) NOT NULL,
  `employee_isss` varchar(20) NOT NULL,
  `employee_country` int(10) unsigned NOT NULL,
  `employee_afp` int(10) unsigned NOT NULL,
  `employee_nup` varchar(15) NOT NULL,
  `employee_gender` int(10) unsigned NOT NULL,
  `employee_civil_status` int(10) unsigned NOT NULL,
  `employee_passport` varchar(45) DEFAULT NULL,
  `employee_address` varchar(45) NOT NULL,
  `employee_hired_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_employee`),
  KEY `fk_dbo_employee_dbo_country1_idx` (`employee_country`),
  KEY `fk_dbo_employee_dbo_afp1_idx` (`employee_afp`),
  KEY `fk_dbo_employee_dbo_civil_status1_idx` (`employee_civil_status`),
  KEY `fk_dbo_employee_dbo_address1_idx` (`employee_address`),
  KEY `fk_dbo_employee_dbo_gender1_idx` (`employee_gender`),
  CONSTRAINT `fk_dbo_employee_dbo_address1` FOREIGN KEY (`employee_address`) REFERENCES `dbo_address` (`id_address`),
  CONSTRAINT `fk_dbo_employee_dbo_afp1` FOREIGN KEY (`employee_afp`) REFERENCES `dbo_afp` (`id_afp`),
  CONSTRAINT `fk_dbo_employee_dbo_civil_status1` FOREIGN KEY (`employee_civil_status`) REFERENCES `dbo_civil_status` (`id_civil_status`),
  CONSTRAINT `fk_dbo_employee_dbo_country1` FOREIGN KEY (`employee_country`) REFERENCES `dbo_country` (`id_country`),
  CONSTRAINT `fk_dbo_employee_dbo_gender1` FOREIGN KEY (`employee_gender`) REFERENCES `dbo_gender` (`id_gender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_employee`
--

LOCK TABLES `dbo_employee` WRITE;
/*!40000 ALTER TABLE `dbo_employee` DISABLE KEYS */;
INSERT INTO `dbo_employee` VALUES ('EMP-000001','Emanuel Adonay ','Rodriguez','1994-05-07','02154235-6','0653-123546-000-0','000060000',20,1,'348212546978231',2,1,'NULL','1','2019-12-02 00:00:00'),('EMP-000002','Samuel Adonay','Medina Martinez','1988-02-12','00024574-4','0614-290209-000-0','000235120',20,2,'231203845201657',2,2,'NULL','2','2019-10-02 00:00:00'),('EMP-000003','Oscar Ernesto','Lopez Suarez','1990-03-05','00352156-4','0253-651655-100-4','003236464',20,1,'213215865415219',2,1,'NULL','3','2010-02-10 00:00:00'),('EMP-000004','Angel Antonio ','Lopez','1995-05-06','01230320-5','5354-215410-155-2','023135351',23,1,'213584164154852',2,2,'','4','2015-01-04 00:00:00'),('EMP-000005','Alejandra Verónica','Centeno García','1995-04-05','02115103-4','0336-434312-110-2','024102546',20,2,'120165142049463',1,1,'','5','2015-03-04 00:00:00'),('prueba','prueba','prueba','2019-02-27','00000000-0','000-000-0000-0000','000000000-0000000000',1,1,'123456789101213',2,1,'123456789','123456789','2019-02-27 00:00:00'),('qwe','qwe','qwe','0001-01-01','qwe','qwe','qwe',1,1,'qwe',1,1,'qwe','qwe','0001-01-01 00:00:00');
/*!40000 ALTER TABLE `dbo_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_employee_branch_log`
--

DROP TABLE IF EXISTS `dbo_employee_branch_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_employee_branch_log` (
  `id_ebl` varchar(100) NOT NULL,
  `ebl_id_profile` varchar(100) NOT NULL,
  `ebl_start_date` date NOT NULL,
  `ebl_end_date` date NOT NULL,
  `ebl_branch` varchar(100) NOT NULL,
  PRIMARY KEY (`id_ebl`),
  KEY `fk_dbo_employee_branch_log_dbo_employee_profile1_idx` (`ebl_id_profile`),
  CONSTRAINT `fk_dbo_employee_branch_log_dbo_employee_profile1` FOREIGN KEY (`ebl_id_profile`) REFERENCES `dbo_employee_profile` (`id_profile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_employee_branch_log`
--

LOCK TABLES `dbo_employee_branch_log` WRITE;
/*!40000 ALTER TABLE `dbo_employee_branch_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_employee_branch_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_employee_contact`
--

DROP TABLE IF EXISTS `dbo_employee_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_employee_contact` (
  `id_employee_contact` int(11) NOT NULL,
  `ec_employee` varchar(100) NOT NULL,
  `ec_contact` varchar(100) NOT NULL,
  PRIMARY KEY (`id_employee_contact`),
  KEY `fk_dbo_employee_contact_dbo_employee_idx` (`ec_employee`),
  KEY `fk_dbo_employee_contact_dbo_contact1_idx` (`ec_contact`),
  CONSTRAINT `fk_dbo_employee_contact_dbo_contact1` FOREIGN KEY (`ec_contact`) REFERENCES `dbo_contact` (`id_contact`),
  CONSTRAINT `fk_dbo_employee_contact_dbo_employee` FOREIGN KEY (`ec_employee`) REFERENCES `dbo_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_employee_contact`
--

LOCK TABLES `dbo_employee_contact` WRITE;
/*!40000 ALTER TABLE `dbo_employee_contact` DISABLE KEYS */;
INSERT INTO `dbo_employee_contact` VALUES (1,'prueba','prueba');
/*!40000 ALTER TABLE `dbo_employee_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_employee_job_log`
--

DROP TABLE IF EXISTS `dbo_employee_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_employee_job_log` (
  `id_ejl` varchar(100) NOT NULL,
  `ejl_employee` varchar(100) NOT NULL,
  `ejl_employee_job` varchar(100) NOT NULL,
  `ejl_start_job` date NOT NULL,
  `ejl_end_job` date NOT NULL,
  PRIMARY KEY (`id_ejl`),
  KEY `fk_dbo_employee_job_log_dbo_employee_profile1_idx` (`ejl_employee`),
  CONSTRAINT `fk_dbo_employee_job_log_dbo_employee_profile1` FOREIGN KEY (`ejl_employee`) REFERENCES `dbo_employee_profile` (`profile_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_employee_job_log`
--

LOCK TABLES `dbo_employee_job_log` WRITE;
/*!40000 ALTER TABLE `dbo_employee_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_employee_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_employee_profile`
--

DROP TABLE IF EXISTS `dbo_employee_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_employee_profile` (
  `id_profile` varchar(100) NOT NULL,
  `profile_employee` varchar(100) NOT NULL,
  `profile_job` varchar(100) NOT NULL,
  `profile_agreement` varchar(45) NOT NULL,
  `profile_date` date DEFAULT NULL,
  `profile_branch_office` varchar(100) NOT NULL,
  PRIMARY KEY (`id_profile`),
  KEY `fk_dbo_employee_profile_dbo_employee1_idx` (`profile_employee`),
  KEY `fk_dbo_employee_profile_dbo_work1_idx` (`profile_job`),
  KEY `fk_dbo_employee_profile_dbo_branch_office1_idx` (`profile_branch_office`),
  KEY `fk_dbo_employee_profile_dbo_agreement_employee1_idx` (`profile_agreement`),
  CONSTRAINT `fk_dbo_employee_profile_dbo_agreement_employee1` FOREIGN KEY (`profile_agreement`) REFERENCES `dbo_agreement_employee` (`id_agreement_employee`),
  CONSTRAINT `fk_dbo_employee_profile_dbo_branch_office1` FOREIGN KEY (`profile_branch_office`) REFERENCES `dbo_branch_office` (`id_branch_office`),
  CONSTRAINT `fk_dbo_employee_profile_dbo_employee1` FOREIGN KEY (`profile_employee`) REFERENCES `dbo_employee` (`id_employee`),
  CONSTRAINT `fk_dbo_employee_profile_dbo_work1` FOREIGN KEY (`profile_job`) REFERENCES `dbo_job` (`id_job`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_employee_profile`
--

LOCK TABLES `dbo_employee_profile` WRITE;
/*!40000 ALTER TABLE `dbo_employee_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_employee_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_gender`
--

DROP TABLE IF EXISTS `dbo_gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_gender` (
  `id_gender` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gender_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_gender`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_gender`
--

LOCK TABLES `dbo_gender` WRITE;
/*!40000 ALTER TABLE `dbo_gender` DISABLE KEYS */;
INSERT INTO `dbo_gender` VALUES (1,'Femenino'),(2,'Masculino');
/*!40000 ALTER TABLE `dbo_gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_group`
--

DROP TABLE IF EXISTS `dbo_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_group` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_group`
--

LOCK TABLES `dbo_group` WRITE;
/*!40000 ALTER TABLE `dbo_group` DISABLE KEYS */;
INSERT INTO `dbo_group` VALUES (1,'ADMIN');
/*!40000 ALTER TABLE `dbo_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_job`
--

DROP TABLE IF EXISTS `dbo_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_job` (
  `id_job` varchar(100) NOT NULL,
  `job_name` varchar(45) NOT NULL,
  `job_division` int(10) unsigned NOT NULL,
  `job_salary` decimal(10,2) NOT NULL,
  `job_local` varchar(45) NOT NULL,
  PRIMARY KEY (`id_job`),
  KEY `fk_dbo_work_dbo_division1_idx` (`job_division`),
  CONSTRAINT `fk_dbo_work_dbo_division1` FOREIGN KEY (`job_division`) REFERENCES `dbo_division` (`id_division`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_job`
--

LOCK TABLES `dbo_job` WRITE;
/*!40000 ALTER TABLE `dbo_job` DISABLE KEYS */;
INSERT INTO `dbo_job` VALUES ('prueba','prueba',1,1.00,'prueba');
/*!40000 ALTER TABLE `dbo_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_job_schedule`
--

DROP TABLE IF EXISTS `dbo_job_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_job_schedule` (
  `id_job_schedule` varchar(100) NOT NULL,
  `job_schedule` varchar(100) NOT NULL,
  `job_job` varchar(100) NOT NULL,
  PRIMARY KEY (`id_job_schedule`),
  KEY `fk_dbo_job_time_dbo_job1_idx` (`job_job`),
  CONSTRAINT `fk_dbo_job_time_dbo_job1` FOREIGN KEY (`job_job`) REFERENCES `dbo_job` (`id_job`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_job_schedule`
--

LOCK TABLES `dbo_job_schedule` WRITE;
/*!40000 ALTER TABLE `dbo_job_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_job_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_municipality`
--

DROP TABLE IF EXISTS `dbo_municipality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_municipality` (
  `id_municipality` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `municipality_name` varchar(45) NOT NULL,
  `municipality_departament` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_municipality`),
  KEY `fk_dbo_municipality_dbo_departament1_idx` (`municipality_departament`),
  CONSTRAINT `fk_dbo_municipality_dbo_departament1` FOREIGN KEY (`municipality_departament`) REFERENCES `dbo_departament` (`id_departament`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_municipality`
--

LOCK TABLES `dbo_municipality` WRITE;
/*!40000 ALTER TABLE `dbo_municipality` DISABLE KEYS */;
INSERT INTO `dbo_municipality` VALUES (1,'Ahuachapán',1),(2,'Apaneca',1),(3,'Atiquizaya',1),(4,'Concepción de Ataco',1),(5,'El Refugio',1),(6,'Guaymango',1),(7,'Jujutla',1),(8,'San Francisco Menéndez',1),(9,'San Lorenzo',1),(10,'San Pedro Puxtla',1),(11,'Tacuba',1),(12,'Turín',1),(13,'Cinquera',2),(14,'Dolores',2),(15,'Guacotecti',2),(16,'Ilobasco',2),(17,'Jutiapa',2),(18,'San isidro',2),(19,'Sensuntepeque',2),(20,'Tejutepeque',2),(21,'Victoria',2),(22,'Citalá',3),(23,'Comalapa',3),(24,'Concepción Quezaltepeque',3),(25,'Dulce Nombre de María',3),(26,'El Carrizal',3),(27,'El Paraíso',3),(28,'La Laguna',3),(29,'La Palma',3),(30,'La Reina',3),(31,'Las Vueltas',3),(32,'Nombre de Jesús',3),(33,'Nueva Concepción',3),(34,'Nueva Trinidad',3),(35,'Ojos de Agua',3),(36,'Potonico',3),(37,'San Antonio de la Cruz',3),(38,'San Antonio Los Ranchosl',3),(39,'San Fernando',3),(40,'San Francisco Lempa',3),(41,'San Francisco Morazán',3),(42,'San Ingacio',3),(43,'San Isidro Labrador',3),(44,'San José Cancasque',3),(45,'San José La Flores',3),(46,'San Luis del Carmen ',3),(47,'San Miguel de Mercedes',3),(48,'San Rfael ',3),(49,'Santa Rita',3),(50,'Tejutla',3),(51,'Candelaria',4),(52,'Cojutepeque',4),(53,'El Carmen',4),(54,'El Rosario',4),(55,'Monte San Juan',4),(56,'Oratorio de Concepción',4),(57,'San Bartolomé Perulapía',4),(58,'San Cristóbal',4),(59,'San JoséGuayagual',4),(60,'San Pedro Perulapán',4),(61,'San Rafael Cedros',4),(62,'San Ramón',4),(63,'Santa Cruz Analquito',4),(64,'Santa Cruz Michapa ',4),(65,'Suchitoto',4),(66,'Tenancingo',4),(67,'Arambala',5),(68,'Cacaopera',5),(69,'Chilanga',5),(70,'Corinto',5),(71,'Delicias de Concepción',5),(72,'El Divisadero',5),(73,'EL Rosario',5),(74,'Gualococti',5),(75,'Guatajiagua',5),(76,'Joateca',5),(77,'Jocoatique',5),(78,'Jocoro',5),(79,'Lolotiquillo',5),(80,'Meanguera',5),(81,'Osicala',5),(82,'Perquín',5),(83,'San Carlos',5),(84,'San Fernado',5),(85,'San Francisco Gotera',5),(86,'San Isidro',5),(87,'San Simón',5),(88,'Sensembra',5),(89,'Sociedad',5),(90,'Torola',5),(91,'Yamabal',5),(92,'Yoloaiquín',5),(93,'Antiguo Cuscatlán',6),(94,'Chiltiupán',6),(95,'Ciudad Arce',6),(96,'Colón',6),(97,'Comasagua',6),(98,'Huizúcar',6),(99,'Jayaque',6),(100,'Jicalapa',6),(101,'La Libertad',6),(102,'Santa Tecla',6),(103,'Nuevo Cuscatlán',6),(104,'San Juan Opico',6),(105,'Quezaltepeque',6),(106,'Sacacoyo',6),(107,'San José Villanueva',6),(108,'San Matías',6),(109,'San Pablo Tacachico',6),(110,'Talnique',6),(111,'Tamanique',6),(112,'Teotepeque',6),(113,'Tepecoyo',6),(114,'Zaragoza',6),(115,'Cuyultitán',7),(116,'El Rosario de la Paz',7),(117,'Jerusalén',7),(118,'Mercedes La Ceiba',7),(119,'Olocuilta',7),(120,'Paraíso de Osorio',7),(121,'San Antonio Masahuat',7),(122,'San Emigndio',7),(123,'San Francisco Chinameca',7),(124,'San Juan Nonualco',7),(125,'San Juan Talpa',7),(126,'San Juan Tepezontes',7),(127,'San Luis La Herradura',7),(128,'San Luis Talpa',7),(129,'San Miguel Tepezontes',7),(130,'San PEDRO nONUALCO',7),(131,'San Rafael Obrajuelo',7),(132,'Santa María Ostuma',7),(133,'Santiago Nonualco',7),(134,'Tapalhuaca',7),(135,'Zacatecoluca',7),(136,'Anamorós',8),(137,'Bolívar',8),(138,'Concepción de Oriente',8),(139,'Conchagua',8),(140,'El Carmen',8),(141,'El Sauce',8),(142,'Intipucá',8),(143,'La Unión',8),(144,'Lilisque',8),(145,'Meanguera del Golfo',8),(146,'Nueva Esparta',8),(147,'Polorós',8),(148,'San Alejo',8),(149,'San José',8),(150,'Santa Rosa De Lima',8),(151,'Yayantique',8),(152,'Yucuaiquín',8),(153,'Carolina',9),(154,'Chapeltique',9),(155,'Chinameca',9),(156,'Chirilagua',9),(157,'Ciudad Barrios',9),(158,'Comacarán',9),(159,'El Tránsito',9),(160,'Lolotique',9),(161,'Moncagua',9),(162,'Nueva Guadalupe',9),(163,'Nuevo Edén de San Juan',9),(164,'Quelepa',9),(165,'San Antonio del Mosco',9),(166,'San Gerardo ',9),(167,'San Jorge',9),(168,'San Luis de la Reina',9),(169,'San Miguel',9),(170,'San Rafael Oriente',9),(171,'Sesori',9),(172,'Uluazapa',9),(173,'Aguilares',10),(174,'Apopa',10),(175,'Ayutuxtepeque',10),(176,'Delgado',10),(177,'Cuscatancingo',10),(178,'El Paisnal',10),(179,'Guazapa',10),(180,'Ilopango',10),(181,'Mejicanos',10),(182,'Nejapa',10),(183,'Panchimalco',10),(184,'Rosario de Mora',10),(185,'San Marcos',10),(186,'San Martín',10),(187,'San Salvador',10),(188,'Santiago Texacuangos',10),(189,'Santo TOmas',10),(190,'Soyapango',10),(191,'Tonacatepeque',10),(192,'Candelaria de la Frontera ',12),(193,'Chalchuapa',12),(194,'Coatepeque',12),(195,'El Congo',12),(196,'El Porvenir',12),(197,'Masahuat',12),(198,'Metapán',12),(199,'San Antonio Pajonal',12),(200,'San Sebastián Salitrillo',12),(201,'Santa Ana',12),(202,'Santa Rosa Guachipilín',12),(203,'Santiago de la Frontera',12),(204,'Texistepeque',12),(205,'Acajutla',13),(206,'Armenia',13),(207,'Caluco',13),(208,'Cuisnahuat',13),(209,'Izalco',13),(210,'Juayúa',13),(211,'Nahuizalco',13),(212,'Nahulingo',13),(213,'Salcoatitán',13),(214,'San Antonio del Monte',13),(215,'San Julián ',13),(216,'Santa Catarina Masahuat',13),(217,'Santa Isabel Ishuatán',13),(218,'Santo Domingo de Guzmán ',13),(219,'Sonsonate',13),(220,'Sonzacate',13),(221,'Alegría',14),(222,'Berlín',14),(223,'Califormia',14),(224,'Concepción  Batres',14),(225,'El Triunfo',14),(226,'Ereguayaquín',14),(227,'Estanzuelas',14),(228,'Jiquilisco',14),(229,'Juacuapa',14),(230,'Jucuarán',14),(231,'Mercedes Umaña',14),(232,'Nueva Granada',14),(233,'Ozatlán',14),(234,'Puerto El Triunfo',14),(235,'San Agustín',14),(236,'San Buenaventura',14),(237,'San Dionisio',14),(238,'San Francisco Javier',14),(239,'Santa Elena',14),(240,'Santa María',14),(241,'Santiago de María',14),(242,'Tecapán',14),(243,'Usulután',14),(244,'Agua Caliente',3),(245,'Arcatao',3),(246,'Chalatenango',3),(247,'Apastepeque',11),(248,'Guadalupe',11),(249,'San Cayetano Istepeque',11),(250,'San Esteban Catarina',11),(251,'San Ildefonso',11),(252,'San Lorenzo',11),(253,'San Sebastián',11),(254,'San Vicente',11),(255,'Santa Clara',11),(256,'Santo Domingo',11),(257,'Tecoluca',11),(258,'Tepetitán',11),(259,'Verapaz',11);
/*!40000 ALTER TABLE `dbo_municipality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_payroll`
--

DROP TABLE IF EXISTS `dbo_payroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_payroll` (
  `iddbo_payroll` int(11) NOT NULL,
  PRIMARY KEY (`iddbo_payroll`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_payroll`
--

LOCK TABLES `dbo_payroll` WRITE;
/*!40000 ALTER TABLE `dbo_payroll` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_payroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_question`
--

DROP TABLE IF EXISTS `dbo_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `question_question` varchar(200) NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_question`
--

LOCK TABLES `dbo_question` WRITE;
/*!40000 ALTER TABLE `dbo_question` DISABLE KEYS */;
INSERT INTO `dbo_question` VALUES (1,'Primer Animal de compañia'),(2,'Nombre de Soltera de la Madre'),(3,'Color Favorito'),(4,'Juguete Favorito de Niño'),(5,'Año de graduación del padre'),(6,'Año de nacimiento de bisabuela'),(7,'Fruta favorita del abuelo'),(8,'Marca primer smartphone');
/*!40000 ALTER TABLE `dbo_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_recovery`
--

DROP TABLE IF EXISTS `dbo_recovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_recovery` (
  `id_recovery` varchar(100) NOT NULL,
  `recovery_user` varchar(100) NOT NULL,
  `recovery_question` int(11) DEFAULT NULL,
  `recovery_anwser` text,
  PRIMARY KEY (`id_recovery`),
  KEY `fk_dbo_recovery_dbo_user1_idx` (`recovery_user`),
  KEY `fk_dbo_recovery_dbo_question1_idx` (`recovery_question`),
  CONSTRAINT `fk_dbo_recovery_dbo_question1` FOREIGN KEY (`recovery_question`) REFERENCES `dbo_question` (`id_question`),
  CONSTRAINT `fk_dbo_recovery_dbo_user1` FOREIGN KEY (`recovery_user`) REFERENCES `dbo_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_recovery`
--

LOCK TABLES `dbo_recovery` WRITE;
/*!40000 ALTER TABLE `dbo_recovery` DISABLE KEYS */;
INSERT INTO `dbo_recovery` VALUES ('RVY-EMP1-Adonay@ADMIN-1','EMP-000001',1,'b221d9dbb083a7f33428d7c2a3c3198ae925614d70210e28716ccaa7cd4ddb79'),('RVY-EMP1-Adonay@ADMIN-2','EMP-000001',3,'cb9c245f6cf4910aca447a02e910139b5456d63d53be538e386ed48472eaca5f'),('RVY-EMP1-Adonay@ADMIN-3','EMP-000001',7,'b4ca32d7b0dbfc0fe26905fa9bc513a2d16f853a9106e37cae6c62d3b5134b77');
/*!40000 ALTER TABLE `dbo_recovery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_schedule`
--

DROP TABLE IF EXISTS `dbo_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_schedule` (
  `id_schedule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_name` varchar(100) NOT NULL,
  `schedule_day` varchar(45) NOT NULL,
  `schedule_start_time` time NOT NULL,
  `schedule_end_time` time NOT NULL,
  PRIMARY KEY (`id_schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_schedule`
--

LOCK TABLES `dbo_schedule` WRITE;
/*!40000 ALTER TABLE `dbo_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_status`
--

DROP TABLE IF EXISTS `dbo_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_status` (
  `id_status` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_status`
--

LOCK TABLES `dbo_status` WRITE;
/*!40000 ALTER TABLE `dbo_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_status_employee`
--

DROP TABLE IF EXISTS `dbo_status_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_status_employee` (
  `id_status_employee` varchar(100) NOT NULL,
  `status_employee` int(10) unsigned NOT NULL,
  `status_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_status_employee`),
  KEY `fk_dbo_status_employee_dbo_status1_idx` (`status_employee`),
  CONSTRAINT `fk_dbo_status_employee_dbo_employee_profile1` FOREIGN KEY (`id_status_employee`) REFERENCES `dbo_employee_profile` (`id_profile`),
  CONSTRAINT `fk_dbo_status_employee_dbo_status1` FOREIGN KEY (`status_employee`) REFERENCES `dbo_status` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_status_employee`
--

LOCK TABLES `dbo_status_employee` WRITE;
/*!40000 ALTER TABLE `dbo_status_employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_status_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_status_employee_log`
--

DROP TABLE IF EXISTS `dbo_status_employee_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_status_employee_log` (
  `id_status_employee_log` int(11) NOT NULL AUTO_INCREMENT,
  `sel_employee` varchar(100) NOT NULL,
  `sel_status` int(10) unsigned NOT NULL,
  `sel_start_date` date NOT NULL,
  `sel_end_date` date DEFAULT NULL,
  PRIMARY KEY (`id_status_employee_log`),
  KEY `fk_dbo_status_employee_log_dbo_status_employee1_idx` (`sel_employee`),
  CONSTRAINT `fk_dbo_status_employee_log_dbo_status_employee1` FOREIGN KEY (`sel_employee`) REFERENCES `dbo_status_employee` (`id_status_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_status_employee_log`
--

LOCK TABLES `dbo_status_employee_log` WRITE;
/*!40000 ALTER TABLE `dbo_status_employee_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbo_status_employee_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_user`
--

DROP TABLE IF EXISTS `dbo_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dbo_user` (
  `id_user` varchar(100) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_pass` varchar(200) NOT NULL,
  `user_group` int(10) unsigned NOT NULL,
  `user_email` varchar(45) NOT NULL,
  `user_recovery` text NOT NULL,
  `user_status` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `fk_dbo_user_dbo_group1_idx` (`user_group`),
  CONSTRAINT `fk_dbo_user_dbo_employee1` FOREIGN KEY (`id_user`) REFERENCES `dbo_employee` (`id_employee`),
  CONSTRAINT `fk_dbo_user_dbo_group1` FOREIGN KEY (`user_group`) REFERENCES `dbo_group` (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_user`
--

LOCK TABLES `dbo_user` WRITE;
/*!40000 ALTER TABLE `dbo_user` DISABLE KEYS */;
INSERT INTO `dbo_user` VALUES ('EMP-000001','EMP1-Adonay@ADMIN','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,'ad1@gmail.com','123 a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1);
/*!40000 ALTER TABLE `dbo_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tr_employee_recovery` AFTER INSERT ON `dbo_user` FOR EACH ROW BEGIN

INSERT INTO `dbmodoru`.`dbo_recovery` (`id_recovery`, `recovery_user`) 
VALUES ( concat('RVY-',new.user_name,'-1') , new.id_user);

INSERT INTO `dbmodoru`.`dbo_recovery` (`id_recovery`, `recovery_user`)
VALUES ( concat('RVY-',new.user_name,'-2') , new.id_user);

INSERT INTO `dbmodoru`.`dbo_recovery` (`id_recovery`, `recovery_user`) 
VALUES ( concat('RVY-',new.user_name,'-3') , new.id_user);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary view structure for view `view_user_employee`
--

DROP TABLE IF EXISTS `view_user_employee`;
/*!50001 DROP VIEW IF EXISTS `view_user_employee`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `view_user_employee` AS SELECT 
 1 AS `id_e`,
 1 AS `name_e`,
 1 AS `last_name_e`,
 1 AS `birthday_e`,
 1 AS `dui_e`,
 1 AS `nit_e`,
 1 AS `isss_e`,
 1 AS `country_e`,
 1 AS `afp_e`,
 1 AS `nup_e`,
 1 AS `gender_e`,
 1 AS `civil_status_e`,
 1 AS `passport_e`,
 1 AS `adress_e`,
 1 AS `hire_date_e`,
 1 AS `id_u`,
 1 AS `name_u`,
 1 AS `pass_u`,
 1 AS `group_u`,
 1 AS `email_u`,
 1 AS `recovery_u`,
 1 AS `status_u`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'dbmodoru'
--

--
-- Dumping routines for database 'dbmodoru'
--
/*!50003 DROP PROCEDURE IF EXISTS `ps_employee_address` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_employee_address`(
in e_id varchar(100),
in e_name varchar(45),
in e_last_name varchar(45),
in e_birthdate Date,
in e_dui varchar(10),
in e_nit varchar(17),
in e_isss varchar(20),
in e_country int,
in e_afp int,
in e_nup varchar(15),
in e_gender int,
in e_civil_status int,
in e_passport varchar(45),
in e_addres varchar(45),
in e_hired_date datetime,
in a_municipality int,
in a_address text
)
BEGIN

INSERT INTO `dbmodoru`.`dbo_address` 
(`id_address`, `employee_municipality`, `employee_address`) 
VALUES 
(e_addres, a_municipality, a_address);

INSERT INTO `dbmodoru`.`dbo_employee` 
(`id_employee`, `employee_name`, `employee_last_name`, `employee_birthday`, `employee_dui`, `employee_nit`, `employee_isss`, `employee_country`, `employee_afp`, `employee_nup`, `employee_gender`, `employee_civil_status`, `employee_passport`, `employee_address`, `employee_hired_date`) 
VALUES 
(e_id , e_name, e_last_name, e_birthdate, e_dui, e_nit, e_isss, e_country, e_afp, e_nup, e_gender, e_civil_status, e_passport, e_addres, e_hired_date);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `view_user_employee`
--

/*!50001 DROP VIEW IF EXISTS `view_user_employee`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_user_employee` AS select `e`.`id_employee` AS `id_e`,`e`.`employee_name` AS `name_e`,`e`.`employee_last_name` AS `last_name_e`,`e`.`employee_birthday` AS `birthday_e`,`e`.`employee_dui` AS `dui_e`,`e`.`employee_nit` AS `nit_e`,`e`.`employee_isss` AS `isss_e`,`e`.`employee_country` AS `country_e`,`e`.`employee_afp` AS `afp_e`,`e`.`employee_nup` AS `nup_e`,`e`.`employee_gender` AS `gender_e`,`e`.`employee_civil_status` AS `civil_status_e`,`e`.`employee_passport` AS `passport_e`,`e`.`employee_address` AS `adress_e`,`e`.`employee_hired_date` AS `hire_date_e`,`u`.`id_user` AS `id_u`,`u`.`user_name` AS `name_u`,`u`.`user_pass` AS `pass_u`,`u`.`user_group` AS `group_u`,`u`.`user_email` AS `email_u`,`u`.`user_recovery` AS `recovery_u`,`u`.`user_status` AS `status_u` from (`dbo_employee` `e` join `dbo_user` `u` on((`e`.`id_employee` = `u`.`id_user`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-20 14:02:06
