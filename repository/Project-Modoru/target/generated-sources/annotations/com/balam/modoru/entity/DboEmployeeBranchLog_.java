package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployeeProfile;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboEmployeeBranchLog.class)
public class DboEmployeeBranchLog_ { 

    public static volatile SingularAttribute<DboEmployeeBranchLog, DboEmployeeProfile> eblIdProfile;
    public static volatile SingularAttribute<DboEmployeeBranchLog, String> idEbl;
    public static volatile SingularAttribute<DboEmployeeBranchLog, Date> eblEndDate;
    public static volatile SingularAttribute<DboEmployeeBranchLog, String> eblBranch;
    public static volatile SingularAttribute<DboEmployeeBranchLog, Date> eblStartDate;

}