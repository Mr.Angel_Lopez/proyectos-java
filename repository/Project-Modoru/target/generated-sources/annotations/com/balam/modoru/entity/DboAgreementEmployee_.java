package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployeeProfile;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboAgreementEmployee.class)
public class DboAgreementEmployee_ { 

    public static volatile SingularAttribute<DboAgreementEmployee, String> aeJobName;
    public static volatile SingularAttribute<DboAgreementEmployee, String> aeBranchOffice;
    public static volatile SingularAttribute<DboAgreementEmployee, String> aeEmployee;
    public static volatile CollectionAttribute<DboAgreementEmployee, DboEmployeeProfile> dboEmployeeProfileCollection;
    public static volatile SingularAttribute<DboAgreementEmployee, Date> aeDate;
    public static volatile SingularAttribute<DboAgreementEmployee, String> aeJob;
    public static volatile SingularAttribute<DboAgreementEmployee, BigDecimal> aeSalary;
    public static volatile SingularAttribute<DboAgreementEmployee, String> idAgreementEmployee;

}