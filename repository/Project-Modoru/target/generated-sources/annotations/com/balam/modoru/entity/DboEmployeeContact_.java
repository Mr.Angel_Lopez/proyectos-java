package com.balam.modoru.entity;

import com.balam.modoru.entity.DboContact;
import com.balam.modoru.entity.DboEmployee;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboEmployeeContact.class)
public class DboEmployeeContact_ { 

    public static volatile SingularAttribute<DboEmployeeContact, DboContact> ecContact;
    public static volatile SingularAttribute<DboEmployeeContact, DboEmployee> ecEmployee;
    public static volatile SingularAttribute<DboEmployeeContact, Integer> idEmployeeContact;

}