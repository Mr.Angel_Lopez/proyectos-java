package com.balam.modoru.entity;

import com.balam.modoru.entity.DboUser;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboGroup.class)
public class DboGroup_ { 

    public static volatile SingularAttribute<DboGroup, String> groupName;
    public static volatile CollectionAttribute<DboGroup, DboUser> dboUserCollection;
    public static volatile SingularAttribute<DboGroup, Integer> idGroup;

}