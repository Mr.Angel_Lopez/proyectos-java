package com.balam.modoru.entity;

import com.balam.modoru.entity.DboQuestion;
import com.balam.modoru.entity.DboUser;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboRecovery.class)
public class DboRecovery_ { 

    public static volatile SingularAttribute<DboRecovery, String> idRecovery;
    public static volatile SingularAttribute<DboRecovery, DboUser> recoveryUser;
    public static volatile SingularAttribute<DboRecovery, DboQuestion> recoveryQuestion;
    public static volatile SingularAttribute<DboRecovery, String> recoveryAnwser;

}