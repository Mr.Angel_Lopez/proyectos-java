package com.balam.modoru.entity;

import com.balam.modoru.entity.DboAgreementEmployee;
import com.balam.modoru.entity.DboBranchOffice;
import com.balam.modoru.entity.DboEmployee;
import com.balam.modoru.entity.DboEmployeeBranchLog;
import com.balam.modoru.entity.DboEmployeeJobLog;
import com.balam.modoru.entity.DboJob;
import com.balam.modoru.entity.DboStatusEmployee;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboEmployeeProfile.class)
public class DboEmployeeProfile_ { 

    public static volatile SingularAttribute<DboEmployeeProfile, String> idProfile;
    public static volatile CollectionAttribute<DboEmployeeProfile, DboEmployeeJobLog> dboEmployeeJobLogCollection;
    public static volatile SingularAttribute<DboEmployeeProfile, DboJob> profileJob;
    public static volatile SingularAttribute<DboEmployeeProfile, DboAgreementEmployee> profileAgreement;
    public static volatile SingularAttribute<DboEmployeeProfile, DboBranchOffice> profileBranchOffice;
    public static volatile SingularAttribute<DboEmployeeProfile, DboEmployee> profileEmployee;
    public static volatile SingularAttribute<DboEmployeeProfile, Date> profileDate;
    public static volatile SingularAttribute<DboEmployeeProfile, DboStatusEmployee> dboStatusEmployee;
    public static volatile CollectionAttribute<DboEmployeeProfile, DboEmployeeBranchLog> dboEmployeeBranchLogCollection;

}