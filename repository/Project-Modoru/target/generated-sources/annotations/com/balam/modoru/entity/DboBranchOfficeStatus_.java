package com.balam.modoru.entity;

import com.balam.modoru.entity.DboBranchOffice;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboBranchOfficeStatus.class)
public class DboBranchOfficeStatus_ { 

    public static volatile SingularAttribute<DboBranchOfficeStatus, Integer> idBranchOfficeStatus;
    public static volatile SingularAttribute<DboBranchOfficeStatus, String> bosName;
    public static volatile CollectionAttribute<DboBranchOfficeStatus, DboBranchOffice> dboBranchOfficeCollection;

}