package com.balam.modoru.entity;

import com.balam.modoru.entity.DboAddress;
import com.balam.modoru.entity.DboBranchOffice;
import com.balam.modoru.entity.DboDepartament;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboMunicipality.class)
public class DboMunicipality_ { 

    public static volatile SingularAttribute<DboMunicipality, Integer> idMunicipality;
    public static volatile CollectionAttribute<DboMunicipality, DboBranchOffice> dboBranchOfficeCollection;
    public static volatile CollectionAttribute<DboMunicipality, DboAddress> dboAddressCollection;
    public static volatile SingularAttribute<DboMunicipality, DboDepartament> municipalityDepartament;
    public static volatile SingularAttribute<DboMunicipality, String> municipalityName;

}