package com.balam.modoru.entity;

import com.balam.modoru.entity.DboBranchOfficeStatus;
import com.balam.modoru.entity.DboEmployeeProfile;
import com.balam.modoru.entity.DboMunicipality;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboBranchOffice.class)
public class DboBranchOffice_ { 

    public static volatile SingularAttribute<DboBranchOffice, String> boAddress;
    public static volatile CollectionAttribute<DboBranchOffice, DboEmployeeProfile> dboEmployeeProfileCollection;
    public static volatile SingularAttribute<DboBranchOffice, String> boName;
    public static volatile SingularAttribute<DboBranchOffice, DboBranchOfficeStatus> boStatus;
    public static volatile SingularAttribute<DboBranchOffice, String> idBranchOffice;
    public static volatile SingularAttribute<DboBranchOffice, DboMunicipality> boMunicipality;

}