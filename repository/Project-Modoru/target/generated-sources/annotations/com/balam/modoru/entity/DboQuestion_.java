package com.balam.modoru.entity;

import com.balam.modoru.entity.DboRecovery;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboQuestion.class)
public class DboQuestion_ { 

    public static volatile SingularAttribute<DboQuestion, String> questionQuestion;
    public static volatile CollectionAttribute<DboQuestion, DboRecovery> dboRecoveryCollection;
    public static volatile SingularAttribute<DboQuestion, Integer> idQuestion;

}