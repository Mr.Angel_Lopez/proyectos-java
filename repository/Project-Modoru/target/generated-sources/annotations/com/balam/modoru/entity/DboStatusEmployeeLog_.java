package com.balam.modoru.entity;

import com.balam.modoru.entity.DboStatusEmployee;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboStatusEmployeeLog.class)
public class DboStatusEmployeeLog_ { 

    public static volatile SingularAttribute<DboStatusEmployeeLog, Integer> selStatus;
    public static volatile SingularAttribute<DboStatusEmployeeLog, Date> selStartDate;
    public static volatile SingularAttribute<DboStatusEmployeeLog, Integer> idStatusEmployeeLog;
    public static volatile SingularAttribute<DboStatusEmployeeLog, DboStatusEmployee> selEmployee;
    public static volatile SingularAttribute<DboStatusEmployeeLog, Date> selEndDate;

}