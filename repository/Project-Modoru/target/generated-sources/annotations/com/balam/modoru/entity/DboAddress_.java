package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployee;
import com.balam.modoru.entity.DboMunicipality;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboAddress.class)
public class DboAddress_ { 

    public static volatile CollectionAttribute<DboAddress, DboEmployee> dboEmployeeCollection;
    public static volatile SingularAttribute<DboAddress, String> employeeAddress;
    public static volatile SingularAttribute<DboAddress, DboMunicipality> employeeMunicipality;
    public static volatile SingularAttribute<DboAddress, String> idAddress;

}