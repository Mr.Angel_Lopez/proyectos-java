package com.balam.modoru.entity;

import com.balam.modoru.entity.DboMunicipality;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboDepartament.class)
public class DboDepartament_ { 

    public static volatile SingularAttribute<DboDepartament, Integer> idDepartament;
    public static volatile SingularAttribute<DboDepartament, String> departamentName;
    public static volatile CollectionAttribute<DboDepartament, DboMunicipality> dboMunicipalityCollection;

}