package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployee;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboCivilStatus.class)
public class DboCivilStatus_ { 

    public static volatile CollectionAttribute<DboCivilStatus, DboEmployee> dboEmployeeCollection;
    public static volatile SingularAttribute<DboCivilStatus, String> civilStatusName;
    public static volatile SingularAttribute<DboCivilStatus, Integer> idCivilStatus;

}