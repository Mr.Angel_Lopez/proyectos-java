package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployee;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboAfp.class)
public class DboAfp_ { 

    public static volatile CollectionAttribute<DboAfp, DboEmployee> dboEmployeeCollection;
    public static volatile SingularAttribute<DboAfp, Integer> idAfp;
    public static volatile SingularAttribute<DboAfp, String> afpName;

}