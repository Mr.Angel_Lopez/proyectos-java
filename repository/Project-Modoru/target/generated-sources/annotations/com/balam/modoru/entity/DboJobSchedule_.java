package com.balam.modoru.entity;

import com.balam.modoru.entity.DboJob;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboJobSchedule.class)
public class DboJobSchedule_ { 

    public static volatile SingularAttribute<DboJobSchedule, String> idJobSchedule;
    public static volatile SingularAttribute<DboJobSchedule, String> jobSchedule;
    public static volatile SingularAttribute<DboJobSchedule, DboJob> jobJob;

}