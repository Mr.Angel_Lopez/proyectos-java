package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployeeContact;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboContact.class)
public class DboContact_ { 

    public static volatile SingularAttribute<DboContact, String> idContact;
    public static volatile SingularAttribute<DboContact, String> contactEmail;
    public static volatile CollectionAttribute<DboContact, DboEmployeeContact> dboEmployeeContactCollection;
    public static volatile SingularAttribute<DboContact, String> contactName;
    public static volatile SingularAttribute<DboContact, Integer> contactPriority;
    public static volatile SingularAttribute<DboContact, String> contactLastName;
    public static volatile SingularAttribute<DboContact, String> contactPhone;
    public static volatile SingularAttribute<DboContact, String> contactMovil;

}