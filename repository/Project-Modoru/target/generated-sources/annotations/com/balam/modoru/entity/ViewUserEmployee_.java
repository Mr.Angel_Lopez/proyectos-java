package com.balam.modoru.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(ViewUserEmployee.class)
public class ViewUserEmployee_ { 

    public static volatile SingularAttribute<ViewUserEmployee, String> nameE;
    public static volatile SingularAttribute<ViewUserEmployee, String> duiE;
    public static volatile SingularAttribute<ViewUserEmployee, Date> birthdayE;
    public static volatile SingularAttribute<ViewUserEmployee, String> isssE;
    public static volatile SingularAttribute<ViewUserEmployee, String> idE;
    public static volatile SingularAttribute<ViewUserEmployee, Integer> groupU;
    public static volatile SingularAttribute<ViewUserEmployee, String> lastNameE;
    public static volatile SingularAttribute<ViewUserEmployee, String> nupE;
    public static volatile SingularAttribute<ViewUserEmployee, Integer> civilStatusE;
    public static volatile SingularAttribute<ViewUserEmployee, Integer> statusU;
    public static volatile SingularAttribute<ViewUserEmployee, String> nameU;
    public static volatile SingularAttribute<ViewUserEmployee, String> recoveryU;
    public static volatile SingularAttribute<ViewUserEmployee, Integer> afpE;
    public static volatile SingularAttribute<ViewUserEmployee, Date> hireDateE;
    public static volatile SingularAttribute<ViewUserEmployee, String> adressE;
    public static volatile SingularAttribute<ViewUserEmployee, String> passportE;
    public static volatile SingularAttribute<ViewUserEmployee, String> idU;
    public static volatile SingularAttribute<ViewUserEmployee, Integer> countryE;
    public static volatile SingularAttribute<ViewUserEmployee, Integer> genderE;
    public static volatile SingularAttribute<ViewUserEmployee, String> passU;
    public static volatile SingularAttribute<ViewUserEmployee, String> emailU;
    public static volatile SingularAttribute<ViewUserEmployee, String> nitE;

}