package com.balam.modoru.entity;

import com.balam.modoru.entity.DboActivityLog;
import com.balam.modoru.entity.DboEmployee;
import com.balam.modoru.entity.DboGroup;
import com.balam.modoru.entity.DboRecovery;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboUser.class)
public class DboUser_ { 

    public static volatile SingularAttribute<DboUser, String> idUser;
    public static volatile CollectionAttribute<DboUser, DboActivityLog> dboActivityLogCollection;
    public static volatile SingularAttribute<DboUser, Integer> userStatus;
    public static volatile SingularAttribute<DboUser, DboEmployee> dboEmployee;
    public static volatile CollectionAttribute<DboUser, DboRecovery> dboRecoveryCollection;
    public static volatile SingularAttribute<DboUser, String> userPass;
    public static volatile SingularAttribute<DboUser, String> userRecovery;
    public static volatile SingularAttribute<DboUser, String> userEmail;
    public static volatile SingularAttribute<DboUser, String> userName;
    public static volatile SingularAttribute<DboUser, DboGroup> userGroup;

}