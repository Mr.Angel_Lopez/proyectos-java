package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployeeProfile;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboEmployeeJobLog.class)
public class DboEmployeeJobLog_ { 

    public static volatile SingularAttribute<DboEmployeeJobLog, String> ejlEmployeeJob;
    public static volatile SingularAttribute<DboEmployeeJobLog, Date> ejlEndJob;
    public static volatile SingularAttribute<DboEmployeeJobLog, String> idEjl;
    public static volatile SingularAttribute<DboEmployeeJobLog, DboEmployeeProfile> ejlEmployee;
    public static volatile SingularAttribute<DboEmployeeJobLog, Date> ejlStartJob;

}