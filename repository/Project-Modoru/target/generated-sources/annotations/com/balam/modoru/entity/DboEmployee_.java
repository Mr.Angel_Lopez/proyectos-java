package com.balam.modoru.entity;

import com.balam.modoru.entity.DboAddress;
import com.balam.modoru.entity.DboAfp;
import com.balam.modoru.entity.DboCivilStatus;
import com.balam.modoru.entity.DboCountry;
import com.balam.modoru.entity.DboEmployeeContact;
import com.balam.modoru.entity.DboEmployeeProfile;
import com.balam.modoru.entity.DboGender;
import com.balam.modoru.entity.DboUser;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboEmployee.class)
public class DboEmployee_ { 

    public static volatile SingularAttribute<DboEmployee, String> employeeNup;
    public static volatile SingularAttribute<DboEmployee, String> employeeName;
    public static volatile SingularAttribute<DboEmployee, String> employeePassport;
    public static volatile SingularAttribute<DboEmployee, DboCountry> employeeCountry;
    public static volatile CollectionAttribute<DboEmployee, DboEmployeeProfile> dboEmployeeProfileCollection;
    public static volatile SingularAttribute<DboEmployee, DboAfp> employeeAfp;
    public static volatile SingularAttribute<DboEmployee, DboGender> employeeGender;
    public static volatile SingularAttribute<DboEmployee, DboAddress> employeeAddress;
    public static volatile SingularAttribute<DboEmployee, String> employeeIsss;
    public static volatile SingularAttribute<DboEmployee, String> employeeNit;
    public static volatile SingularAttribute<DboEmployee, String> idEmployee;
    public static volatile SingularAttribute<DboEmployee, String> employeeDui;
    public static volatile SingularAttribute<DboEmployee, DboUser> dboUser;
    public static volatile SingularAttribute<DboEmployee, Date> employeeBirthday;
    public static volatile SingularAttribute<DboEmployee, DboCivilStatus> employeeCivilStatus;
    public static volatile SingularAttribute<DboEmployee, String> employeeLastName;
    public static volatile CollectionAttribute<DboEmployee, DboEmployeeContact> dboEmployeeContactCollection;
    public static volatile SingularAttribute<DboEmployee, Date> employeeHiredDate;

}