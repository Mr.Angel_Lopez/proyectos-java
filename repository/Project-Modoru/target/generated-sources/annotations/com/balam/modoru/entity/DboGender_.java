package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployee;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboGender.class)
public class DboGender_ { 

    public static volatile CollectionAttribute<DboGender, DboEmployee> dboEmployeeCollection;
    public static volatile SingularAttribute<DboGender, Integer> idGender;
    public static volatile SingularAttribute<DboGender, String> genderName;

}