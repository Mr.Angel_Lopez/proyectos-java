package com.balam.modoru.entity;

import com.balam.modoru.entity.DboStatusEmployee;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboStatus.class)
public class DboStatus_ { 

    public static volatile SingularAttribute<DboStatus, Integer> idStatus;
    public static volatile CollectionAttribute<DboStatus, DboStatusEmployee> dboStatusEmployeeCollection;
    public static volatile SingularAttribute<DboStatus, String> statusName;

}