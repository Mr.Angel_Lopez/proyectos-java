package com.balam.modoru.entity;

import com.balam.modoru.entity.DboDivision;
import com.balam.modoru.entity.DboEmployeeProfile;
import com.balam.modoru.entity.DboJobSchedule;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboJob.class)
public class DboJob_ { 

    public static volatile SingularAttribute<DboJob, String> jobName;
    public static volatile SingularAttribute<DboJob, String> jobLocal;
    public static volatile SingularAttribute<DboJob, BigDecimal> jobSalary;
    public static volatile CollectionAttribute<DboJob, DboEmployeeProfile> dboEmployeeProfileCollection;
    public static volatile SingularAttribute<DboJob, String> idJob;
    public static volatile CollectionAttribute<DboJob, DboJobSchedule> dboJobScheduleCollection;
    public static volatile SingularAttribute<DboJob, DboDivision> jobDivision;

}