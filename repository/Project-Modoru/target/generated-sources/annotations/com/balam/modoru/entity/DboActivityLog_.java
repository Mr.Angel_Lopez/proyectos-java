package com.balam.modoru.entity;

import com.balam.modoru.entity.DboUser;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboActivityLog.class)
public class DboActivityLog_ { 

    public static volatile SingularAttribute<DboActivityLog, Date> activityDate;
    public static volatile SingularAttribute<DboActivityLog, String> idActivityLog;
    public static volatile SingularAttribute<DboActivityLog, String> activityDescription;
    public static volatile SingularAttribute<DboActivityLog, DboUser> activityUser;
    public static volatile SingularAttribute<DboActivityLog, String> activityAction;

}