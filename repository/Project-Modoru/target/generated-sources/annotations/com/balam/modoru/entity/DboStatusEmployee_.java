package com.balam.modoru.entity;

import com.balam.modoru.entity.DboEmployeeProfile;
import com.balam.modoru.entity.DboStatus;
import com.balam.modoru.entity.DboStatusEmployeeLog;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:45")
@StaticMetamodel(DboStatusEmployee.class)
public class DboStatusEmployee_ { 

    public static volatile SingularAttribute<DboStatusEmployee, Date> statusDate;
    public static volatile SingularAttribute<DboStatusEmployee, String> idStatusEmployee;
    public static volatile CollectionAttribute<DboStatusEmployee, DboStatusEmployeeLog> dboStatusEmployeeLogCollection;
    public static volatile SingularAttribute<DboStatusEmployee, DboEmployeeProfile> dboEmployeeProfile;
    public static volatile SingularAttribute<DboStatusEmployee, DboStatus> statusEmployee;

}