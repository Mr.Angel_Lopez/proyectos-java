package com.balam.modoru.entity;

import com.balam.modoru.entity.DboJob;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboDivision.class)
public class DboDivision_ { 

    public static volatile SingularAttribute<DboDivision, String> divisonName;
    public static volatile CollectionAttribute<DboDivision, DboJob> dboJobCollection;
    public static volatile SingularAttribute<DboDivision, Integer> idDivision;

}