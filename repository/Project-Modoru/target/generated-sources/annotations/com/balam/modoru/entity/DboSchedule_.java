package com.balam.modoru.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2019-02-19T14:26:44")
@StaticMetamodel(DboSchedule.class)
public class DboSchedule_ { 

    public static volatile SingularAttribute<DboSchedule, String> scheduleName;
    public static volatile SingularAttribute<DboSchedule, String> scheduleDay;
    public static volatile SingularAttribute<DboSchedule, Integer> idSchedule;
    public static volatile SingularAttribute<DboSchedule, Date> scheduleStartTime;
    public static volatile SingularAttribute<DboSchedule, Date> scheduleEndTime;

}